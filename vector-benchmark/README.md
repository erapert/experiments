Rambling about:
---------------
These are a bunch of little benchmarks for various programming languages.

I wanted to evaluate relative performance of the various languages given a piece of functionality that's been written in a natural way and then compare how the different languages/environments handled that. I am aware of several cases where performance could be greatly improved by taking advantage of the way a language is implemented or by using specialized language features. But I've done my best to keep the Vector3 libraries as semantically equivalent as possible because I want to measure how the different languages/implementations execute my ideas; *not* how quickly they can implement a given algorithm.

See, it's the languages' jobs to map my train of thought into an executable program. It's hard enough just to get things to compile and run. I've got enough on my plate worrying about features and deadlines I don't want to worry about esoteric optimizations that require a decade of experience or a PhD in Computer Science to suss out. That's not to say that I don't consider performance to be important! But at the same time I place a higher emphasis on:

* Legibility
* Natural/easy flow of logic and interface.
* keeping the API/method calls and semantics consistent across languages
	(for example, ruby allows "equals?" as a method name but C++ does not)

I understand that everyone writes crappy code; mine is terrible! But I want to strive toward clarity.


How to run:
-----------
1. Make sure you've got the following languages/environments installed and config'd
	- lua (5.2+)
	- php (5.3+)
	- ruby (1.8.7 || 1.9.3 || 2.0.0)
	- python (2.7 but should also work with 3.x)
	- C# (3.0 on Mono)
	- C++ (0xx on g++)
	- D (2.x on gdc 4.6+)
	- javascript (node.js 4.10+)
2. Several benchmarks need to be compiled or require some extra libs that provide things like high-rez timers. So run these commands in the directory of the relevant benchmark:
	- php: `pear install Benchmark Console_CommandLine`
	- C++: `sudo apt-get install libboost-dev && make`
	- lua: `make && sudo make install`
	- D: `make`
3. Run `./test.sh` from the top level dir
	- of course it needs to be executable if it isn't already: `chmod u+x ./test.sh`

Notes:
------
* all times should be displayed in seconds
	- C# prints in HH:mm:ss format
	- all others in raw seconds
