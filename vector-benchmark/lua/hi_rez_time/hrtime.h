/*
	For Lua all C functions return an int which says how many items they're returning on the stack.
	The actual return of the values is done by pushing the vals onto the stack.
*/
#ifndef HI_REZ_TIMER
#define HI_REZ_TIMER

#include <sys/time.h>

#include <lua.h>
#include <lualib.h>

// inits this lib and registers the C functions
int luaopen_hrtime (lua_State * L);

int hrt_getTime (lua_State * L);

#endif

