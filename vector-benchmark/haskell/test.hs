import Criterion.Main
import Vector3

unitTest =
	let
		v0 = Vector3 1.1 2.2 3.3
		v1 = Vector3 4.4 5.5 6.6
	in do
		putStrLn $ "v0 = " ++ stringify v0
		putStrLn $ "v1 = " ++ show v1
		putStrLn $ "v0 == v1: " ++ show (equals v0 v1)
		putStrLn $ "v0 + 2.2: " ++ show (plusScalar v0 2.2)
		putStrLn $ "v0 - 2.2: " ++ show (minusScalar v0 2.2)
		putStrLn $ "v0 + v1: " ++ show (plus v0 v1)
		putStrLn $ "v0 - v1: " ++ show (minus v0 v1)
		putStrLn $ "transform v0 1.1 2.2 3.3: " ++ show (transform v0 1.1 2.2 3.3)
		putStrLn $ "scale v0 2.0: " ++ show (scale v0 2.0)
		putStrLn $ "len v0: " ++ show (len v0)
		putStrLn $ "dot v0 v1: " ++ show (dot v0 v1)
		putStrLn $ "cross v0 v1: " ++ show (cross v0 v1)

main =
	let
		v0 = Vector3 1.1 2.2 3.3
		v1 = Vector3 4.4 5.5 6.6
	in do
		defaultMain [
			bgroup "utils" [
				bench "stringify v0" $ whnf (stringify) v0,
				bench "equals v0 v1" $ whnf (equals v0) v1
				],
			bgroup "scalarArithmetic" [
				bench "plusScalar v0 1.1" $ whnf (plusScalar v0) 1.1,
				bench "minusScalar v1 2.2" $ whnf (minusScalar v1) 2.2,
				bench "transform v0 1.1 2.2 3.3" $ whnf (transform v0 1.1 2.2) 3.3,
				bench "scale v1 10.0" $ whnf (scale v0) 10.0
				],
			bgroup "vectorArithmetic" [
				bench "plus v0 v1" $ whnf (plus v0) v1,
				bench "minus v0 v1" $ whnf (minus v1) v0
				],
			bgroup "complexOperations" [
				bench "length v0" $ whnf (len) v0,
				bench "dot v0 v1" $ whnf (dot v0) v1,
				bench "cross v0 v1" $ whnf (cross v0) v1
				]
			]
