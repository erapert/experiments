extern crate rustc_serialize;
extern crate docopt;
mod vector3;
mod timer;

use docopt::Docopt;
use vector3::Vector3;
use timer::Timer;


fn unit () {
	let v1 = Vector3 { x: 1.0, y: 2.0, z: 3.0 };
	let v2 = Vector3 { x: 4.0, y: 5.0, z: 6.0 };
	
	println! ("test:");
	
	v1.print_self ();
	v2.print_self ();
	
	// todo: write asserts for all these
	println! ("v1.equals (v2): {}", v1.equals (&v2));
	println! ("v1.plus_scalar (1.0): {}", v1.plus_scalar (1.0).stringify ());
	println! ("v1.minus_scalar (1.0): {}", v1.minus_scalar (1.0).stringify ());
	println! ("v1.plus (v2): {}", v1.plus (&v2).stringify ());
	println! ("v1.minus (v2): {}", v1.minus (&v2).stringify ());
	println! ("v1.transform (1.0, 2.0, 3.0): {}", v1.transform (1.0, 2.0, 3.0).stringify ());
	println! ("v1.scale (2.0): {}", v1.scale (2.0).stringify ());
	println! ("v1.length (): {}", v1.length ());
	println! ("v1.dot (v2): {}", v1.dot (&v2));
	println! ("v1.cross (v2): {}", v1.cross (&v2).stringify ());
}

fn benchmark () {
	let v1 = Vector3 { x: 1.0, y: 2.0, z: 3.0 };
	let v2 = Vector3 { x: 4.0, y: 5.0, z: 6.0 };
	let mut t = Timer::new ();
	
	println! ("benchmark:");
	
	println! ("v1: {}", v1.stringify ());
	println! ("v2: {}", v2.stringify ());
	
	println! ("v1.stringify (): {}",
		t.time_it (|| {
			let temp = v1.stringify ();
		}).duration_str ()
	);
	
	println! ("v1.equals (&v2): {}",
		t.time_it (|| {
			let temp = v1.equals (&v2);
		}).duration_str ()
	);
	
	println! ("v1.plus_scalar (2.0): {}",
		t.time_it (|| {
			let temp = v1.plus_scalar (2.0);
		}).duration_str ()
	);
	
	println! ("v1.minus_scalar (2.0): {}",
		t.time_it (|| {
			let temp = v1.minus_scalar (2.0);
		}).duration_str ()
	);
	
	println! ("v1.plus (&v2): {}",
		t.time_it (|| {
			let temp = v1.plus (&v2);
		}).duration_str ()
	);
	
	println! ("v1.minus (&v2): {}",
		t.time_it (|| {
			let temp = v1.minus (&v2);
		}).duration_str ()
	);
	
	println! ("v1.transform (1.0, 2.0, 3.0): {}",
		t.time_it (|| {
			let temp = v1.transform (1.0, 2.0, 3.0);
		}).duration_str ()
	);
	
	println! ("v1.scale (2.0): {}",
		t.time_it (|| {
			let temp = v1.scale (2.0);
		}).duration_str ()
	);
	
	println! ("v1.length (): {}",
		t.time_it (|| {
			let temp = v1.length ();
		}).duration_str ()
	);
	
	println! ("v1.dot (&v2): {}",
		t.time_it (|| {
			let temp = v1.dot (&v2);
		}).duration_str ()
	);
	
	println! ("v1.cross (&v2): {}",
		t.time_it (|| {
			let temp = v1.cross (&v2);
		}).duration_str ()
	);
}




const USAGE : &'static str = "
Usage:
	vector_benchmark [options]

Options:
	-h, --help         Show this message.
	-u, --unit         Run unit test.
	-b, --benchmark    Run benchmark.
";

#[derive(Debug, RustcDecodable)]
struct Args {
	flag_unit: bool,
	flag_benchmark: bool
}

fn main () {
	let args : Args = Docopt::new (USAGE)
					.and_then (|d| d.decode())
					.unwrap_or_else (|e| e.exit ());
	
	if args.flag_unit {
		unit ();
	}
	if args.flag_benchmark {
		benchmark ();
	}
}
