import std.stdio;
import logger;
	
void main () {
	
	write ("\n");
	
	// you can set the width for indentation in spaces
	// and you can turn off color
	auto l = new Logger (4, false);
	
	l.indent(1).log ("You can [r]init[/] with any [c]indentationSize[/] you want, and without color");
	write ("\n");
	
	// you can turn color support on later though
	l.enableColor = true;
	// and you can set indentSize too
	l.indentSize = 2;
	
	// anything that std.string.format() supports
	l.resetIndent()
	.log ("[c]log()[/] supports string formatting:").indent (1)
	.log ("Roses are [r]RED")
	.log ("Violets are [b]%s[/]", "BLUE")
	.log ("and this is the number [m]%d[/]", 42)
	.resetIndent();
	
	write ("\n");
	
	// and you can chain most functions together
	l.log ("Colors [y]run to the end[/] mark").indent (2)
		.log ("[r]But lines are auto-terminated.")
		.log ("So this is not colorized.")
	.resetIndent ()
	.log ("you can reset")
	.setIndent (2)
		.log ("and set (%d) the indentation level", 2)
	.resetIndent ();
	
	write ("\n");
	
	l.log ("and there's a nifty function that'll print out all the codes for you:");
	l.showColorCodes ();
	
	write ("\n");
}
