#!/usr/bin/python

import argparse, sys, subprocess

projName = ''
projDescription = ''
projArch = 'i386'
dirs = ['src', 'lib', 'inc', 'obj']
files = ['Makefile', 'README.md']


def makeStructure ():
	output = ''
	dcmd = ['mkdir', projName]
	fcmd = ['touch', '']
	
	subprocess.call (dcmd)
	print (output)
	
	for d in dirs:
		output = ''
		dcmd [1] = "%s/%s" % (projName, d)
		subprocess.call (dcmd)
		print (output)
	
	for f in files:
		output = ''
		fcmd [1] = f
		subprocess.call (fcmd)
		print (output)

def writeMakefile ():
	f = open (projName + '/Makefile', 'w')
	exeName_dbg = "%s_dbg.%s" % (projName, projArch)
	f.write (
"""
CFLAGS_OPT=-O3 -Wall -std=c++0x
CFLAGS_DBG=-Wall -std=c++0x

%s:
	g++ $(CFLAGS_DBG) -o %s
	chmod u+x %s

clean: %s:
	rm ./%s
""" % (exeName_dbg, exeName_dbg, exeName_dbg, exeName_dbg, exeName_dbg)
	)
	f.close ()

def writeReadmefile ():
	f = open (projName + '/README.md', 'w')
	f.write ("%s%s\n%s" % (projName, ('=' * len (projName)), projDescription))
	f.close ()

if __name__ == "__main__":
	parser = argparse.ArgumentParser ()
	parser.add_argument ('-n', help = 'Name of project', required = True)
	parser.add_argument ('-d', help = 'Description of project')
	parser.add_argument ('-a', help = 'Target architecture (i.e. i386)')
	
	args = parser.parse_args ()
	
	if (args.n):
		projName = args.n
	if (args.d):
		projDescription = args.d
	if (args.a):
		projArch = args.a
	
	makeStructure ()
	writeMakefile ()
	writeReadmefile ()
	
	
