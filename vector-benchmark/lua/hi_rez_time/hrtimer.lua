
-- gives us hrt_getTime()
require 'hrtime'

local HRTimer = {}
HRTimer.__index = HRTimer

function HRTimer.new ()
	local tim = { startTime = 0, stopTime = 0 }
	setmetatable (tim, HRTimer)
	return tim
end

function HRTimer:start ()
	self.startTime = hrt_getTime ()
	return self.startTime
end

function HRTimer:stop ()
	self.stopTime = hrt_getTime ()
	return self.stopTime
end

function HRTimer:duration ()
	return (self.stopTime - self.startTime)
end

function HRTimer:durationStr ()
	return '' .. string.format ("%.16f", self:duration()) .. ' secs'
end

return HRTimer

