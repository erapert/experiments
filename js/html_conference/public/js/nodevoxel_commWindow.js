/*
	The commWindow manages all the video calls
	requires: nodevoxel_utils.js
*/
var namespace = nodevoxel_ns || {};
namespace.CommWindow = function (container, opts) {
	var cfg = Object.assign ({
		newVidWidth: 320,	// width and height for new videos that are added
		newVidHeight: 240
	}, (opts || {}));
	
	var _this = this,
		vidCalls = {};	// handles to the video windows inside me
	
	this.add = function (mediaStream) {
		console.log ('commWindow.add ', mediaStream);
		// vidCalls.mediaStream.id = {};
		// container.append ('<video id="" width="" height="">')
		// video.src = mediaStream;
	}
	
	this.remove = function (streamID) {
		// container.remove (streamID);
		// delete vidCalls.streamID;
	}
};
