extern crate time;

//use time::{ Duration, PreciseTime };


pub struct Timer {
	start_time: time::PreciseTime,
	end_time: time::PreciseTime
}

impl Timer {
	pub fn new () -> Timer {
		let now = time::PreciseTime::now ();
		Timer {
			start_time: now,
			end_time: now
		}
	}
	
	pub fn start (&mut self) {
		self.start_time = time::PreciseTime::now ();
	}
	
	pub fn stop (&mut self) {
		self.end_time = time::PreciseTime::now ();
	}
	
	pub fn time_it <F> (&mut self, closure: F) -> &mut Timer
		where F : Fn () {
		
		self.start();
		closure();
		self.stop();
		
		self
	}
	
	pub fn show (&self) -> &Timer {
		println! ("{}", self.duration_str ());
		
		self
	}
	
	pub fn duration (&self) -> time::Duration {
		self.start_time.to (self.end_time)
	}
	
	pub fn duration_us (&self) -> f64 {
		let d = self.duration();
		(d.num_microseconds().unwrap() as f64 / 1000000 as f64)
	}
	
	pub fn duration_s (&self) -> f64 {
		let d = self.duration();
		d.num_seconds() as f64
	}
	
	pub fn duration_str (&self) -> String {
		format! ("{} seconds", self.duration_s() + self.duration_us())
	}
}
