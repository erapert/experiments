#!/usr/bin/ruby

def get_longest_run str
	prev_char = ''
	max_char_so_far = ''
	max_len_so_far = 0
	count = 1
	
	str.chars.each do |curr_char|
		if curr_char == prev_char
			count += 1
			if count > max_len_so_far
				max_char_so_far = curr_char
				max_len_so_far = count
			end
		else
			count = 1
		end
		prev_char = curr_char
	end
	
	return { :longest_run_char => max_char_so_far, :length_of_run => max_len_so_far }
end


if __FILE__ == $0
	# in ruby command line args don't include the name of the program itself
	if ARGV.length != 1
		puts 'The only arg this program accepts is a string to search for runs'
		exit 1
	end
	
	test_str = ARGV[0]
	longest_run = get_longest_run test_str
	
	puts "Test string: #{test_str}"
	puts "Longest run: #{longest_run}"
	
	exit 0
end
