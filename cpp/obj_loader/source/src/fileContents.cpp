#include <fstream>
#include "fileContents.hpp"
using namespace std;

FileContents::FileContents () {
}

FileContents::FileContents (const FileContents & other) {
	copy (other);
}

FileContents::~FileContents () {
	// contents will clean itself up
}

bool FileContents::load (const char * fname) {
	// must open the file in binary mode otherwise line endings are auto converted
	// and therefore tellg() will become unreliable
	// (offset by the newlines being silently converted)
	ifstream fil (fname, ios_base::in | ios_base::binary);
	if (!fil.is_open ()) { return false; }
	fil.seekg (0, std::ios::end);
	auto fsize = fil.tellg ();
	contents.resize ((unsigned int)fsize);
	fil.seekg (0);
	fil.read (&contents[0], (unsigned int)fsize);
	fil.close ();
	return true;
}

bool FileContents::save (const char * fname) {
	ofstream fil (fname);
	if (!fil.is_open ()) { return false; }
	fil << contents.c_str ();
	fil.close ();
	return true;
}
