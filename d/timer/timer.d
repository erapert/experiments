import std.datetime;
import std.string;

import std.stdio;

class Timer {
	
	void start () {
		watch.reset ();
		watch.start ();
	}
	
	void stop () {
		watch.stop ();
	}
	
	/// returned time is in seconds down to microsecond accuracy
	float duration () {
		return (watch.peek().usecs () / 1000000.0);
	}
	
	string durationStr () {
		return format ("%f secs", duration());
	}
	
	private
		StopWatch watch;
}
