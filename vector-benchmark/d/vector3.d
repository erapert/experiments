import std.stdio;
import std.string;
import std.math;

class Vector3 {
	
	this () {
		set (0, 0, 0);
	}
	
	this (float X = 0, float Y = 0, float Z = 0) {
		set (X, Y, Z);
	}
	
	final void set (float X = 0, float Y = 0, float Z = 0) {
		this.x = X;
		this.y = Y;
		this.z = Z;
	}
	
	final string stringify () {
		return format ("(%f, %f, %f)", x, y, z);
	}
	
	final bool equals (ref Vector3 other) {
		return ((x == other.x) && (y == other.y) && (z == other.z));
	}
	
	final Vector3 plusScalar (float factor) {
		return new Vector3 ((x + factor), (y + factor), (z + factor));
	}
	
	final Vector3 minusScalar (float factor) {
		return new Vector3 ((x - factor), (y - factor), (z - factor));
	}
	
	final Vector3 plus (ref Vector3 other) {
		return new Vector3 ((x + other.x), (y + other.y), (z + other.z));
	}
	
	final Vector3 minus (ref Vector3 other) {
		return new Vector3 ((x + other.x), (y + other.y), (z + other.z));
	}
	
	final Vector3 transform (float X, float Y, float Z) {
		return new Vector3 ((x + X), (y + Y), (z + Z));
	}
	
	final Vector3 scale (float factor) {
		return new Vector3 ((x * factor), (y * factor), (z * factor));
	}
	
	final float length () {
		return sqrt (dot (this));
	}
	
	final float dot (ref Vector3 other) {
		return ((x * other.x) + (y * other.y) + (z * other.z));
	}
	
	final Vector3 cross (ref Vector3 other) {
		return new Vector3 ((x * other.x), (y * other.y), (z * other.z));
	}
	
	final void print_self () {
		writefln (stringify ());
	}
	
	
	float x, y, z;
}
