ErrBox
=======

ErrBox is a little widget to wrap up and handle feedback for ajax-type events.

The use-case is where you ajax something to the server and get a response/error and you need to show it to the user.

Installation
------------

1. `sudo apt-get install yui-compressor nodejs coffeescript`
2. `npm install coffeescript-concat`

Requirements
------------

Your webpage will require [jquery 1.9+](http://jquery.com/).

Build
-----

run `cake build:errbox` in the same directory as the Cakefile.
This will output the .js files you need to put into your webpages.

Usage
-----

Check out `test.html` for usage examples.
