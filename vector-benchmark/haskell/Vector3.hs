module Vector3 where

data Vector3 = Vector3 {
		x :: Float,
		y :: Float,
		z :: Float
	}
	deriving (Eq, Show)

stringify :: Vector3 -> String
stringify v = show v

equals :: Vector3 -> Vector3 -> Bool
equals v o = (v == o)

plusScalar :: Vector3 -> Float -> Vector3
plusScalar v scalar = Vector3 ((x v) + scalar) ((y v) + scalar) ((z v) + scalar)

minusScalar :: Vector3 -> Float -> Vector3
minusScalar v scalar = Vector3 ((x v) - scalar) ((y v) - scalar) ((z v) - scalar)

plus :: Vector3 -> Vector3 -> Vector3
plus v o = Vector3 ((x v) + (x o)) ((y v) + (y o)) ((z v) + (z o))

minus :: Vector3 -> Vector3 -> Vector3
minus v o = Vector3 ((x v) - (x o)) ((y v) - (y o)) ((z v) - (z o))

transform :: Vector3 -> Float -> Float -> Float -> Vector3
transform v xx yy zz = Vector3 ((x v) + xx) ((y v) + yy) ((z v) + zz)

scale :: Vector3 -> Float -> Vector3
scale v f = Vector3 ((x v) * f) ((y v) * f) ((z v) * f)

len :: Vector3 -> Float
len v = sqrt (dot v v)

dot :: Vector3 -> Vector3 -> Float
dot v o = (((x v) * (x o)) + ((y v) * (y o)) + ((z v) * (z o)))

cross :: Vector3 -> Vector3 -> Vector3
cross v o = Vector3
				(((y v) * (z o)) - ((z v) * (y o)))
				(((z v) * (x o)) - ((x v) * (z o)))
				(((x v) * (y o)) - ((y v) * (x o)))
