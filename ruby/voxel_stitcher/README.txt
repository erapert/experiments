ANDREW:
	* install imagemagick from here:
		http://www.imagemagick.org/script/binary-releases.php#windows
	* then run
		"ruby ./stitcher_cli.rb -o output.tga -d dir/full/of/images"


since the popular ruby gems only seem to work on osx and linux we'll just install ImageMagick itself
and use it over the cli:
	WINDOWS:
		http://www.imagemagick.org/script/binary-releases.php#windows

	LINUX:
		sudo apt-get install imagemagick

	IMAGE_MAGICK DOCS:
		http://www.imagemagick.org/Usage/

USAGE:

stitcher_cli [options]
    -o, --output FILENAME            Name of output file.
    -n, --numericsort                Turn on numeric sort (default): files are sorted numerically.
    -a, --alphasort                  Turn on alphabetic sorting: files are sorted alphabetically.
    -l, --nosort                     Turn off sorting, use the files in the order given.
    -d, --dir DIRECTORY              All files in this directory will be stitched together.
										No trailing slash: "./some/directory" NOT "./some/directory/".
    -e, --exe                        Full path to the "montage" executable from ImageMagick.
										use this if montage isn't on your PATH variable
    -s, --size SIZE                  Size of each input image. (256x256 by default)
    -x, --xdim XDIMENSION            How many tiles accross. (is set to result in 16x16 by default)
    -i, --images CSV_IMG_NAMES       Pass image names on the command line. NO SPACES!
										Must look like:
					/full/path/to/image0.jpg,relative/path/image1.jpg,image2.jpg,some_dir/image3.jpg


HOW TO USE:
	
	DEFAULT:
		run "ruby ./stitcher_cli.rb -o output.tga -d dir/full/of/images"
		and output.tga will be written:
			- tiled from top-left to bottom-right
			- in numeric order (NOT alphabetic order)
			- each input image will be resized to 256x256 by default
			
		
		if you had 256 images that are 256x256 inside dir/full/of/images you'd get this in a 4096x4096 image:
			
			0 1 2 3 ... ... ...
			.
			.
			.       254 255 256
		
		basically, everything's set up to do the Right Thing automatically
	
	
	ADVANCED:
		Let's say I want:
			- some specific files(img1.tga,img3.tga,img4.tga,img2.tga)
			- in a specific order: 1,3,4,2
			- with each resized to 512x512 pixels
				(the input files are, say, 256x256)
		
		
		run "ruby ./stitcher_cli.rb --nosort -s 512x512 -o output.tga -i img1.tga,img3.tga,img4.tga,img2.tga"
		
		
		-s means size:
			The input files will be resized to the given size and then tiled into the output.			
			So here you'd wind up with a 1024x1024 output image
		
		--nosort means don't sort file names numerically, and don't sort alphabetically
			So the input files will be tiled in the order you gave:
			1 3
			4 2
		
		-i means that you want to pass a list of files
			Don't put a space between filenames, only a comma!


INCIDENTALLY:
	If you feel like using ImageMagick directly it's not that bad.
	
	Let's say you want to generate a 16 targas with centered sequential numbers in them:
	
	1. run "irb"
	2. set up a range:
		>> r = 0..15
	3. now run a loop:
		>> r.each do |i|
	4. and then leverage ImageMagick's utils:
		>> `convert -gravity center -background "rgba(0,0,0,0)" -size 256x256 -font Monospace -pointsize 128 label:#{i} #{i}.tga`
	5. don't forget to end it:
		>> end
	6. ??? PROFIT!

