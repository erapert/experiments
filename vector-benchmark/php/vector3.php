<?php
	class Vector3 {
		public $x;
		public $y;
		public $z;

		function __construct ($x, $y, $z) {
			$this->set ($x, $y, $z);
		}

		function set ($x, $y, $z) {
			$this->x = $x;
			$this->y = $y;
			$this->z = $z;
		}

		function stringify () {
			return "({$this->x}, {$this->y}, {$this->z})";
		}

		function equals (Vector3 &$other) {
			if (($this->x === $other->x) && ($this->y === $other->y) && ($this->z === $other->z)) {
				return true;
			} else {
				return false;
			}
		}

		function plusScalar ($factor) {
			return new Vector3 (
				($this->x + $factor),
				($this->y + $factor),
				($this->z + $factor)
			);
		}

		function plus (Vector3 &$other) {
			return new Vector3 (
				($this->x + $other->x),
				($this->y + $other->y),
				($this->z + $other->z)
			);
		}

		function minus (Vector3 &$other) {
			return new Vector3 (
				($this->x - $other->x),
				($this->y - $other->y),
				($this->z - $other->z)
			);
		}

		function transform ($x = 0, $y = 0, $z = 0) {
			return new Vector3 (
				($this->x + $x),
				($this->y + $y),
				($this->z + $z)
			);
		}

		function scale ($factor) {
			return new Vector3 (
				($this->x * $factor),
				($this->y * $factor),
				($this->z * $factor)
			);	
		}

		function length () {
			return sqrt($this->dot ($this));
		}

		function dot (Vector3 &$other) {
			return (($this->x * $other->x) + ($this->y * $other->y) * ($this->z * $other->z));
		}

		function cross (Vector3 &$other) {
			return new Vector3 (
				(($this->y * $other->z) - ($this->z * $other->y)),
				(($this->z * $other->x) - ($this->x * $other->z)),
				(($this->x * $other->y) - ($this->y * $other->x))
			);
		}

		function print_self () {
			echo $this->stringify ();
		}
	}
?>