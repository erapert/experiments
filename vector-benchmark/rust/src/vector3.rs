pub struct Vector3 {
	pub x: f32,
	pub y: f32,
	pub z: f32
}

impl Vector3 {
	pub fn stringify (&self) -> String {
		format! ("({}, {}, {})", self.x, self.y, self.z)
	}
	
	pub fn equals (&self, other: &Vector3) -> bool {
		((self.x == other.x) && (self.y == other.y) && (self.z == other.z))
	}
	
	pub fn plus_scalar (&self, factor: f32) -> Vector3 {
		Vector3 { x: (self.x + factor), y: (self.y + factor), z: (self.z + factor) }
	}
	
	pub fn minus_scalar (&self, factor: f32) -> Vector3 {
		Vector3 { x: (self.x - factor), y: (self.y - factor), z: (self.z - factor) }
	}
	
	pub fn plus (&self, other: &Vector3) -> Vector3 {
		Vector3 { x: (self.x + other.x), y: (self.y + other.y), z: (self.z + other.z) }
	}
	
	pub fn minus (&self, other: &Vector3) -> Vector3 {
		Vector3 { x: (self.x - other.x), y: (self.y - other.y), z: (self.z - other.z) }
	}
	
	pub fn transform (&self, xx: f32, yy: f32, zz: f32) -> Vector3 {
		Vector3 { x: (self.x + xx), y: (self.y + yy), z: (self.z + zz) }
	}
	
	pub fn scale (&self, factor: f32) -> Vector3 {
		Vector3 { x: (self.x * factor), y: (self.y * factor), z: (self.z * factor) }
	}
	
	pub fn length (&self) -> f32 {
		(self.dot (self)).sqrt ()
	}
	
	pub fn dot (&self, other: &Vector3) -> f32 {
		((self.x * other.x) + (self.y * other.y) + (self.z * other.z))
	}
	
	pub fn cross (&self, other: &Vector3) -> Vector3 {
		Vector3 {
			x: ((self.y * other.z) - (self.z * other.y)),
			y: ((self.z * other.x) - (self.x * other.z)),
			z: ((self.x * other.y) - (self.y * other.x))
		}
	}
	
	pub fn print_self (&self) {
		println! ("{}", self.stringify ());
	}
}