VRL.ChatWidget = (function () {
	function ChatWidget (args) {
		var _this = this;
		this.config = $.extend ({ width: 512, height: 512 }, args);
		if (!this.config.containerID || !this.config.socket) {
			throw 'containerID and socket are both required.';
		}
		
		this.$container = buildUI (this.config);
		this.$chatLog = this.$container.find ('#chat_log');
		this.$chatInput = this.$container.find ('#chat_input');
		this.$chatSendBtn = this.$container.find ('#chat_send');
		
		this.$chatInput.on ('keypress', function (evt) {
			if (evt.charCode == 13) {
				_this.sendChat (this.value);
				this.value = '';
			}
		});
		this.$chatSendBtn.on ('click', function (evt) {
			_this.sendChat (_this.$chatInput.val ());
			_this.$chatInput.val ('');
		});
		
		var s = this.config.socket;
		s.on ('chat', function (data) {
			_this.logChat (data);
		});
	}
	
	ChatWidget.prototype.sendChat = function (msg) {
		//console.log ('send: ', msg);
		this.config.socket.emit ('chat', { msg: msg });
	}
	
	ChatWidget.prototype.logChat = function (data) {
		//console.log ('recieved: ', data);
		this.$chatLog.append ('<li>' + data.msg + '</li>');
		this.$chatLog.scrollTop (this.$chatLog[0].scrollHeight);
	}
	
	function buildUI (args) {
		$('head').append ('\
			<style>\
				#chat_log_container {\
					background-color: #303030;\
					width: ' + args.width + ';\
					height: ' + args.height + ';\
				}\
				#chat_log {\
					list-style-type: none;\
					padding: 0px;\
					margin: 0px;\
					height: 100%;\
					overflow-y: scroll;\
				}\
				#chat_log li:nth-child(odd) { background-color: #404040; }\
				#chat_input {\
					border: 0px;\
					margin: 0px;\
					background-color: #505050;\
					width: 472px;\
				}\
				#chat_send {\
					border: none;\
					margin: 0px;\
					padding: 1px 4px 1px 2px;\
				}\
			</style>\
		');
		
		var $c = $(args.containerID);
		$c.append ('\
			<div id="chat_log_container">\
				<ul id="chat_log">\
				</ul>\
			</div>\
			<input type="text" id="chat_input">\
			<input type="button" id="chat_send" value="Send">\
		');
		
		return $c;
	}
	
	return ChatWidget;
})();