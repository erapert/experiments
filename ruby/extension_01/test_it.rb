#!/usr/bin/ruby

# import the .so file
require 'stringtest'

# import the module so we can get its functions
include StringTest

hi_c = "Hello, C. You're so damned fast."

puts "Hi there C. Say hello: #{StringTest::say_hi}"
puts "Now repeat after me: #{hi_c}"
StringTest::print_str(hi_c)
