#include <iostream>
using namespace std;

#include "fileContents.hpp"

int main () {
	FileContents fc;
	fc.load ("foo.txt");
	cout << (const char *) fc << endl;
	cout << "---------" << endl;
	cout << fc << endl;
	fc.save ("bar.txt");
	return 0;
}