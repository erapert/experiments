var Vector3 = module.exports = function Vector3 (X, Y, Z) {
	this.set (X, Y, Z);
}

Vector3.prototype.set = function (X, Y, Z) {
	this.x = X;
	this.y = Y;
	this.z = Z;
}

Vector3.prototype.toString = function () {
	return '(' + this.x + ', ' + this.y + ', ' + this.z + ')';
}

Vector3.prototype.equals = function (other) {
	return ((this.x == other.x) && (this.y == other.y) && (this.z == other.z))
}

Vector3.prototype.plusScalar = function (factor) {
	return new Vector3 ((this.x + factor), (this.y + factor), (this.z + factor));
}

Vector3.prototype.minusScalar = function (factor) {
	return new Vector3 ((this.x - factor), (this.y - factor), (this.z - factor));
}

Vector3.prototype.plus = function (other) {
	return new Vector3 ((this.x + other.x), (this.y + other.y), (this.z + other.z));
}

Vector3.prototype.minus = function (other) {
	return new Vector3 ((this.x - other.x), (this.y - other.y), (this.z - other.z));
}

Vector3.prototype.transform = function (X, Y, Z) {
	return new Vector3 ((this.x + X), (this.y + Y), (this.z + Z));
}

Vector3.prototype.scale = function (factor) {
	return new Vector3 ((this.x * factor), (this.y * factor), (this.z * factor));
}

Vector3.prototype.length = function () {
	return Math.sqrt (this.dot (this));
}

Vector3.prototype.dot = function (other) {
	return ((this.x * other.x) + (this.y * other.y) + (this.z * other.z));
}

Vector3.prototype.cross = function (other) {
	return new Vector3 (
		((this.y * other.z) - (this.z * other.y)),
		((this.z * other.x) - (this.x * other.z)),
		((this.x * other.y) - (this.y * other.x))
	);
}

Vector3.prototype.print_self = function () {
	console.log (this);
}
