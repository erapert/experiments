#!/usr/bin/env node

/*
	runs on nodejs v0.8.17+
*/

function unit_test () {
	var Vector3 = require ('./vector3');
	
	var t0 = new Vector3 (1, 1, 1);
	var t1 = new Vector3 (1, 1, 1);
	var t2 = new Vector3 (2, 2, 2);
	
	console.log ('t0 == t1: ' + t0.equals(t1));
	console.log ('t1 == t2: ' + t1.equals(t2));
	
	t0.set (2, 2, 2);
	console.log ('t0: ' + t0);
	t1.set (8, 8, 8);
	console.log ('t1: ' + t1);
	t2.set (16, 16, 16);
	console.log ('t2: ' + t2);
	
	var temp = t0.plusScalar (2);
	console.log ('t0 + 2 = ' + temp);
	
	console.log ('t0.plus(t1): ' + t0.plus(t1));
	console.log ('t2.minus(t1): ' + t2.minus(t1));
	
	console.log ('t0.transform(0, 8, 0): ' + t0.transform(0, 8, 0));
	
	console.log ('t0.scale 0.5: ' + t0.scale(0.5));
	
	console.log ('t1.length: ' + t1.length());
	console.log ('t2.length: ' + t2.length());
	
	console.log ('t0.dot(t1): ' + t0.dot(t1));
	
	console.log ('t1.cross(t2): ' + t1.cross(t2));
	
	t1.set (1, 2, 3);
	t2.set (4, 5, 6);
	console.log ('t1.set(1, 2, 3): ' + t1);
	console.log ('t2.set(4, 5, 6): ' + t2);
	console.log ('t1.cross(t2): ' + t1.cross(t2));
}

// helper for printing time in seconds
function print_stats (stats) {
	for (stat in stats) {
		if (typeof (stats[stat]) == 'number') {
			console.log (stat + ': ' + stats[stat].toFixed(20));
		}
	}
}
function print_mean (stats) {
	process.stdout.write (' avg: ' + stats['mean'].toFixed(20) + ' secs\n');
}

// requires benchmark.js: " npm install benchmark "
function bench_mark () {
	var Vector3 = require ('./vector3');
	var Benchmark = require ('benchmark');
	var bm = new Benchmark.Suite;
	
	var t0 = new Vector3 (1, 2, 3);
	var t1 = new Vector3 (4, 5, 6);
	
	console.log ('timing individual operations...');
	bm.add ('Vector3#stringify', function () {
		var temp = t0.stringify ();
	}).add ('Vector3#equals', function () {
		var temp = t0.equals (t1);
	}).add ('Vector3#plusScalar', function () {
		var temp = t0.plusScalar (10.0);
	}).add ('Vector3#minusScalar', function () {
		var temp = t0.minusScalar (10.0);
	}).add ('Vector3#plus', function () {
		var temp = t0.plus (t1);
	}).add ('Vector3#minus', function () {
		var temp = t0.minus (t1);
	}).add ('Vector3#transform', function () {
		var temp = t0.transform (0, 8, 0);
	}).add ('Vector3#scale', function () {
		var temp = t0.scale (0.5);
	}).add ('Vector3#length', function () {
		var temp = t0.length ();
	}).add ('Vector3#dot', function () {
		var temp = t0.dot (t1);
	}).add ('Vector3#cross', function () {
		var temp = t0.cross (t1);
	}).on ('complete', function () {
		for (each in bm) {
			if (!isNaN (each)) {
				process.stdout.write (bm[each]['name']);
				print_mean (bm[each]['stats']);
			}
		}
	}).run ({ async: false });
	
	console.log ('Doing time trial (all ops in a row)');
	/*
		For some reason node.js runs through this loop EXTREMELY fast (three times faster than C++!!!!)
		It appears that node.js is doing some sneaky run-time optimization where it's cutting out almost all
		of the work in this loop (for example, temp always winds up set to t0.cross(t1) so why bother
		doing things like temp = t0.plus(t1)?).
		
		Intriguing, but agravating.
	*/
	var timeTrial = function () {
		var temp = null;
		for (var i = 0; i < 1000000; ++i) {
			t0.set (1, 2, 3);
			temp = t0.stringify ();
			temp = t0.equals (t1);
			temp = t0.plusScalar (10.0);
			temp = t0.minusScalar (10.0);
			temp = t0.plus (t1);
			temp = t0.minus (t1);
			temp = t0.transform (0, 8, 0);
			temp = t0.scale (0.5);
			temp = t0.length ()
			temp = t0.dot (t1);
			temp = t0.cross (t1);
		}
	}
	bm = new Benchmark ('Time Trial', timeTrial, {
		onComplete: function () {
			console.log (bm.name + ': ' + bm.times.elapsed.toFixed(20));
		}
	});
	bm.run ({ async: false });
}


// requires argparse: " npm install argparse "
(function () {
	var ArgumentParser = require ('argparse').ArgumentParser;
	
	var parser = new ArgumentParser ({
		version: '1.0',
		addHelp: true,
		description: 'Test vector3.js',
		epilog: 'The benchmark may take much longer to complete than, say, the php benchmark. But be patient.'
	});
	
	parser.addArgument (['-u', '--unittest'], {
		help: 'run and print the unit test',
		action: 'storeTrue'
	});
	parser.addArgument (['-b', '--benchmark'], {
		help: 'run the benchmark',
		action: 'storeTrue'
	});
	
	var args = parser.parseArgs ();
	
	if (process.argv.length == 2) {
		console.log (parser.printHelp());
	}
	if (args.unittest) {
		unit_test ();
	}
	if (args.benchmark) {
		bench_mark ();
	}
})();
