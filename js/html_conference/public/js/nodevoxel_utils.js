/*
	we don't want a jquery dependency in our libs
	but we do find $.extend to be useful
	but we also don't want to conflict with ES6
	
	http://youmightnotneedjquery.com/
	http://www.2ality.com/2014/01/object-assign.html
*/
Object.assign = Object.assign || function (out) {
	out = out || {};

	for (var i = 1; i < arguments.length; i++) {
		if (!arguments[i]) continue;

		for (var key in arguments[i]) {
			if (arguments[i].hasOwnProperty(key)) {
				out[key] = arguments[i][key];
			}
		}
	}

	return out;
};
