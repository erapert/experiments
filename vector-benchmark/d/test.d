import std.stdio;
import std.getopt; // requires gdc 4.6+
import std.datetime;

import core.thread;

import vector3;
import timer;

void unit_test () {
	writefln ("unit_test...");
	
	auto t0 = new Vector3 (1, 1, 1);
	auto t1 = new Vector3 (1, 1, 1);
	auto t2 = new Vector3 (2, 2, 2);
	
	writefln ("t0 == t1: %b", t0.equals (t1));
	writefln ("t1 == t2: %b", t1.equals (t2));
	
	t0.set (2, 2, 2);
	writefln ("t0: %s", t0.stringify ());
	t1.set (8, 8, 8);
	writefln ("t1: %s", t1.stringify ());
	t2.set (16, 16, 16);
	writefln ("t2: %s", t2.stringify ());
	
	Vector3 temp = t0.plusScalar (2);
	writefln ("t0 + 2 = %s", temp.stringify ());
	
	writefln ("t0.plus(t1): %s", t0.plus(t1).stringify ());
	writefln ("t2.minus(t1): %s", t2.minus(t1).stringify ());
	
	writefln ("t0.transform(0, 8, 0): %s", t0.transform (0, 8, 0).stringify ());
	
	writefln ("t0.scale(0.5): %s", t0.scale (0.5).stringify ());
	
	writefln ("t1.length: %f", t1.length ());
	writefln ("t2.length: %f", t2.length ());
	
	writefln ("t0.dot(t1): %f", t0.dot(t1));
	
	writefln ("t1.cross(t2): %s", t1.cross(t2).stringify());
	
	t1.set (1, 2, 3);
	t2.set (4, 5, 6);
	writefln ("t1.set(1, 2, 3): %s", t1.stringify ());
	writefln ("t2.set(4, 5, 6): %s", t2.stringify ());
	writefln ("t1.cross(t2): %s", t1.cross(t2).stringify ());
}

void benchmark () {
	Timer t = new Timer;
	
	Vector3 t0 = new Vector3 (1, 2, 3);
	Vector3 t1 = new Vector3 (4, 5, 6);
	
	writefln ("timing individual operations...");
	
	string tempStr;
	Vector3 tempV = new Vector3;
	float tempF;
	bool tempB;
	
	t.start ();
	t0.set (1, 2, 3);
	t.stop ();
	writefln ("set: %s", t.durationStr());
	
	t.start ();
	t.stop ();
	writefln ("equals: %s", t.durationStr());
	
	t.start ();
	t.stop ();
	writefln ("stringify: %s", t.durationStr());
	
	t.start ();
	tempV = t0.plusScalar (10.0);
	t.stop ();
	writefln ("plusScalar: %s", t.durationStr());
	
	t.start ();
	tempV = t0.minusScalar (10.0);
	t.stop ();
	writefln ("minusScalar: %s", t.durationStr());
	
	t.start ();
	tempV = t0.plus (t1);
	t.stop ();
	writefln ("plus: %s", t.durationStr());
	
	t.start ();
	tempV = t0.minus (t1);
	t.stop ();
	writefln ("minus: %s", t.durationStr());
	
	t.start ();
	tempV = t0.transform (0, 8, 0);
	t.stop ();
	writefln ("transform: %s", t.durationStr());
	
	t.start ();
	tempV = t0.scale (0.5);
	t.stop ();
	writefln ("scale: %s", t.durationStr());
	
	t.start ();
	tempF = t0.length ();
	t.stop ();
	writefln ("length: %s", t.durationStr());
	
	t.start ();
	tempF = t0.dot (t1);
	t.stop ();
	writefln ("dot: %s", t.durationStr());
	
	t.start ();
	tempV = t0.cross (t1);
	t.stop ();
	writefln ("cross: %s", t.durationStr());
	
	writefln ("Doing time trial (all ops in a row)");
	t.start ();
	for (int i = 0; i < 1000000; ++i) {
		t0.set (1, 2, 3);
		tempB = t0.equals (t1);
		tempStr = t0.stringify ();
		tempV = t0.plusScalar (10.0);
		tempV = t0.minusScalar (10.0);
		tempV = t0.plus (t1);
		tempV = t0.minus (t1);
		tempV = t0.transform (0, 8, 0);
		tempV = t0.scale (0.5);
		tempF = t0.length ();
		tempF = t0.dot (t1);
		tempV = t0.cross (t1);
	}
	t.stop ();
	writefln ("time trial: %s", t.durationStr());
}


void printHelp () {
	writefln ("usage:");
	writefln ("-h	:	print this help message");
	writefln ("-u	:	run unit test");
	writefln ("-b	:	run benchmark");
}


void main (string [] args) {
	bool doUnitTest, doBenchmark, needHelp;
	
	if (args.length < 2) {
		printHelp ();
		return;
	}
	
	// getopt() pops args off the array, so watch out
	getopt (args,
		"u", &doUnitTest,
		"b", &doBenchmark,
		"h", &needHelp
	);
	
	if (needHelp) {
		printHelp ();
		return;
	}
	
	if (doUnitTest)
		unit_test ();
	
	if (doBenchmark)
		benchmark ();
}
