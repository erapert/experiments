class Vector3
	attr_accessor :x, :y, :z
	
	def initialize x, y, z
		set x, y, z
	end
	
	def set x, y, z
		@x = x
		@y = y
		@z = z
	end
	
	def stringify
		return "(#{x}, #{y}, #{z})"
	end
	
	def equals? other
		return ((@x == other.x) and (@y == other.y) and (@z == other.z))
	end
	
	def plusScalar factor
		return Vector3.new((@x + factor), (@y + factor), (@z + factor))
	end
	
	def minusScalar factor
		return Vector3.new((@x - factor), (@y - factor), (@z - factor))
	end
	
	def plus other
		return Vector3.new((@x + other.x), (@y + other.y), (@z + other.z))
	end
	
	def minus other
		return Vector3.new((@x - other.x), (@y - other.y), (@z - other.z))
	end
	
	def transform x = 0, y = 0, z = 0
		return Vector3.new((@x + x), (@y + y), (@z + z))
	end
	
	def scale factor
		return Vector3.new((@x * factor), (@y * factor), (@z * factor))
	end
	
	def length
		return Math.sqrt(dot(self))
	end
	
	def dot other
		return ((@x * other.x) + (@y * other.y) + (@z * other.z))
	end
	
	def cross other
		return Vector3.new(
			((@y * other.z) - (@z * other.y)),
			((@z * other.x) - (@x * other.z)),
			((@x * other.y) - (@y * other.x))
		)
	end
	
	def print_self
		puts stringify
	end
end
