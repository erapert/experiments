#ifndef VECTOR3_H
#define VECTOR3_H

// you need to install boost for the string interpolation in stringify()
// " sudo apt-get install libboost-all-dev "
#include <boost/lexical_cast.hpp>
// stringify() and print_self() need these
#include <iostream>
#include <string>
// sqrt()
#include <math.h>

using namespace std;

/*
	of course I could override things like operator== and operator+
	but I'm trying to keep things as close to ruby, python, and php
	as possible and those languages disallow operator overloading
*/
class Vector3 {
	public:
		Vector3						();
		Vector3						(const float & X, const float & Y, const float & Z);
		Vector3						(const Vector3 & other);
		~Vector3					();
		
		Vector3 &	operator =		(const Vector3 & other);
		Vector3 &	copy			(const Vector3 & other);
		
		void		set				(const float & X, const float & Y, const float & Z);
		string		stringify		();
		bool		equals			(const Vector3 & other);
		Vector3		plusScalar		(const float & factor);
		Vector3		minusScalar		(const float & factor);
		Vector3		plus			(const Vector3 & other);
		Vector3		minus			(const Vector3 & other);
		Vector3		transform		(float X = 0, float Y = 0, float Z = 0);
		Vector3		scale			(const float & factor);
		float		length			() const;
		float		dot				(const Vector3 & other) const;
		Vector3		cross			(const Vector3 & other);
		void		print_self		();
		
	public:
		float x;
		float y;
		float z;
};


inline Vector3::Vector3 () {
	set (0.0, 0.0, 0.0);
}
inline Vector3::Vector3 (const float & X, const float & Y, const float & Z) {
	set (X, Y, Z);
}
inline Vector3::Vector3 (const Vector3 & other) {
	copy (other);
}
inline Vector3::~Vector3 () {}

inline Vector3 & Vector3::operator = (const Vector3 & other) {
	return copy (other);
}
inline Vector3 & Vector3::copy (const Vector3 & other) {
	set (other.x, other.y, other.z);
	return * this;
}

inline void Vector3::set (const float & X, const float & Y, const float & Z) {
	x = X;
	y = Y;
	z = Z;
}

inline string Vector3::stringify () {
	string rtrn;
	rtrn =
		"(" +
		boost::lexical_cast<string>(x) +
		", " +
		boost::lexical_cast<string>(y) +
		", " +
		boost::lexical_cast<string>(z) +
		")";
	return rtrn;
}
inline bool Vector3::equals (const Vector3 & other) {
	return ((x == other.x) && (y == other.y) && (z == other.z)) ? true : false;
}
inline Vector3 Vector3::plusScalar (const float & factor) {
	return Vector3 ((x + factor), (y + factor), (z + factor));
}
inline Vector3 Vector3::minusScalar (const float & factor) {
	return Vector3 ((x - factor), (y - factor), (z - factor));
}
inline Vector3 Vector3::plus (const Vector3 & other) {
	return Vector3 ((x + other.x), (y + other.y), (z + other.z));
}
inline Vector3 Vector3::minus (const Vector3 & other) {
	return Vector3 ((x - other.x), (y - other.y), (z - other.z));
}
inline Vector3 Vector3::transform (float X, float Y, float Z) {
	return Vector3 ((x + X), (y + Y), (z + Z));
}
inline Vector3 Vector3::scale (const float & factor) {
	return Vector3 ((x * factor), (y * factor), (z * factor));
}
inline float Vector3::length () const {
	return sqrt (dot (*this));
}
inline float Vector3::dot (const Vector3 & other) const {
	return ((x * other.x) + (y * other.y) + (z * other.z));
}
inline Vector3 Vector3::cross (const Vector3 & other) {
	return Vector3 ((x * other.x), (y * other.y), (z * other.z));
}
inline void Vector3::print_self () {
	cout << stringify () << endl;
}

#endif