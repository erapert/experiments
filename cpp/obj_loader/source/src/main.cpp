#include <iostream>
using namespace std;

#include "oglMeshUtils.hpp"

int main () {
	OGLMesh mesh;
	if (!OGLMeshUtils::load ("models/cube.obj", mesh)) {
		cout << "Failed to load." << endl;
	}
	cout << "mesh: " << endl;
	OGLMeshUtils::printMesh (mesh);
	return 0;
}