#!/usr/bin/python

class Logger ():
	def __init__ (self, indentSize=1, enableColor=True):
		self.ind = 0				# not indented to start with
		self.enableColor = enableColor
		self.indSize = indentSize	# how many spaces is one indent
		self.colors = {
			'[.]': ('\033[90m',	'grey'),
			'[r]': ('\033[91m', 'red'),
			'[g]': ('\033[92m', 'green'),
			'[y]': ('\033[93m', 'yellow'),
			'[b]': ('\033[94m', 'blue'),
			'[m]': ('\033[95m', 'magenta'),
			'[c]': ('\033[96m', 'cyan'),
			'[w]': ('\033[97m', 'white'),
			'[/]': ('\033[0m', 'end color/reset')
		}
		self.endColor = self.colors['[/]'][0]
	
	def log (self, msg):
		i = self._getIndentStr ()
		if (self.enableColor):
			m = self._colorize (msg)
			print ("%s%s%s" % (i, m, self.endColor))
		else:
			print ("%s%s" % (i, msg))
		return self
	
	def indent (self, i):
		self.ind = self.ind + i
		if (self.ind < 0):
			self.ind = 0
		return self
	
	def resetIndent (self):
		self.ind = 0
		return self
	
	def setIndent (self, i):
		self.ind = i
		if (self.ind < 0):
			self.ind = 0
		return self
	
	def _colorize (self, msg):
		rtrn = msg
		for color in self.colors:
			rtrn = rtrn.replace (color, self.colors[color][0])
		return rtrn
	
	def _getIndentStr (self):
		return (' ' * (self.ind * self.indSize))
	
	def _showColorCodes (self):
		i = self._getIndentStr ()
		for code in self.colors:
			print ("%s'%s' = %s%s%s" % (i, code, self.colors[code][0], self.colors[code][1], self.endColor))



if __name__ == '__main__':
	# You can set how many spaces are used for display and you can turn off color
	l = Logger (indentSize=2, enableColor=False)
	
	# don't worry, you can enable color later:
	l.enableColor = True
	
	l._showColorCodes ()
	#	'[.]' = '\033[90m' # grey
	#	'[r]' = '\033[91m' # red
	#	'[g]' = '\033[92m' # green
	#	'[y]' = '\033[93m' # yellow
	#	'[b]' = '\033[94m' # blue
	#	'[m]' = '\033[95m' # magenta
	#	'[c]' = '\033[96m' # cyan
	#	'[w]' = '\033[97m' # white
	#	'[/]' = '\033[0m'  # end color/reset
	
	# you can chain the public methods (except for the constructor which Python restrics)
	l.resetIndent ().log ('Codes will be replaced with the proper ANSI escape sequences-- [c]they[y]do[r]not[g]turn[b]into[w]whitespace.[/]')
	
	# by the way, disabling color should run faster since it's no longer inserting the color codes
	l.enableColor = False
	l.log ('If you forget to use `[/]` to end a color')
	l.enableColor = True
	l.indent (1).log ('[b]it will run \'till it finds a new color tag or to the end of the string.')
	l.indent (-1).log ('Because of this you don\'t have to terminate color before setting to a new color. Check this out:')
	l.indent (1).log ('[r]RED [g]GREEN [b]BLUE[/] And this is uncolored.')
	
	l.resetIndent ().log ('Setting indentation to less than zero simply resets to zero:')
	l.indent (5).log ('Indented to 5.')
	l.setIndent (-1000).log ('Indented to -1000.')
	
	# manually reseting the console colors in case testing screwed up
	print ('\033[0m')

