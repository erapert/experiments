High Resolution Time
====================

Lua's built-in `os.time` only goes down to seconds. For benchmarks I needed down to the millisecond at least. So I decided to dig into Lua's C API and write the module for myself.

Building/Installing
-------------------
1. Be sure you have the build tools necessary on your system: gcc, lua libs and headers.
2. You'll need Lua 5.2+. Ubuntu comes with 5.0 which is incompatible. You'll want to uninstall whatever apt-get gave you and go grab the [source](http://www.lua.org/ftp/lua-5.2.2.tar.gz)  yourself.
3. Build and install Lua 5.2.
	It should be something like this inside the Lua source directory `make; sudo make install`
4. Alrighty, now build and install the hi-rez-timer by doing this from inside the "hi_rez_timer" directory: `make; sudo make install`

Usage
-----
See `test.lua`. In general you'll want to do something like this:
```lua
-- howLong is number of seconds to "work" for
function pretendToWork (howLong)
	os.execute ("sleep " .. tonumber (howLong))
end

function testMe ()
	local timer = require 'hrtimer'
	
	timer:start ()
	pretend_to_work (3)
	timer:stop ()
	print ('raw duration: ' .. timer:duration())
	print ('work took ' .. timer:durationStr ())
end
```

Notes
-----
The `Makefile` contains commands that you may find useful. Run them like so: `make install` or `make uninstall`

+ `install` Installs the C module to `/usr/local/lib/lua/5.2/` and the Lua module to `/usr/local/share/lua/5.2/` by default. If you need them installed to different places then look into the `Makefile` and change the `LUA_LIB_PATH` to where the .lua module should go, and change `LUA_CLIB_PATH` to where the .so file should go.
+ `uninstall` deletes the .lua and .so files from wherever `LUA_LIB_PATH` and `LUA_CLIB_PATH` are set to.
+ `clean` destroys the .o and .so files that are output by `make`.
+ Of course, just running `make` on its own will compile the C module.

License
-------
This project and all its source code are covered by the **I just wanna share** license which is as follows:
	<pre>
	Do what you want with this program and its source code--
	I ain't gonna stop you-- but please give me credit for the
	code that I wrote.
	</pre>
In addition I try to comment code that I didn't write or that I pulled off the internet somewhere. Obviously, if I put a comment attributing credit to somebody then... well... that credit belongs to them not me!

