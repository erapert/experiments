if RUBY_VERSION == '1.8.7'
	require File.join(File.dirname(__FILE__), 'console_notifier.rb')
else
	require_relative 'console_notifier.rb'
end

# helper for gathering a list of file names and making sure they exist etc.
class FHelper
	public
		# args:
		#	string names: comma-separated paths to files (absolute or relative)
		# returns:
		#	array: array containing names of all the images
		def self.collect_arg_filenames(names)
			files = names.split ','
		
			if !files_valid? files
				ConsoleNotifier.error 'aborted.'
				exit(1)
			else
				return files
			end
		end

		# args:
		#	string dirname: path to a directory containing image files to stitch together (absolute or relative)
		# returns:
		#	array: array containing names of all the files in this dir or nil if failed
		def self.collect_dir_filenames(dirname)
			if !File.directory? dirname
				ConsoleNotifier.error "Directory doesn't seem to exist: '#{dirname}'"
				exit(1)
			end
			
			files = Dir.glob "#{dirname}/*"
		
			if files.length == 0
				ConsoleNotifier.error "No images found in '#{dirname}'"
				exit(1)
			end
			if !files_valid? files
				ConsoleNotifier.error 'aborted.'
				exit(1)
			else
				return files
			end
		end
	
	private
		# regexp to match magic bytes of image files
		# if no match then the file isn't a supported image file
		#MAGIC_BYTES = [
		#	/\x42\x4d/,			# bitmap
		#	/\x89\x50\x4e\x47/,	# png
		#	/\002/,				# targa
		#	/\xff\xd8\xff\xe0/,	# jpeg
		#	/\x4d\x4d\x00\x2a/,	# big-endian tif
		#	/\x49\x49\x2a\x00/	# little-endian tif
		#]
		
		# args:
		#	array file_names: array of file names-- can be absolute or relative to the current dir
		# returns:
		#	bool:	true if the files exist, false otherwise
		def self.files_valid?(file_names)
			invalid_files = []
		
			file_names.each do |fname|
				if !File.exists? fname
					invalid_files << fname
				end
				
				# now check to make sure this is a valid image file
				magic_bytes = IO.read fname, 10
				case magic_bytes
					when /\x42\x4d/,		# bitmap
						/\x89\x50\x4e\x47/,	# png
						/\002/, /\n/,		# targa
						/\xff\xd8\xff\xe0/,	# jpeg
						/\x4d\x4d\x00\x2a/,	# big-endian tif
						/\x49\x49\x2a\x00/	# little-endian tif
					else
						invalid_files << fname
				end
			end
		
			if invalid_files.length > 0
				invalid_files.each do |fname|
					ConsoleNotifier.error "File doesn\'t exist or is not an acceptable image: \e[33m'#{fname}'\e[0m"
				end
				return false
			else
				return true
			end
		end
end
