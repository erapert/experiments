class ConsoleNotifier
	# red
	def self.error(msg)
		puts "\e[31m!ERROR!\e[0m #{msg}"
	end
	
	# yellow
	def self.warn(msg)
		puts "\e[33m!WARNING!\e[0m #{msg}"
	end
	
	# green, but with emphasis
	def self.important(msg, msg_head = 'IMPORTANT')
		puts "\e[1m\e[32m#{msg_head}:\e[0m \e[37m#{msg}\e[0m"
	end
	
	# cyan
	def self.notify(msg)
		puts "\e[36m>>\e[0m #{msg}"
	end
end
