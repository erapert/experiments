#
#	requires
#		+ JQuery
#		+ You need to define "var VRL = VRL || {};" somewhere before this file is included
#
class VRL.Alerter
	#
	#	args:
	#		alertBoxId:	jquery selector for the id where you want alerts inserted: '#someDiv'
	#		unit:		string indicating time unit: 'secs' || 'mins' || 'hours'
	#		alerts:		array of alerts to be shown: [{ time: 123, msg: 'something' }]
	#
	constructor: (alertBoxId, unit, alerts) ->
		@alertBox = $(alertBoxId)
		if unit and alerts
			this.set unit, alerts
	
	_set: (alert, factor) ->
		setTimeout(() ->
			console.log alert.msg
			# TODO: insert the alert msg into @alertBox
		, alert.time * factor)
	
	#
	#	args:
	#		unit: string indicating the time unit to use (secs, mins, hours)
	#
	_getUnitFactor: (unit) ->
		switch unit
			when 'secs' then return 1000
			when 'mins' then return 1000 * 60
			when 'hours' then return 1000 * 60 * 60
			else throw 'Invalid unit. Valid units are "secs", "mins", "hours"'
	
	set: (unit, alerts) ->
		if Object.prototype.toString.call(alerts) == '[object Array]'
			for alert in alerts
				this._set alert, this._getUnitFactor unit
		else
			this._set alerts, this._getUnitFactor unit
