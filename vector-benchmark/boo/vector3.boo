namespace VRL

import System
import Math

class Vector3:
	[Property(X)]
	x as single
	
	[Property(Y)]
	y as single
	
	[Property(Z)]
	z as single
	
	def constructor ():
		x = X
		y = Y
		z = Z
	
	def set (X, Y, Z):
		x = X
		y = Y
		z = Z
	
	def stringify ():
		return "($(x), $(y), $(z))"
		
	def equals (other as Vector3):
		return ((x == other.x) and (y == other.y) and (z == other.z))
	
	def plusScalar (factor as single):
		return Vector3 (X: (x + factor), Y: (y + factor), Z: (z + factor))
	
	def minusScalar (factor as single):
		return Vector3 (X: (x - factor), Y: (y - factor), Z: (z - factor))
	
	def plus (other as Vector3):
		return Vector3 (X: (x + other.x), Y: (y + other.y), Z: (z + other.z))
	
	def minus (other as Vector3):
		return Vector3 (X: (x - other.x), Y: (y - other.y), Z: (z - other.z))
	
	def transform (x as single, y as single, z as single):
		return Vector3 (X: (x + x), Y: (y + y), Z: (z + z))
	
	def scale (factor as single):
		return Vector3 (X: (x * factor), Y: (y * factor), Z: (z * factor))
	
	def length ():
		return Math.Sqrt (dot (self))
	
	def dot (other as Vector3):
		return ((x * other.x) + (y * other.y) + (z * other.z))
	
	def cross (other as Vector3):
		return Vector3(
			X: ((y * other.z) - (z * other.y)),
			Y: ((z * other.x) - (x * other.z)),
			Z: ((x * other.y) - (y * other.x))
		)
	
	def print_self ():
		print (stringify ())

