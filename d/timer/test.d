import std.stdio;
import core.thread;
import std.datetime;

import timer;

void main () {
	writefln ("timer test:");
	
	Timer t = new Timer;
	StopWatch sw;
	auto sleepTime = 10;
	
	writefln ("sleeping for %d with stop watch...", sleepTime);
	sw.start ();
	Thread.sleep (dur!("seconds")(sleepTime));
	sw.stop ();
	writefln ("slept for %f secs", (sw.peek().usecs() / 1000000.0));
	sw.reset ();
	
	writefln ("sleeping for %d with timer...", sleepTime);
	t.start ();
	Thread.sleep (dur!("seconds")(sleepTime));
	t.stop ();
	writefln ("slept for: %s", t.durationStr ());
	
	writefln ("sleeping for %d with stop watch...", sleepTime);
	sw.start ();
	Thread.sleep (dur!("seconds")(sleepTime));
	sw.stop ();
	writefln ("slept for %f secs", (sw.peek().usecs() / 1000000.0));
	sw.reset ();
	
	writefln ("sleeping for %d with timer...", sleepTime);
	t.start ();
	Thread.sleep (dur!("seconds")(sleepTime));
	t.stop ();
	writefln ("slept for: %s", t.durationStr ());
}
