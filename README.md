Code Experiments
===========

Random experiments and little one-offs

Items of Interest
-----------------
* **vector_benchmark**: A bunch of benchmarks programs written in various languages. The idea isn't to see if I could write the most efficient possible code in each language. Rather, to see the relative performance of my natural implementation style in each language.
* **color_logger**: A simple, not fast, logger that supports ASCII color implemented in various languages.
* The various other directories contain little one-offs, plugins, or utilities.


Notes
-----
* Generally if something doesn't work right out of the box there'll be a *readme* or some kind of instructions or comments for how to get it to run or build-- if you have make and g++ `make` is your friend.

* Frequently a program or project in here will just plain **not work**. That happens if I get bored or get dragged away to work on something else. If you feel like contributing then, by all means, take the code and run with it.

License
-------
This project and all its source code are covered by the **I just wanna share** license which is as follows:
	<pre>
	Do what you want with this program and its source code--
	I ain't gonna stop you-- but please give me credit for the
	code that I wrote.
	</pre>
In addition I try to comment code that I didn't write or that I pulled off the internet somewhere. Obviously, if I put a comment attributing credit to somebody then... well... that credit belongs to them not me!
