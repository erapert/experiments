Alerter
=======

Alert wrapper/manager written in Coffeescript/Javascript that handles doing alerts on a web-page.

Basically, you init an Alerter with an array of times and it'll do the rest-- pops up a little alert box when the time has passed.

Installation
------------

1. `sudo apt-get install yui-compressor nodejs coffeescript`
2. `npm install coffeescript-concat`

Build
-----

run `cake build:alerter` from the top level directory.

Usage
-----

In your HTML:

```html
<html>
	<head>
		<!-- requires jquery -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
		<!-- VRL serves as a namespace -->
		<script> var VRL = VRL || {}; </script>
		<!-- the alerter must be included AFTER the above -->
		<script src="out/alerter.js"></script>
	</head>
	<body>
		<div id="alerts">
			<!-- pass this id to the constructor and alerts will be inserted here -->
		</div>
		<script>
			$(document).ready (function () {
				// the numbers are seconds to wait for the alert
				var alerter = new VRL.Alerter ('#alerts', [ 1, 2, 3 ]);
			});
		</script>
	</body>
</html>
```
