#!/usr/bin/ruby

require 'socket'


class Client
	def initialize
		@cfg = { :port => 20000 }
		@sock = nil
		@output_handler = nil
	end


	# clean up after ourselves
	def die
		@sock.puts 'KILL_ME'
		@sock.close
		puts 'socket closed.'
		@output_handler.terminate!
		puts 'io killed.'
	end
	
	# print server responses
	def handle_output
		while line = @sock.gets # returns nil on EOF
			puts "> #{line}"
		end
	end

	# run
	def start
		puts 'opening socket...'
		@sock = TCPSocket.new('localhost', @cfg[:port])

		puts 'opening reciever thread...'
		@output_handler = Thread.start{ handle_output }

		puts 'enter your messages:'
		should_exit = false
		while (!should_exit)
			str = gets.chomp!
			case str
			when 'exit'
				puts 'trying to die...'
				@sock.puts str
				die
				should_exit = true
			else
				@sock.puts str
			end
		end
	end
end


def main
	c = Client.new
	
	c.start
	
	return 0
end

main

