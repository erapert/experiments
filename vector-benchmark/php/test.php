<?php
	require_once 'Console/CommandLine.php';	// install: " pear install Console_CommandLine ""
	require_once 'Benchmark/Timer.php';		// install: " pear install Benchmark "
	require_once 'vector3.php';
	
	function unitTest () {
		$t0 = new Vector3 (1, 1, 1);
		$t1 = new Vector3 (1, 1, 1);
		$t2 = new Vector3 (2, 2, 2);
		
		echo "t0 == t1: {$t0->equals($t1)}\n";
		echo "t1 == t2: {$t1->equals($t2)}\n";
		
		$t0->set (2, 2, 2);
		echo "t0: {$t0->stringify()}\n";
		$t1->set (8, 8, 8);
		echo "t1: {$t1->stringify()}\n";
		$t2->set (16, 16, 16);
		echo "t2: {$t2->stringify()}\n";
		
		$temp = $t0->plusScalar (2);
		echo "t0 + 2 = {$temp->stringify()}\n";
		
		echo "t0->plus(t1): {$t0->plus($t1)->stringify()}\n";
		echo "t2->minus(t1): {$t2->minus($t1)->stringify()}\n";
		
		echo "t0->transform(0, 8, 0): {$t0->transform(0, 8, 0)->stringify()}\n";
		
		echo "t0->scale(0.5): {$t0->scale(0.5)->stringify()}\n";
		
		echo "t1->length(): {$t1->length()}\n";
		echo "t2->length(): {$t2->length()}\n";
		
		echo "t0->dot(t1): {$t0->dot($t1)}\n";
		
		echo "t1->cross(t2): {$t1->cross($t2)->stringify()}\n";
		
		$t1->set (1, 2, 3);
		$t2->set (4, 5, 6);
		echo "t1->set(1, 2, 3): {$t1->stringify()}\n";
		echo "t2->set(4, 5, 6): {$t2->stringify()}\n";
		echo "t1->cross(t2): {$t1->cross($t2)->stringify()}\n";
	}
	
	function benchmark () {
		$t0 = new Vector3 (1, 2, 3);
		$t1 = new Vector3 (4, 5, 6);
		
		$timer = new Benchmark_Timer ();
		$timer->start ();
		
		echo "(times in seconds)\n";
		echo "timing individual operations...\n";
		$timer->setMarker ('stringify');
		$temp = $t0->stringify ();
		
		$timer->setMarker ('plus');
		$temp = $t0->plus ($t1);
		
		$timer->setMarker ('transform');
		$temp = $t0->transform (0, 8, 0);
		
		$timer->setMarker ('scale');
		$temp = $t0->scale (0.5);
		
		$timer->setMarker ('length');
		$temp = $t0->length ();
		
		$timer->setMarker ('dot');
		$temp = $t0->dot ($t1);
		
		$timer->setMarker ('cross');
		$temp = $t0->cross ($t1);
		
		$timer->stop ();
		$timer->display ();
		
		echo "Doing time trial (all ops in a row)\n";
		$timer = new Benchmark_Timer ();
		$timer->start ();
		
		for ($i = 0; $i < 1000000; $i++) {
			$temp = $t0->stringify ();
			$temp = $t0->plus ($t1);
			$temp = $t0->transform (0, 8, 0);
			$temp = $t0->scale (0.5);
			$temp = $t0->length ();
			$temp = $t0->dot ($t1);
			$temp = $t0->cross ($t1);
		}
		
		$timer->stop ();
		$timer->display ();
	}

	$parser = new Console_CommandLine (array ('description' => 'Unit test and benchmark for Vector3'));
	$parser->addOption (
		'unittest',
		array (
			'short_name' => '-u',
			'long_name' => '--unittest',
			'description' => 'run and print the unit test',
			'action' => 'StoreTrue'
		)
	);
	$parser->addOption (
		'benchmark',
		array (
			'short_name' => '-b',
			'long_name' => '--benchmark',
			'description' => 'run the benchmark',
			'action' => 'StoreTrue'
		)
	);
	
	try {
		if (count ($argv) == 1) {
			$parser->displayUsage ();
			exit ();
		}
		$opts = $parser->parse ();
		if ($opts->options['unittest'])
			unitTest ();
		if ($opts->options['benchmark'])
			benchmark ();
		if ($opts->options['help'])
			$parser->displayUsage ();
	
	} catch (Exception $e) {
		$parser->displayError ($e->getMessage ());
	}
?>