function get_longest_run (search_str) {
	var prev_char = '',
		max_char_so_far = '',
		max_len_so_far = 0,
		count = 1;
	
	for (current_char of search_str) {
		if (current_char == prev_char) {
			count += 1;
			if (count > max_len_so_far) {
				max_char_so_far = current_char;
				max_len_so_far = count;
			}
		} else {
			count = 1;
		}
		prev_char = current_char;
	}
	
	return { longest_run_char: max_char_so_far, length_of_run: max_len_so_far };
}

if (require.main === module) {
    // nodejs main.js somelongstringhere
    if (process.argv.length != 3) {
	    console.log ('The only arg this program accepts is a string to search for runs.');
	    process.exit (1)
    }

    var test_str = process.argv[2],
	    longest_run = get_longest_run (test_str);

    console.log (`Test string: ${test_str}`);
    console.log (`Longest run: (${longest_run.longest_run_char}, ${longest_run.length_of_run})`);
}
