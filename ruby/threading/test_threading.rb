#!/usr/bin/ruby

class Thredder
	def initialize
		puts 'initializing...'
		@thred_handle = nil
	end

	def die
		puts 'dying...'
		@thred_handle.terminate!
	end

	def do_work
		loop do
			puts 'doing work...'
			sleep 1
		end
	end

	def run
		@thred_handle = Thread.new do
			puts 'running...'
			do_work
		end
		
		# for some FUCKING reason ruby will only do input from the main thread!
		should_exit = false
		while (!should_exit)
			print ': '
			str = gets.chomp!
			case str
			when 'exit'
				die
				should_exit = true
			else
				puts "echo: #{str}"
			end
		end
	end
end

t = Thredder.new
t.run

