#= require <errbox.coffee>
#
#	requires
#		+ JQuery
#		+ You need to define "var VRL = VRL || {};" somehwere before this file is included
#		+ you need 'errbox.coffee' somewhere that coffeescript-concat can find it
#
#	valid json from server looks like this:
#		{
#		success: 'error' || 'success' || 'warning',
#		message: 'some <i>text</i> string to be inserted into <pre>feedback box</pre>'
#		}
#
class VRL.ErrableAjax
	constructor: (settings) ->
		this._checkConstructorArgs(settings)
		
		@mySettings = settings
		@myForm = $(settings.form)
		@myForm.find("#{settings.form}_submit").on('click', () =>
			this.submit()
		)
		
		eb = @myForm.find('.errbox')
		if eb.length > 0
			@errbox = new VRL.ErrBox (eb)
		else
			@errbox = null
	
	submit: () ->
		data = {}
		@myForm.find('input').each( (index, input) ->
			data[input.getAttribute('name')] = input.value
		)
		$.ajax({
			url: @mySettings.url,
			type: 'POST',
			data: data
		}).success( (msg) =>
			response = JSON.parse(msg)
			if @errbox
				@errbox.set(response.success, response.message)	
			if @mySettings.callback?
				@mySettings.callback(@myForm, response)
		)
	
	_checkConstructorArgs: (args) ->
		if not (args.form? and args.url?)
			throw "!ERROR! ErrableAjax: you must pass 'enclosingDivId' and 'url' to the constructor."
		if args.callback? and typeof args.callback != 'function'
			throw "!ERROR! ErrableAjax: 'args.callback' must be a function."
