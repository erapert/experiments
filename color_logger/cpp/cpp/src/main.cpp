#include <iostream>
using namespace std;

// set this in your compiler settings: #define LOGGING_ENABLED 1
#include "Logger.h"

int main () {
	// you can set indentSize and enableColor at init
	Logger l (2, false);
	// you can set them later, also, notice how you can chain stuff
	l.enableColor(true)->setIndentSize(2)->showColorCodes ();
	
	l.resetIndent()
	->log ("Codes will be replaced replaced with the proper ANSI escape sequences-- [c]they[y]do[r]not[g]turn[b]into[w]whitespace.[/]");
	
	// by the way, disabling color should run much faster since it's no longer inserting color codes
	l.enableColor(false)
		->log ("If you forget to use `[/]` to end a color")
		->enableColor(true)
		->indent(1)->log ("[b]it will run \'till it finds a new color tag or to the end of the string.")
		->indent(-1)->log ("Because of this you don't have to terminate color before setting to a new color. Check this out:")
		->indent(1)->log ("[r]RED [g]GREEN [b]BLUE[/] And this is uncolored.");
	
	l.log ("[r]%s[/] works like [g]%s[/]: %d", "String interpolation", "printf()", 42);
	
	// supports std::string too
	string lessThan ("Setting indentation to less than zero simply resets to zero:");
	l.resetIndent()->log (lessThan)
	->indent (5)->log ("Indented to 5.")
	->setIndent (-1000)->log ("Indented to -1000");
	
	// clear color just in case the testing screwed stuff up
	#ifdef WIN32
		cout << "\033[0m" << endl;
	#endif
	
	return 0;
}
