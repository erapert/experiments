#
#	requires
#		+ JQuery
#		+ You need to define "var VRL = VRL || {};" somehwere before this file is included
#
class VRL.ErrBox
	constructor: (errBox) ->
		# you can pass in the jquery id string or a jquery object itself
		if typeof errBox == 'string'
			@myBox = $(errBox)
		else
			@myBox = errBox
		
		@myBox.html('
			<button id="errbox_close">x</button>
			<ul id="errbox_msgs"></ul>
		')
		@msgs = @myBox.find('#errbox_msgs')
		@myBox.find('#errbox_close').on('click', () =>
			this.close()
		)
		# valid status/classes: 'error', 'warning', 'info'
		#	they should be red, yellow, green
		@statuses = [ 'error', 'warning', 'success' ]
		# start off hidden
		@myBox.hide()
	
	close: () ->
		@msgs.empty()
		@myBox.hide()
	
	set: (status, msg) ->
		if status not in @statuses
			throw "Invalid status '#{status}'. Valid statuses are #{@statuses}"
		
		@msgs.empty()
		
		if typeof msg == 'string'
			@msgs.append("<li>#{msg}</li>")
		else
			for m in msg
				@msgs.append("<li>#{m}</li>")
		
		for stat in @statuses
			@myBox.removeClass stat
		@myBox.addClass status
		@myBox.show()
