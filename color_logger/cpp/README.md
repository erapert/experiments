Logger
======
A terminal logger for C++ that supports color.

Requirements
------------
* [g++ 4.3+](http://gcc.gnu.org/projects/cxx0x.html) with C++ 11 support.
* [scons](http://www.scons.org/): `sudo apt-get install scons`
* ANSI color support for your terminal (Windows' console does not support these and will display garbage).

Compiling the test
------------------
* Run `scons` from the same dir as the `SConstruct` file.
* Debug mode: `scons debug=1`

Usage
-----
Just plop `Logger.h` and `Logger.cpp` somewhere that your compiler or project can find it then:
####Quick and simple:
```cpp
// define this in your compiler settings, i.e. for g++: -DLOGGING_ENABLED
// in VisualStudio set it in your project settings dialog
//#define LOGGING_ENABLED 1
#include "Logger.h"

int main () {
    // indent size = 2 spaces, color turned on
    Logger log (2, true);
    // string interpolation works like printf()
    log.log ("[r]RED [g]GREEN [b]%s[/]", "BLUE");
    log.indent(1)->log("Methods can chain")->indent(1)->log("because they return a pointer to the logger.");
    
    return 0;
}
```
You can run the compiled executable to see some testing output: `./logger.i386`

####More involved:
```cpp
#include <iostream>
using namespace std;
#include "Logger.h"

// you probably will want one instance of the logger available throughout the program...
// you can set indentSize and enableColor at init
Logger l = Logger (2, false);

int main () {
	// you can set them later, also, notice how you can chain stuff
	l.enableColor(true)->setIndentSize(2)->showColorCodes ();
	
	l.resetIndent()
	->log ("Codes will be replaced replaced with the proper ANSI escape sequences--")
	->indent(1)->log ("[c]they[y]do[r]not[g]turn[b]into[w]whitespace.[/]")->indent(-1);
	
	// by the way, disabling color should run much faster since it's no longer inserting color codes
	l.enableColor(false)
		->log ("If you forget to use `[/]` to end a color")
		->enableColor(true)
		->indent(1)->log ("[b]it will run \'till it finds a new color tag or to the end of the string.")
		->indent(-1)->log ("Because of this you don't have to terminate color before setting to a new color. Check this out:")
		->indent(1)->log ("[r]RED [g]GREEN [b]BLUE[/] And this is uncolored.");
	
	l.log ("[r]%s[/] works like [g]%s[/]: %d", "String interpolation", "printf()", 42);
	
	// supports std::string too
	string lessThan ("Setting indentation to less than zero simply resets to zero:");
	l.resetIndent()->log (lessThan)
	->indent (5)->log ("Indented to 5.")
	->setIndent (-1000)->log ("Indented to -1000");
	
	// clear color just in case the testing screwed stuff up
	cout << "\033[0m" << endl;
	
	return 0;
}
```

###Notes
* The technique used for colorizing is quite slow compared to what most C++ programmers might expect because it's doing a string find and replace for each color tag. Turning off color will speed things up considerably. The faster way would be to print each character in the string inserting colors and interpolating vars as they are encountered... this isn't as simple though, so I left it for another day.
* Windows' terminal does not support ANSI color escape codes (which is what the logger uses for color).