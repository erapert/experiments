#ifndef TIMER_H
#define TIMER_H

#include <cstdlib>
#include <sys/time.h>	// POSIX only, for Windows... do something else
#include <string>
#include <sstream>
#include <iomanip>

/*
	boost::lexical_cast<string> gives scientific notation
	which is difficult to compare against the the decimal
	results from the rest of the benchmarks. So we use
	some std::ostringstream stuff instead. I'll leave this
	commented out in case you want to play around.
*/
//#include <boost/lexical_cast.hpp>

class Timer {
	public:
					Timer		();
					~Timer		();
		timeval		start		();
		timeval		stop		();
		float		duration	() const;	// returns duration in seconds
		std::string	durationStr	() const;
	
	private:
		// no copying of this class
					Timer		(const Timer &);
		Timer &		operator =	(const Timer &);
		Timer &		copy		(const Timer &);
		
		timeval		startTime;
		timeval		endTime;
};

inline Timer::Timer () {
	memset (((void *) &startTime), 0, sizeof (timeval));
	memset (((void *) &endTime), 0, sizeof (timeval));
}

inline Timer::~Timer () {}

inline timeval Timer::start () {
	gettimeofday (& startTime, NULL);
	return startTime;
}

inline timeval Timer::stop () {
	gettimeofday (& endTime, NULL);
	return endTime;
}

/*
	A microsecond ('usecond') is one millionth of a second.
	But tv_usec is an integer. So we have to divide by 1 million
	to get it calibrated right.
*/
inline float Timer::duration () const {
	float seconds = endTime.tv_sec  - startTime.tv_sec;
	float useconds = endTime.tv_usec - startTime.tv_usec;
	return (seconds + (useconds / 1000000.0));
}

inline std::string Timer::durationStr () const {
	
	//return boost::lexical_cast<string>(duration()) + " seconds";
	
	std::ostringstream formatStr;
	formatStr << setprecision (16);
	formatStr.setf(ios::fixed, ios::floatfield);
	formatStr.setf (ios::showpoint);
	formatStr << duration ();
	return formatStr.str ();
}

#endif