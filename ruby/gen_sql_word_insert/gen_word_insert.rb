
require 'optparse'

def gen_insertion_file args
	if !File.exists? args[:fname]
		raise "!ERROR! Failed to find file '#{fname}'"
	end
	
	if args[:dval]
		defcol = ", `#{args[:dval][0]}`"
		defval = ", '#{args[:dval][1]}'"
	else
		defcol = ''
		defval = ''
	end
	
	
	infile = File.open args[:fname], 'r'
	outfile = File.open "#{args[:fname]}.sql", 'w'
	
	out_text = "INSERT INTO `#{args[:tname]}` (`#{args[:cname]}`#{defcol}) VALUES\n"
	while line = infile.gets
		out_text += "('#{line.chop}'#{defval}),\n"
	end
	out_text.chop!()
	out_text.chop!()
	out_text += ';'
	
	outfile.puts out_text
	
	infile.close
	outfile.close
end

if __FILE__ == $0
	args = {}
	parser = OptionParser.new do |o|
		o.on('-f', '--file [FILE]', 'Input file. Each word is on its own line.') { |f| args[:fname] = f }
		o.on('-t', '--tablename [TNAME]', 'Name of table to insert into') { |t| args[:tname] = t }
		o.on('-c', '--columnname [CNAME]', 'Name of column to insert into') { |c| args[:cname] = c }
		o.on('-d', '--defaultedcolumn NAME:VALUE', 'Name and default value to add to every row. Format is "NAME:VALUE".') { |d| args[:dval] = d.split(':') }
	end
	
	if ARGV.length == 0
		puts parser.help
	else
		parser.parse!
		if args[:fname] and args[:tname] and args[:cname]
			gen_insertion_file args
		else
			puts parser.help
		end
	end
end

