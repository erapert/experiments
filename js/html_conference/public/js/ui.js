$(function () {
	var roomName = null,
		sock = io.connect ();
	
	// socket.io, when self-served, automatically knows who its server is
	$(window).on ('beforeunload', function () {
		sock.close ();
	});
	
	var contactList = new nodevoxel_ns.ContactList (
			$('#contacts'),
			{
				onClickItem: function (elem) {
					/*
					var $e = $(elem);
					console.log ('onSelectUser: ', elem, $e.data ('address'));
					$('#call-address').attr('disabled', false).data ('address', $e.data ('address'));
					*/
				}
			}
		),
		localVid = new nodevoxel_ns.VideoCall (
			$('#localVid')[0],
			{ width: 320, height: 240 }
		),
		onJoinRoom = function () {
			roomName = $('#room-name').val ();
			if (roomName != '') {
				console.log ('joining ', roomName);
				sock.emit ('create or join', roomName);
				$('#join-room').hide ();
				$('#leave-room').show ();
				sock.emit ('get room users', roomName);
				$('#call-address').attr ('disabled', false);
			}
		},
		setLocalAndDoAnswer = function (sessDescrip) {
			peerConn.setLocalDescription (sessDescrip);
			sock.emit ('rtc message', { roomName: roomName, data: sessDescrip });
		},
		makePeerConnection = function () {
			var rtrn = new RTCPeerConnection (null);
			rtrn.onicecandidate = function (evt) {
				//console.log ('onicecandidate ', evt);
				// send my candidate through the signalling sock
				if (evt.candidate) {
					sock.emit ('rtc message', {
						roomName: roomName,
						data: {
							type: 'candidate',
							label: evt.candidate.sdpMLineIndex,
							id: evt.candidate.sdpMid,
							candidate: evt.candidate.candidate
						}
					});
				}
			};
			rtrn.onaddstream = function (evt) {
				console.log ('onaddstream ', evt);
				// tell commWindow to add a new remote vid
				//commWindow.add ({ stream: evt.stream });
				$('#videos-container').append ('<video id="remoteVid" autoplay="autoplay" muted="muted">');
				$('#remoteVid')[0].src = window.URL.createObjectURL (evt.stream);
			};
			rtrn.onremovestream = function (evt) {
				console.log ('remote stream removed.', evt);
				// tell commWindow to remove the remote vid
				$('#remoteVid').remove ();
			};
			return rtrn;
		},
		peerConn = null;
	
	$('#room-name').on ('keypress', function (ev) {
		var keycode = (ev.keyCode ? ev.keyCode : ev.which);
		if (keycode == '13') {
			onJoinRoom ();
		}
    });
	$('#join-room').on ('click', onJoinRoom);
	$('#leave-room').on ('click', function () {
		sock.emit ('leave room', roomName);
		$('#leave-room').hide ();
		$('#join-room').show ();
		contactList.clearAll ();
	});
	
	$('#start-camera').on ('click', function () {
		//console.log ('start camera');
		localVid.start ();
		$('#start-camera').hide ();
		$('#stop-camera').show ();
	});
	$('#stop-camera').on ('click', function () {
		//console.log ('stop camera');
		localVid.stop ();
		$('#start-camera').show ();
		$('#stop-camera').hide ();
	});
	// start local media on page load
	localVid.start ();
	$('#start-camera').hide ();
	$('#stop-camera').show ();
	
	
	$('#call-address').on ('click', function () {
		/*
		var addr = $(this).data ('address');
		console.log ('call ' + addr);
		*/
		
		peerConn = makePeerConnection ();
		peerConn.addStream (localVid.mediaStream);
		peerConn.createOffer (setLocalAndDoAnswer, function (err) { throw err; });
	});
	$('#hang-up').on ('click', function () {
		console.log ('hang up');
		// tell the signalling server that I'm outtie
		peerConn.close ();
		peerConn = null;
	});
	
	sock.on ('user joined room', function (data) {
		console.log ('user joined:', data);
		contactList.add (data);
	});
	sock.on ('user left room', function (data) {
		console.log	('user left:', data);
		contactList.remove (data);
	});
	sock.on ('users list', function (data) {
		console.log ('users list: ', data);
		$('#contacts').html ('');
		contactList.set (data);
	});
	sock.on ('rtc message', function (msg) {
		console.log ('rtc message: ', msg);
		switch (msg.data.type) {
			case 'candidate':
				if (!peerConn) {
					peerConn = makePeerConnection ();
					peerConn.addStream (localVid.mediaStream);
				}
				peerConn.addIceCandidate (new RTCIceCandidate ({
					sdpMLineIndex: msg.data.label,
					candidate: msg.data.candidate
				}));
				break;
			
			case 'offer':
				if (!peerConn) {
					peerConn = makePeerConnection ();
					peerConn.addStream (localVid.mediaStream);
				}
				peerConn.setRemoteDescription (new RTCSessionDescription (msg.data));
				peerConn.createAnswer (
					setLocalAndDoAnswer,
					function (err) { throw err; },
					{ mandatory: { OfferToReceiveAudio: true, OfferToReceiveVideo: true } }
				);
				break;
			
			case 'answer':
				peerConn.setRemoteDescription (new RTCSessionDescription (msg.data));
				break;
			
			default:
				console.log ('unknown rtc message type: ', msg.data.type);
				throw ('unknown rtc message type');
				break;
		}
	});
});
