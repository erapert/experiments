var stat = require ('node-static'),
	http = require ('http'),
	filesv = new (stat.Server) (),
	listenPort = 3000,
	app = http.createServer (function (req, res) {
		filesv.serve (req, res);
	}).listen (listenPort);

var io = require ('socket.io').listen (app);

console.log ('listening on port ' + listenPort);

io.sockets.on ('connection', function (sock) {
	
	sock.on ('create or join', function (roomName) {
		//console.log (sock.id, 'requested to join room: ', roomName);
		
		sock.join (roomName);
		io.to (roomName).emit ('user joined room', { name: sock.id, address: sock.handshake.address });
	});
	
	sock.on ('leave room', function (roomName) {
		//console.log (sock.id, ' requested to leave room: ', roomName);
		
		sock.leave (roomName);
		io.to (roomName).emit ('user left room', { name: sock.id, address: sock.handshake.address });
	});
	
	sock.on ('get room users', function (roomName) {
		//console.log (sock.id, ' requested list of users in ', roomName);
		
		// socket.io version < 1.0
		/*
			io.sockets.clients ('roomName');
		*/
		
		// socket.io version 1.0 broke backwards compatibility...
		var clientsInThisRoom = io.sockets.adapter.rooms [roomName],
			usersList = [];
		
		for (var cid in clientsInThisRoom) {
			usersList.push ({ name: cid, address: io.sockets.connected [cid].handshake.address });
		}
		
		console.log (usersList);
		
		sock.emit ('users list', usersList);
	});
	
	sock.on ('rtc message', function (msg) {
		//console.log ('rtc message: ', msg);
		io.to (msg.roomName).emit ('rtc message', msg);
	});
});
