#include "hrtime.h"

// get the current time with usec accuracy (secs since epoch)
int hrt_getTime (lua_State * L) {
	struct timeval t;
	gettimeofday (& t, NULL); // included from <sys/time.h>
	double seconds = (t.tv_sec + (t.tv_usec / 1000000.0));
	
	lua_pushnumber (L, seconds);
	return 1;
}

// init this lib and register C functions
int luaopen_hrtime (lua_State * L) {
	lua_register (L, "hrt_getTime", hrt_getTime);
	return 0;
}

