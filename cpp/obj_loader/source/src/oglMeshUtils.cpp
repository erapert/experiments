#include <iostream>
#include <fstream>
#include <string>
#include <vector>
using namespace std;

#include "oglMeshUtils.hpp"
#include "oglFundamentals.hpp"
#include "oglMesh.hpp"

bool OGLMeshUtils::load (const char * fname, OGLMesh & m) {
	cout << "load \"" << fname << "\"" << endl;
	ifstream fil (fname, ios::in);
	string line;

	while (getline (fil, line)) {
		switch (line [0]) {
			case 'v':
				switch (line [1]) {
					case 'n':
						// a vertex normal
						cout << "normal: (" << line.substr(2) << ")" << endl;
						break;
					case 't':
						// texture coordinate
						cout << "uv: (" << line.substr (2) << ")" << endl;
						break;
					case 'p':
						// parameter space vert
						cout << "param vert: " << line.substr (2) << endl;
						break;
					case ' ':
						// a vertex position
						cout << "vertex: (" << line.substr (2) << ")" << endl;
					break;
				}
				break;
			case 'f': // a face (an element index triplet)
				cout << "face: " << line.substr (2) << endl;
				break;
			case 'o': // introduces a new object/mesh (rest of line is name)
				cout << "object/mesh: " << line.substr (2) << endl;
				break;
			case 's':
				cout << "smoothing group: " << line.substr (2) << endl;
				break;
			case '#':
			default:
				// ignore comments and invalid token lines
				// ignore usemtl lines
				// ignore mtllib lines
				break;
		}
	}

	return true;
}

void OGLMeshUtils::printMesh (const OGLMesh & m) {
	cout << "printMesh()" << endl;
}