#include "ruby.h"

/*
	NOTE:
		To build this module do not just run make. Run extconf.rb-- this will generate the makefile.
		Only AFTER running extconf.rb should you run make to build the final .so file.
*/

// handle to me, the module
VALUE MyTest = Qnil;



void Init_mytest ();
VALUE method_get_num (VALUE);


// ruby calls this on module load
void Init_mytest ()
	{
	MyTest = rb_define_module ("MyTest");
	
	rb_define_method (MyTest, "get_num", method_get_num, 0);
	}


// the actual functions that do things:

// note the "self" arg. This is declared as a method in Init_mytest() and therefore needs this handle to self
VALUE method_get_num (VALUE self)
	{
	int rtrn = 42;
	return INT2NUM (rtrn);
	}

