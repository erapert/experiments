#ifndef OGL_MESH_UTILS_H
#define OGL_MESH_UTILS_H 1

/*
	Intimately related to OGLMesh.
	This class wraps up all the I/O for the mesh class.
*/

#include "oglMesh.hpp"

class OGLMeshUtils {
	public:
		static bool load (const char * fname, OGLMesh & m);
		static void printMesh (const OGLMesh & m);
};

#endif
