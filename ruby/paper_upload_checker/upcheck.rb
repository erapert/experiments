#!/usr/bin/ruby

require 'optparse'

class Processor
	attr_accessor :max_fname, :max_fsize, :replace_underscores, :dir, :check_only
	
	def initialize
		@max_fname = 60					# file names must be less than 60 chars by default
		@max_fsize = 29000000			# files need to be less than 29mb by default
		@replace_underscores = false	# leave filenames alone by default
		@dir = './'						# look in the current dir by default
		@check_only = true				# if false then the files will be modified
		@done_clean = true				# if we run through with no errors or problems at all then say so
	end
	
	public
		
		def process(path = nil)
			if !File.exists? @dir and !File.exists path
				throw 'Invalid path or none given'
			end
			process_path = (path and File.exists?(path)) ? path : @dir
			
			if @check_only
				notify 'Only checking, will not change files'
			end
			
			Dir.foreach(process_path) do |filename|
				if filename != '.' and filename != '..' and filename != __FILE__
					full_path = "#{process_path}#{filename}"
					check_file_size full_path
					if @replace_underscores
						underscored_name = replace_underscores process_path, filename
					else
						underscored_name = filename
					end
					trim_file_name process_path, underscored_name
				end
			end
			
			say_done
		end
	
	private
		# for bash-- prints "!WARNING!" in red
		def warn(msg)
			puts "\e[31m!WARNING!\e[0m #{msg}"
		end
		
		def info(msg)
			puts "\e[33m!INFO!\e[0m #{msg}"
		end
		
		def notify(msg = 'everything is ok :)')
			puts "\e[1m\e[32mOK\e[34m #{msg}\e[0m"
		end
		
		def say_done
			if @done_clean
				status = 'No problems with any files.'
			else
				status = 'There were problems with some files. Check log.'
			end
			puts "\e[1m\e[36mDONE \e[34m#{status}\e[0m"
		end
		
		# if the file name is too long then renames file to a shorter name
		def trim_file_name(path, filename)
			if filename.length > @max_fname
				if !@check_only
					ext = File.extname filename
					new_fname = filename.slice(0, (@max_fname - (ext.length))) + ext
					File.rename("#{path}#{filename}", "#{path}#{new_fname}")
					info "File name too long; renamed: \e[36m\"#{filename}\"\e[0m to \e[35m\"#{new_fname}\"\e[0m"
				
				else
					warn "file name length > #{@max_fname} chars:\n\t\"#{filename}\""
				end
				@done_clean = false
			end
		end
		
		# if file is too big then prints warning
		def check_file_size(full_pathname)
			fsize = File.size full_pathname
			if fsize.to_i > @max_fsize
				warn "File is too big: \e[36m#{fsize} bytes > #{@max_fsize} bytes\e[0m :\n\t\"#{full_pathname}\""
				@done_clean = false
			end
		end
		
		def replace_underscores(path, filename)
			if filename.include? ' '
				info "filename has spaces in it: \e[36m\"#{filename}\"\e[0m"
				if !@check_only
					new_fname = filename.gsub ' ', '_'
					File.rename "#{path}#{filename}", "#{path}#{new_fname}"
					info "Renamed to \"\e[36m#{new_fname}\e[0m"
					@done_clean = false
				else
					new_fname = filename
				end
			else
				new_fname = filename
			end
			
			return new_fname
		end
end




if __FILE__ == $0
	p = Processor.new
	
	parser = OptionParser.new do |o|
		o.on('-n', '--maxname LEN', 'max file name length in characters') { |len| p.max_fname = len.to_i }
		o.on('-m', '--maxsize SIZE', 'max file size in kilobytes') { |siz| p.max_fsize = siz.to_i }
		o.on('-u', '--underscore', 'replace spaces with underscores in file names') { p.replace_underscores = true }
		o.on('-d', '--dir DIRECTORY', 'full path to directory full of files') { |dir| p.dir = dir }
		o.on('-p', '--process', 'directory to process-- this will change your files') { p.check_only = false }
		o.on('-c', '--check', 'directory to check-- will not change files, only warns') { p.check_only = true }
	end
	
	if ARGV.length == 0
		puts parser.help()
	else
		begin parser.parse!
		rescue OptionParser::InvalidOption => e
			puts 'Invalid option'
			puts parser.help()
		end
		p.process
	end
end
