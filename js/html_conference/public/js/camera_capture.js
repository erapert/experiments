$(function () {
	navigator.getMedia = navigator.getUserMedia || navigator.mozGetUserMedia || navigator.webkitGetUserMedia;
	
	if (!navigator.getMedia) {
		throw ('browser doesn\'t seem to support webrtc');
		return;
	}
	
	var cameraStream = null,
		cameraStreamURL = null,
		vidElem = null;
	
	$('#start-camera').on ('click', function () {
		navigator.getMedia (
			{ video: true },
			function (stream) {
				cameraStream = stream;
				cameraStreamURL = window.URL.createObjectURL (stream);
				$('#videos-container').append ('<video id="vid" width="320" height="240"></video');
				vidElem = $('#vid')[0];
				vidElem.src = cameraStreamURL;
				vidElem.play ();
			},
			function (err) {
				console.log ('err: ', err);
			}
		);
	});
	
	$('#stop-camera').on ('click', function () {
		if (cameraStream) {
			cameraStream.stop ();
			$('#vid').remove ();
		}
	});
});
