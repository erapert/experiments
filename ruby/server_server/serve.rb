#!/usr/bin/ruby

# !STOP! don't require 'rubygems' directly in your source code
# instead consider one of the following:
#	1. run ruby with the arg directly:
#			ruby -rubygems FOO.rb
#	2. set up your environment to import rubygems stuff:
#			RUBYOPT='-r rubygems'
#			export RUBYOPT
#			ruby FOO.rb
#		(you can even put this into your ~/.bashrc
#		
#require 'rubygems'

require 'active_record'
require 'models/server_entry'
require 'socket'
require 'thread'
require 'pp'

class ServerServer
	def initialize
		@port = 20000
		@dbcfg = {
			:adapter => 'mysql2',
			:database => 'vg_servers',
			:username => 'acropoli',
			:password => '5part4n',
			:host => 'vgservers.virtualgemini.com'
		}
		@dbcon = nil
	end
	
	def send_servers_list client
		puts 'send list'
		ServerEntry.find(:all).each do |entry|
			client.puts entry.stringify
		end
	end

	def start
		puts "starting server on port #{@port}..."
		sock = TCPServer.new 'localhost', @port

		puts 'connecting to database...'
		pp @dbcfg
		
		@dbcon = ActiveRecord::Base.establish_connection @dbcfg
		if !@dbcon
			puts "!ERROR! failed to connect to database!\nTerminating..."
		else
			puts 'available servers:'
			ServerEntry.find(:all).each do |entry|
				puts entry.stringify
			end
		end
		
		puts 'Now accepting clients.'
		loop do
			Thread.start sock.accept do |client|
				begin
					puts 'client connected.'
					while client_msg = client.gets
						m = client_msg.chomp!
						puts ">>#{m}"
						case m
						when 'LIST_ALL_SERVERS'
							puts '> LIST_ALL_SERVERS'
							send_servers_list client
						when 'POST_NEW_SERVER'
							puts '> POST_NEW_SERVER'
						when 'EXIT', 'exit', 'KILL_ME'
							puts '> client wants to disconnect.'
							client.close
							Thread.exit # exit this thread dealing with this client
						else
							puts "invalid input: \"#{client_msg}\""
							client.puts "invalid input: #{m}"
						end
					end
			
				rescue
					($stderr << "!ERROR! #{$!.inspect}\n#{$!.backtrace}\n").flush
				ensure
					puts 'client closed.'
					client.close
				end
			end
		end
	end
end


def main
	s = ServerServer.new
	
	s.start
	
	return 0
end

main

