Logger
======
Yet another terminal logger for python. Supports color.

Requirements
------------
* python 2.7+
* ANSI color support for your terminal

Usage
-----
####Quick and simple:
```python
	from logger import Logger
	l = Logger ()
	l.log ('[red]Error:[/] This is an error message.')
	l.indent (2).log ('[.]Info:[/] This will be shown in grey.')
```
You can run the module itself to see some testing output: `./logger.py`

####More involved:
```python
	from logger import Logger
	
	# You can set how many spaces are used for display and you can turn off color
	l = Logger (indentSize=2, enableColor=False)
	
	# don't worry, you can enable color later:
	l.enableColor = True
	
	l._showColorCodes ()
	#	'[.]' = '\033[90m' # grey
	#	'[r]' = '\033[91m' # red
	#	'[g]' = '\033[92m' # green
	#	'[y]' = '\033[93m' # yellow
	#	'[b]' = '\033[94m' # blue
	#	'[m]' = '\033[95m' # magenta
	#	'[c]' = '\033[96m' # cyan
	#	'[w]' = '\033[97m' # white
	#	'[/]' = '\033[0m'  # end color/reset
	
	# you can chain the public methods (except for the constructor which Python restrics)
	l.resetIndent ().log ('Codes will be replaced with the proper ANSI escape sequences-- [c]they[y]do[r]not[g]turn[b]into[w]whitespace.[/]')
	
	# by the way, disabling color should run faster since it's no longer inserting the color codes
	l.enableColor = False
	l.log ('If you forget to use `[/]` to end a color')
	l.enableColor = True
	l.indent (1).log ('[b]it will run \'till it finds a new color tag or to the end of the string.')
	l.indent (-1).log ('Because of this you don\'t have to terminate color before setting to a new color. Check this out:')
	l.indent (1).log ('[r]RED [g]GREEN [b]BLUE[/] And this is uncolored.')
	
	l.resetIndent ().log ('Setting indentation to less than zero simply resets to zero:')
	l.indent (5).log ('Indented to 5.')
	l.setIndent (-1000).log ('Indented to -1000.')
```
