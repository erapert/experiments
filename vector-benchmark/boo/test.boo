import System

import VRL

def unitTest ():
	print ("unit test...")
	t0 = Vector3 (X: 1, Y: 1, Z: 1)
	t1 = Vector3 (X: 1, Y: 1, Z: 1)
	t2 = Vector3 (X: 2, Y: 2, Z: 2)
	
	print ("t0 == t1: $(t0.equals (t1))")
	print ("t1 == t2: $(t1.equals (t2))")
	
	t0.set (2, 2, 2)
	print ("t0: $(t0.stringify ())")
	t1.set (8, 8, 8)
	print ("t1: $(t1.stringify ())")
	t2.set (16, 16, 16)
	print ("t2: $(t2.stringify ())")
	
	temp = t0.plusScalar (2)
	print ("t0 + 2 = $(temp.stringify ())")
	
	print ("t0.plus(t1): $(t0.plus(t1).stringify())")
	print ("t2.minus(t1): $(t2.minus(t1).stringify())")
	
	print ("t0.transform(0, 8, 0): $(t0.transform(0, 8, 0).stringify())")
	
	print ("t0.scale(0.5): $(t0.scale (0.5).stringify())")
	
	print ("t1.length(): $(t1.length())")
	print ("t2.length(): $(t2.length())")
	
	print ("t0.dot(t1): $(t0.dot(t1))")
	
	print ("t1.cross(t2): $(t1.cross(t2).stringify())")
	
	t1.set (1, 2, 3)
	t2.set (4, 5, 6)
	print ("t1.set(1, 2, 3): $(t1.stringify())")
	print ("t2.set(4, 5, 6): $(t2.stringify())")
	print ("t1.cross(t2): $(t1.cross(t2).stringify())")


def benchmark ():
	print ("benchmark...")



def print_help ():
	print ("usage:")
	print ("\tunit test:    -u")
	print ("\tbenchmark:    -b")
	print ("\thelp:         -h")




if (argv.Length < 1):
	print_help ()
else:
	for a in argv:
		if a == "-h":
			print_help ()
		elif a == "-u":
			unitTest ()
		elif a == "-b":
			benchmark ()

