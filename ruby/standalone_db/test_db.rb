# !STOP! don't require 'rubygems' directly in your source code
# instead consider one of the following:
#	1. run ruby with the arg directly:
#			ruby -rubygems FOO.rb
#	2. set up your environment to import rubygems stuff:
#			RUBYOPT='-r rubygems'
#			export RUBYOPT
#			ruby FOO.rb
#		(you can even put this into your ~/.bashrc
#		
#require 'rubygems'

require 'active_record'

ActiveRecord::Base.establish_connection(
	:adapter => 'mysql2',
	:database => 'vg_servers',
	:username => 'acropoli',
	:password => '5part4n',
	:host => 'vgservers.virtualgemini.com')

require 'models/server'

s = Server.find 1

puts "server name: \"#{s.name}\""

