DragAuth::Application.routes.draw do
	post 'auth' => 'drag_auth#check_auth', :as => 'check_auth'
	get 'dashboard' => 'drag_auth#dashboard', :as => 'dashboard'
	root :to => "drag_auth#auth_page"
end
