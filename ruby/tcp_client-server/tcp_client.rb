#!/usr/bin/ruby

require 'socket' # tcp connection stuff

puts 'opening socket...'
sock = TCPSocket.new 'localhost', 20000

puts 'opening reciever thread...'
Thread.start do
	while line = sock.gets # returns nil on EOF
		puts "\n> #{line}"
	end
end

puts 'enter your messages:'
begin
	while str = gets
		sock.puts str
	end
rescue
	($stderr << "!ERROR! #{$!.inspect}\n#{$!.backtrace}\n").flush
ensure
	puts 'sock closed.'
	sock.close
end

