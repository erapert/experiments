--[[
	requires Lua 5.2+
	
	import this file like so:
		local Vector3 = require 'vector3'
	
	For nice paper on how to optimize lua (and there are several spots in here that might need it)
	see this: http://www.lua.org/gems/sample.pdf
--]]

local Vector3 = {}
Vector3.__index = Vector3

function Vector3.new (x, y, z)
	local vec = {}
	setmetatable (vec, Vector3)
	vec:set (x, y, z)
	return vec
end

function Vector3:set (x, y, z)
	self.x = x
	self.y = y
	self.z = z
end

function Vector3:stringify ()
	return '(' .. self.x .. ', ' .. self.y .. ', ' .. self.z .. ')'
end

function Vector3:equals (other)
	return ((self.x == other.x) and (self.y == other.y) and (self.z == other.z))
end

function Vector3:plusScalar (factor)
	return Vector3.new ((self.x + factor), (self.y + factor), (self.z + factor))
end

function Vector3:minusScalar (factor)
	return Vector3.new ((self.x - factor), (self.y - factor), (self.z - factor))
end

function Vector3:plus (other)
	return Vector3.new ((self.x + other.x), (self.y + other.y), (self.z + other.z))
end

function Vector3:minus (other)
	return Vector3.new ((self.x - other.x), (self.y - other.y), (self.z - other.z))
end

function Vector3:transform (X, Y, Z)
	return Vector3.new ((self.x + X), (self.y + Y), (self.z + Z))
end

function Vector3:scale (factor)
	return Vector3.new ((self.x * factor), (self.y * factor), (self.z * factor))
end

function Vector3:length ()
	return math.sqrt (self:dot (self))
end

function Vector3:dot (other)
	return ((self.x * other.x) + (self.y * other.y) + (self.z * other.z))
end

function Vector3:cross (other)
	return Vector3.new (
		((self.y * other.z) - (self.z * other.y)),
		((self.z * other.x) - (self.x * other.z)),
		((self.x * other.y) - (self.y * other.x))
	)
end

function Vector3:print_self ()
	print (self:stringify ())
end

return Vector3

