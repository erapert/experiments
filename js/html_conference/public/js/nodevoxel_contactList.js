/*
	requires: jquery
*/

var namespace = nodevoxel_ns || {};
// $container is expected to be a jquery obj: $('ul#contacts')
namespace.ContactList = function ($container, opts) {
	
	var _this = this,
		$clist = $container;
		
	this.cfg = $.extend ({
		selectedClass: 'selected',
		onClickItem: function (elem) {}
	}, (opts || {}));
	
	/*
		contact objects are expected to be like:
			{ name: visible_name_in_list, address: some_ip_address }
	*/
	
	var makeItemElemStr = function (contact) {
		return '<li id="' + contact.address.replace( /(:|\.|\[|\])/g, "\\$1" ) + '">' + contact.name + '</li>';
	}
	
	this.add = function (contact) {
		$(makeItemElemStr (contact))
			.appendTo ($clist)
			.on ('click', function () {
				$clist.children ().removeClass (_this.cfg.selectedClass);
				$(this).addClass (_this.cfg.selectedClass);
				_this.cfg.onClickItem (this);
			})
			.on ('blur', function () {
				$(this).removeClass (_this.cfg.selectedClass);
			})
			.data ('address', contact.address);
	}
	
	this.remove = function (contact) {
		/** finding by data attribute is relatively slow
		$clist.children().filter (function (i, elem) {
			return $(this).data ('address') == contact.address;
		}).remove ();
		*/
		$clist.find('#' + contact.address.replace( /(:|\.|\[|\])/g, "\\$1" )).remove ();
	}
	
	this.clearAll = function () {
		$clist.html ('');
	}
	
	this.set = function (contacts) {
		for (var i in contacts) {
			_this.add (contacts [i]);
		}
	}
};
