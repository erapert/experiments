--[[
	hrtimer requires hrt_time.so
	You have Lua 5.2 right?
	You did `make; sudo make install` right?
--]]
local timer = require 'hrtimer'


function pretend_to_work (howLong)
	os.execute ("sleep " .. tonumber (howLong))
end


print ('test hrt_getTime():')
	local start = hrt_getTime ()
	print ('start time: ' .. start)
	pretend_to_work (3)
	local stop = hrt_getTime ()
	print ('stop time: ' .. stop)
	print ('difference: ' .. string.format ("%.16f", (stop - start)))

print ('test hrtimer.lua')
	timer:start ()
	pretend_to_work (3)
	timer:stop ()
	print ('work took ' .. timer:durationStr ())
	
print ('done.')

