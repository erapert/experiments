#!/usr/bin/python

import time, sys, argparse
from vector3 import *

# little class to help benchmark because timeit was slow (WTF?!)
class Timer:
	def __init__ (self, verbose = False):
		self.verbose = verbose
	
	def __enter__ (self):
		self.start = time.time ()
		return self
	
	def __exit__ (self, *args):
		self.end = time.time ()
		self.secs = self.end - self.start
		if (self.verbose):
			print ("%f seconds" % self.secs)


def unitTest ():
	t0 = Vector3 (1, 1, 1)
	t1 = Vector3 (1, 1, 1)
	t2 = Vector3 (2, 2, 2)
	
	print ("t0 == t1: %s" % t0.equals (t1))
	print ("t1 == t2: %s" % t1.equals (t2))
	
	t0.set (2, 2, 2)
	print ("t0: %s" % t0.stringify ())
	t1.set (8, 8, 8)
	print ("t1: %s" % t1.stringify ())
	t2.set (16, 16, 16)
	print ("t2: %s" % t2.stringify ())
	
	temp = t0.plusScalar (2)
	print ("t0 + 2 = %s" % temp.stringify ())
	
	print ("t0.plus(t1): %s" % t0.plus(t1).stringify())
	print ("t2.minus(t1): %s" % t2.minus(t1).stringify())
	
	print ("t0.transform(0, 8, 0): %s" % t0.transform(0, 8, 0).stringify())
	
	print ("t0.scale(0.5): %s" % t0.scale (0.5).stringify())
	
	print ("t1.length(): %s" % t1.length())
	print ("t2.length(): %s" % t2.length())
	
	print ("t0.dot(t1): %s" % t0.dot(t1))
	
	print ("t1.cross(t2): %s" % t1.cross(t2).stringify())
	
	t1.set (1, 2, 3)
	t2.set (4, 5, 6)
	print ("t1.set(1, 2, 3): %s" % t1.stringify())
	print ("t2.set(4, 5, 6): %s" % t2.stringify())
	print ("t1.cross(t2): %s" % t1.cross(t2).stringify())


def benchmark ():
	t0 = Vector3 (1, 2, 3)
	t1 = Vector3 (4, 5, 6)
	
	print ("timing individual operations...")
	
	sys.stdout.write ("stringify: ")
	with Timer(True) as t:
		temp = t0.stringify ()
	sys.stdout.write ("plusScalar: ")
	with Timer(True) as t:
		temp = t0.plusScalar (10.0)
	sys.stdout.write ("minusScalar: ")
	with Timer(True) as t:
		temp = t0.minusScalar (10.0)
	sys.stdout.write ("plus: ")
	with Timer(True) as t:
		temp = t0.plus (t1)
	sys.stdout.write ("minus: ")
	with Timer(True) as t:
		temp = t0.minus (t1)
	sys.stdout.write ("transform: ")
	with Timer(True) as t:
		temp = t0.transform (0, 8, 0)
	sys.stdout.write ("scale: ")
	with Timer(True) as t:
		temp = t0.scale (0.5)
	sys.stdout.write ("length: ")
	with Timer(True) as t:
		temp = t0.length ()
	sys.stdout.write ("dot: ")
	with Timer(True) as t:
		temp = t0.dot (t1)
	sys.stdout.write ("cross: ")
	with Timer(True) as t:
		temp = t0.cross (t1)
	
	sys.stdout.write ("Doing time trial (all ops in a row): ")
	with Timer(True) as t:
		for x in range (1000000):
			temp = t0.stringify ()
			temp = t0.plusScalar (10.0)
			temp = t0.minusScalar (10.0)
			temp = t0.plus (t1)
			temp = t0.minus (t1)
			temp = t0.transform (0, 8, 0)
			temp = t0.scale (0.5)
			temp = t0.length ()
			temp = t0.dot (t1)
			temp = t0.cross (t1)


# if run from the command line then check argv and see what to do
if __name__ == "__main__":
	parser = argparse.ArgumentParser ()
	
	parser.add_argument ('-u', help = 'run and print the unit test')
	parser.add_argument ('-b', help = 'run the benchmark')
	
	args = parser.parse_args ()
	if (len (sys.argv) == 1):
		parser.print_help ()
	if (args.u):
		unitTest ()
	if (args.b):
		benchmark ()
