File Contents
=============
A trivial class for loading a file into memory and accessing its contents as a string.

Requirements
------------
* [g++ 4.3+](http://gcc.gnu.org/projects/cxx0x.html) with C++ 11 support.
* [scons](http://www.scons.org/): `sudo apt-get install scons`
* On Windows you'll need [VisualStudio 2012](http://www.microsoft.com/visualstudio/eng/downloads)

Usage
-----
```cpp
	#include <iostream>
	using namespace std;

	#include "fileContents.hpp"

	int main () {
		FileContents fc;

		// load() reads the contents of the file into a string
		// returns true if success, false if failed (i.e. failed to open file)
		if (fc.load ("foo.txt")) {
			// these are all equivalent
			// they output the c-string contents
			cout << fc << endl;
			cout << (const char *) fc << endl;
			cout << fc.getContents () << endl;
		}

		// want to manually verify that there's stuff in there?
		if (fc.getContents().length() > 0) {
			// you can save to a file too
			fc.save ("bar.txt");
		}

		// want to save some memory?
		fc.unload ();

		// assignment operator sets the contents
		fc = "something something";

		// these are equivalent
		FileContents anotherOne (fc);
		anotherOne.copy (fc);
		anotherOne = fc;

		return 0;
	}
```


