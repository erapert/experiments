﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using colorTextLog;

namespace colorTextLog {
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {
		ColorTextLogger log;

		public MainWindow () {
			InitializeComponent ();
			log = new ColorTextLogger (theTextBox) {
				defaultColor = Colors.LightGray,
				currentColorMode = ColorTextLogger.ColorMode.NoColor,
				indentationSize = 4
			};
		}

		protected void onLogIt (object sender, RoutedEventArgs e) {
			log.mode(ColorTextLogger.ColorMode.SolidColor, Colors.DarkCyan)
					.logln ("plain log, dark cyan")
					.indent (1)
					.mode(ColorTextLogger.ColorMode.EmbeddedColor)
						.logln ("this is uncolored [r]RED[g]GREEN[b]BLUE[/] and this is also uncolored")
						.logln ("[w]white, [lgg]light gray, [gg] gray, [dgg] dark gray, [bb]black[/]")
						.setIndentSize (8)
						.indent (1)
							.logColorTags ()
					.indent (-2)
				.mode(ColorTextLogger.ColorMode.NoColor)
					.logln ("and finally, this is uncolored.");
		}
	}
}
