ErrableAjax
=======

ErrableAjax is a little widget to wrap up and handle ajax forms with error boxes.

The use-case is where you ajax something to the server and get a response/error and you need to show it to the user.

Installation
------------

1. `sudo apt-get install yui-compressor nodejs coffeescript`
2. `npm install coffeescript-concat`

Requirements
------------

Your webpage will require [jquery](http://jquery.com/).

Build
-----

run `cake build:errableajax` in the same directory as the Cakefile.

Usage
-----

Check out `test.html` and `test.php` for example usage.

Basic plain-jane usage:

```
	var form = new VRL.ErrableAjax ({
		form: '#ajaxForm',
		url: 'some/targetURL.php'
	});
```

With a callback that gets called at the end of the errbox handling:
```
	var form = new VRL.ErrableAjax ({
		form: '#ajaxForm',
		url: 'some/targetURL.php',
		callback: function (form, serverResponse) {
			console.log ('form "', form, '" got this response: ', serverResponse);
		}
	});
```
