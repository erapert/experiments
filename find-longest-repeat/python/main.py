#!/usr/bin/python

import sys

def get_longest_run (search_str):
	prev_char = ''
	max_char_so_far = ''
	max_len_so_far = 0
	count = 1
	
	for current_char in search_str:
		if current_char == prev_char:
			count += 1
			if count > max_len_so_far:
				max_char_so_far = current_char
				max_len_so_far = count
		else:
			count = 1
		prev_char = current_char
	return { 'longest_run_char': max_char_so_far, 'length_of_run': max_len_so_far }
			

if __name__ == '__main__':
	if len(sys.argv) != 2:
		print ('The only arg this program accepts is a string to search for runs')
		sys.exit (1)
	
	test_str = sys.argv[1]
	longest_run = get_longest_run (test_str)
	
	print ("Test string: %s" % test_str)
	print ("Longest run: %s" % longest_run)
