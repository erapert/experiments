--[[
	requires Lua 5.2+
--]]

function unit_test ()
	print ('unit_test...')
	local Vector3 = require 'vector3'
	
	local t0 = Vector3.new (1, 1, 1)
	local t1 = Vector3.new (1, 1, 1)
	local t2 = Vector3.new (2, 2, 2)
	
	print ('t0 == t1: ' .. tostring(t0:equals(t1)))
	print ('t1 == t2: ' .. tostring(t1:equals(t2)))
	
	t0:set (2, 2, 2)
	print ('t0: ' .. t0:stringify ())
	t1:set (8, 8, 8)
	print ('t1: ' .. t1:stringify ())
	t2:set (16, 16, 16)
	print ('t2: ' .. t2:stringify ())
	
	local temp = t0:plusScalar (2)
	print ('t0 + 2 = ' .. temp:stringify ())
	
	print ('t0:plus(t1): ' .. t0:plus(t1):stringify())
	print ('t2:minus(t1): ' .. t2:minus(t1):stringify())
	
	print ('t0:transform (0, 8, 0): ' .. t0:transform(0, 8, 0):stringify())
	
	print ('t0:scale (0.5): ' .. t0:scale(0.5):stringify())
	
	print ('t1:length(): ' .. tostring(t1:length()))
	print ('t2:length(): ' .. tostring(t2:length()))
	
	print ('t0:dot(t1): ' .. tostring(t0:dot(t1)))
	
	print ('t1:cross(t2): ' .. t1:cross(t2):stringify())
	
	t1:set (1, 2, 3)
	t2:set (4, 5, 6)
	print ('t1:set(1, 2, 3): ' .. t1:stringify())
	print ('t2:set(4, 5, 6): ' .. t2:stringify())
	print ('t1:cross(t2): ' .. t1:cross(t2):stringify())
end

function bench_mark ()
	print ('bench_mark...')
	
	local timer = require 'hrtimer'
	local Vector3 = require 'vector3'
	local temp = nil
	local t0 = Vector3.new (1, 2, 3)
	local t1 = Vector3.new (4, 5, 6)
	
	timer:start ()
	t0:set (1, 2, 3)
	timer:stop ()
	print ('set: ' .. timer:durationStr())
	
	timer:start ()
	temp = t0:equals (t1)
	timer:stop ()
	print ('equals: ' .. timer:durationStr())
	
	timer:start ()
	temp = t0:stringify ()
	timer:stop ()
	print ('stringify: ' .. timer:durationStr())
	
	timer:start ()
	temp = t0:plusScalar (10.0)
	timer:stop ()
	print ('plusScalar: ' .. timer:durationStr())
	
	timer:start ()
	temp = t0:minusScalar (10.0)
	timer:stop ()
	print ('minusScalar: ' .. timer:durationStr())
	
	timer:start ()
	temp = t0:plus (t1)
	timer:stop ()
	print ('plus: ' .. timer:durationStr())
	
	timer:start ()
	temp = t0:minus (t1)
	timer:stop ()
	print ('minus: ' .. timer:durationStr())
	
	timer:start ()
	temp = t0:transform (0, 8, 0)
	timer:stop ()
	print ('transform: ' .. timer:durationStr())
	
	timer:start ()
	temp = t0:scale (0.5)
	timer:stop ()
	print ('scale: ' .. timer:durationStr())
	
	timer:start ()
	temp = t0:length ()
	timer:stop ()
	print ('length: ' .. timer:durationStr())
	
	timer:start ()
	temp = t0:dot (t1)
	timer:stop ()
	print ('dot: ' .. timer:durationStr())
	
	timer:start ()
	temp = t0:cross (t1)
	timer:stop ()
	print ('cross: ' .. timer:durationStr())
	
	print ('Doing time trial (all ops in a row)')
	timer:start ()
	for i = 0, 1000000, 1 do
		t0:set (1, 2, 3)
		temp = t0:equals (t1)
		temp = t0:stringify ()
		temp = t0:plusScalar (10.0)
		temp = t0:minusScalar (10.0)
		temp = t0:plus (t1)
		temp = t0:minus (t1)
		temp = t0:transform (0, 8, 0)
		temp = t0:scale (0.5)
		temp = t0:length ()
		temp = t0:dot (t1)
		temp = t0:cross (t1)
	end
	timer:stop ()
	print ('time trial: ' .. timer:durationStr())
end



function print_help ()
	print ([[
	Usage: test.lua [-u, -b, -h]
		-u :	run unit test
		-b :	run bench mark
		-h :	print this help message
	]])
end

function parseArgs (args)
	local rtrn = {}
	local len = 0
	for k, v in pairs(args) do
		rtrn[v] = "true"
		len = len + 1
	end
	rtrn['len'] = len
	return rtrn
end


function main ()
	local parsedArgs = parseArgs (arg)
	
	if ((parsedArgs.len < 3) or (parsedArgs['-h'])) then
		print_help ()
		return
	end
	
	if parsedArgs['-u'] then
		unit_test ()
	end
	
	if parsedArgs['-b'] then
		bench_mark ()
	end
end

main ()

