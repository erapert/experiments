HTML Canvas Widget
==================

Bare bones HTML5 `<canvas>` widget that supports:

* callbacks
* colored pens
* erasing
* clearing the canvas
* setting pen thickness

Usage
-----

**You need to have an object named `VRL` declared for use as a namespace before including `canvas.js`.**

```javascript
    var VRL = VRL || {};

    // takes args... neat
    var canvas = new VRL.CanvasWidget ({ canvasElement: document.getElementById ('the-canvas') });
```
See `canvas.html` for a much more complete example.

####args
```
{
// MANDATORY
	canvasElement: foo // the actual DOM element to init with
// OPTIONAL
	onMouseUp: function(event)
	onMouseDown: function(event)
	onMouseMove: function(event)
	onDrawLine: function(color, thickness, x1, y1, x2, y2) // called after a line is stroked (generally called per-mousemove)
	onClearCanvas: function()
	onSetPenColor: function(color) // doesn't work when setting the attr directly
	onSetPenThickness: function(thickness) // doesn't work when setting the attr directly
}
```