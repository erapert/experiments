import math

class Vector3:
	def __init__ (self, x, y, z):
		self.set (x, y, z)
	
	def set (self, x, y, z):
		self.x = x
		self.y = y
		self.z = z
	
	def stringify (self):
		return "({0}, {1}, {2})".format (self.x, self.y, self.z)
	
	def equals (self, other):
		return ((self.x == other.x) and (self.y == other.y) and (self.z == other.z))
	
	def plusScalar (self, factor):
		return Vector3((self.x + factor), (self.y + factor), (self.z + factor))
	
	def minusScalar (self, factor):
		return Vector3((self.x - factor), (self.y - factor), (self.z - factor))
	
	def plus (self, other):
		return Vector3((self.x + other.x), (self.y + other.y), (self.z + other.z))
	
	def minus (self, other):
		return Vector3((self.x - other.x), (self.y - other.y), (self.z - other.z))
	
	def transform (self, x=0, y=0, z=0):
		return Vector3((self.x + x), (self.y + y), (self.z + z))
	
	def scale (self, factor):
		return Vector3((self.x * factor), (self.y * factor), (self.z * factor))
	
	def length (self):
		return math.sqrt (self.dot (self))
	
	def dot (self, other):
		return ((self.x * other.x) + (self.y * other.y) + (self.z * other.z))
	
	def cross (self, other):
		return Vector3(
			((self.y * other.z) - (self.z * other.y)),
			((self.z * other.x) - (self.x * other.z)),
			((self.x * other.y) - (self.y * other.x))
		)
	
	def print_self (self):
		print (self.stringify())
