class DragAuthController < ApplicationController
	layout 'application'
	
	# show the auth page
	def auth_page
		@words = Words::WORDS # give a list of random words to the view
	end
	
	# ajax reciever: check the submission and see if the user is auth'd
	def check_auth
		passes = JSON.parse(params[:pass])
		pass1 = passes[0].join(',')
		pass2 = passes[1].join(',')
				
		usr = User.where(:pass1 => pass1, :pass2 => pass2).first
		if usr
			render :text => dashboard_path
		else
			render :text => 'FAILED'
		end
	end
	
	def dashboard
		@users = User.all
	end
end
