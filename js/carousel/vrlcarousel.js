/*
	vrlCarousel
	
	author:			Erik S. Rapert
	date:			09-28-2012
	for:			VR Lab at the Center for BrainHealth
	description:	A looping carousel partly borrowed from here:
						http://tympanus.net/codrops/2011/08/16/circular-content-carousel/
	
	REQUIRED JAVASCRIPT LIBS:
		jquery_1.8+
		jquery_easing_1.3+
		jquery_mousewheel
	
	NOTES:
		Click the L and R arrows (if you set them up) or
		mouse-over and roll the mouse wheel to scroll (requires jquery_mousewheel).
	
		Init by doing this:
			var carousel = $('#vrlCarouselContainer').vrlCarousel ();
	
		You can pass a hash of options to the constructor (defaults are shown):
			sliderSpeed		: 100,			// speed for the sliding animation
			sliderEasing	: 'easeOutExpo',// easing for the sliding animation
			itemSpeed		: 100,			// speed for the item animation (open / close)
			itemEasing		: 'easeOutExpo',// easing for the item animation (open / close)
			scroll			: 1,			// number of items to scroll at a time
			maxDisplayed	: 5				// max items to display at a time
		For example, to override the default slider speed:
			var carousel = $('#vrlCarouselContainer').vrlCarousel ({sliderSpeed: 500});
	
	IMPORTANT:
		1. Your HTML must have an element with id="vrlCarouselContent" inside the vrlCarouselContainer.
		2. Each item in the carousel must be inside a <div class="teamMemberList-###"> where '###' is a number.
		3. If you want L and R arrow buttons they must have the following ids:
			<div id="vrlCarousel_navL"></div>
			<div id="vrlCarousel_navR"></div>
*/
;(function($) {
	var	aux = {
		// navigates left / right
		navigate: function (dir, $el, $wrapper, opts, cache, callback) {
			
			var scroll		= opts.scroll,
				factor		= 1,
				theClone	= null,
				idxClicked	= 0;
			
			// clone the elements on the right / left and append / prepend them according to dir and scroll
			if (dir === 1) {
				$wrapper.find('.teamMemberList:lt(' + scroll + ')').each(function(i) {
					theClone = $(this).clone (true).css ('left', (cache.totalItems - idxClicked + i) * cache.itemW * factor + 'px').appendTo ($wrapper);
				});
			} else {
				var $first	= $wrapper.children ().eq (0);
				
				$wrapper.find('.teamMemberList:gt(' + ( cache.totalItems  - 1 - scroll ) + ')').each(function(i) {
					// insert before $first so they stay in the right order
					theClone = $(this).clone (true).css ('left', -(scroll - i + idxClicked) * cache.itemW * factor + 'px').insertBefore ($first);
				});
			}
			
			if (callback && typeof callback == 'function')
				callback.call (theClone);
			
			cache.numDoneAnimating = 0;
			
			// animate the left of each item
			// the calculations are dependent on dir and on the cache.expanded value
			$wrapper.find('.teamMemberList').each (function (i) {
				var $item = $(this);
				$item.stop ().animate (
					{ left: (dir === 1) ? '-=' + ( cache.itemW * factor * scroll ) + 'px' : '+=' + ( cache.itemW * factor * scroll ) + 'px' },
					opts.sliderSpeed,
					opts.sliderEasing,
					function () {
						// when all children have finished animating this will then kill off the clone
						var kids = $wrapper.children ();
						cache.numDoneAnimating++;
			
						// if this is the last kid to chime in then kill the clone
						if (cache.numDoneAnimating >= kids.length) {
							if (dir === 1)
								kids.first ().remove ();
							else // if (dir === -1)
								kids.last ().remove ();
				
							cache.numDoneAnimating = 0;
							cache.isAnimating = false;
						
						}
					}
				);
			});
		}
	},
	methods = {
		init: function (options) {
		
			// don't do anything unless the jquery actually selected something
			if (this.length) {
			
				var settings = {
					sliderSpeed		: 100,			// speed for the sliding animation
					sliderEasing	: 'easeOutExpo',// easing for the sliding animation
					itemSpeed		: 100,			// speed for the item animation (open / close)
					itemEasing		: 'easeOutExpo',// easing for the item animation (open / close)
					scroll			: 1,			// number of items to scroll at a time
					maxDisplayed	: 5				// max items to display at a time
				};
			
				return this.each (function () {
				
					// if options were passed in then lets merge them with our default settings
					if (options) { $.extend (settings, options); }
				
					var $el = $(this);
					var $wrapper = $el.find('div#vrlCarouselContent');
					var $items = $wrapper.children('.teamMemberList');
				
					// figure out how wide the children should be-- depends on how many there are and the max num displayed
					var numToDisplay = ($items.length < settings.maxDisplayed) ? $items.length : settings.maxDisplayed;
					var eachWidth = $wrapper.width () / numToDisplay;
				
					// the items will have position absolute
					// calculate the left of each item
					$items.each(function(i) {
						$(this).css({
							position	: 'absolute',
							width		: eachWidth,
							left		: i * eachWidth + 'px'
						});
					});
				
					var cache = {
						itemW: eachWidth,
						totalItems: $items.length,
						numDoneAnimating: 0
					};
				
					// cap the scroll value
					if( settings.scroll < 1 )
						settings.scroll = 1;
					else if( settings.scroll > 3 )
						settings.scroll = 3;
				
					var $navPrev		= $el.find('#vrlCarousel_navL'),
						$navNext		= $el.find('#vrlCarousel_navR');
				
					// hide the items except the first ones
					$wrapper.css ('overflow', 'hidden');
				
					// navigate left
					$navPrev.bind('click.vrlCarousel', function( event ) {
						if( cache.isAnimating ) return false;
						cache.isAnimating	= true;
						aux.navigate( -1, $el, $wrapper, settings, cache );
					});
				
					// navigate right
					$navNext.bind('click.vrlCarousel', function( event ) {
						if( cache.isAnimating ) return false;
						cache.isAnimating	= true;
						aux.navigate( 1, $el, $wrapper, settings, cache );
					});
				
					// adds events to the mouse
					$el.bind('mousewheel.vrlCarousel', function(e, delta) {
						if (delta > 0) {
							if( cache.isAnimating ) return false;
							cache.isAnimating	= true;
							aux.navigate( -1, $el, $wrapper, settings, cache );
						} else {
							if( cache.isAnimating ) return false;
							cache.isAnimating	= true;
							aux.navigate( 1, $el, $wrapper, settings, cache );
						}
						return false;
					});
				
				});
			}
		}
	};
	
	$.fn.vrlCarousel = function(method) {
		if ( methods[method] ) {
			return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.vrlCarousel' );
		}
	};
	
})(jQuery);
