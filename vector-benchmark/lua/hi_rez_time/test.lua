require 'hrtime'
local timer = require 'timer'


function pretend_to_work (howLong)
	local tmp = 1
	for i = 1, howLong, 1 do
		tmp = tmp + 1
	end
end



print ('test hrt_getTime():')
	local start = hrt_getTime ()
	print ('start time: ' .. start)
	
	pretend_to_work (100000000)
	
	local stop = hrt_getTime ()
	print ('stop time: ' .. stop)
	
	print ('difference: ' .. string.format ("%.16f", (stop - start)))

print ('test timer.lua')
	timer:start ()
	pretend_to_work (1000000000)
	timer:stop ()
	print ('work took ' .. timer:durationStr ())
	
print ('done.')

