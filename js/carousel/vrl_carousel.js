;(function ($) {
	
	$.fn.vrlCarousel = function (options) {
		var metaOpts = this.data ();
		
		this.opts = $.extend ({
			initialPos: 0,
			maxDisplayed: 0
		}, metaOpts, options || {});
		
		/* vars to keep track of where I am and how many I have */
		this.currPos = 0;
		this.numToDisplay = 0;
		this.userLists = null;
		this.eachWidth = 0;
		this.currItem = null;
		
		/* methods */
		this.scrollL = function () {
			this.currPos -= 1;
			//this.animate ({left: ((-1 * this.currPos) * this.eachWidth)}, 100);
			console.log ('scrollL-- currPos: ' + parseInt(this.currPos));
			// TODO: pop off the left side and push onto the right
		}
		
		this.scrollR = function () {
			this.currPos += 1;
			//this.animate ({left: ((-1 * this.currPos) * this.eachWidth)}, 100);
			console.log ('scrollR-- currPos: ' + parseInt(this.currPos));
			// TODO: pop off the right side and push onto the left
		}
		
		// layout all my children to fit evenly in the screen space provided
		this._initLayout = function () {
			var totalWidth = this.width (); // get how much horizontal space we have available
			this.eachWidth = totalWidth / this.numToDisplay;
			
			for (var i = 0; i < this.userLists.length; i++) {
				this.userLists [i].style.width = this.eachWidth;
			}
		}
		
		this.init = function () {
			this.userLists = this.children ();
			this.currPos = this.opts.initialPos;
			this.numToDisplay = (this.userLists.length < this.opts.maxDisplayed) ? this.userLists.length : this.opts.maxDisplayed;
			
			this._initLayout ();
		}
		
		return this;
	}
	
})(jQuery, document, window);
