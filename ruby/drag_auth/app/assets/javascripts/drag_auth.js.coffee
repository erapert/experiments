# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

# requires:
#	jquery
#	jquery-ui
#
# private funcs are outside of window.DragAuth
# exported funcs are added to window.DragAuth



window.DragAuth = {
	#onDragStart: (event, element) ->
	#	console.log 'drag start'

	#onDragStop: (event, element) ->
	#	console.log 'drag stop'
	
	# args:
	#	$dndContainers = jquery obj containing all the items to be made drag-n-droppable containers
	#	authTargetContainers = array of dnd targets which should hold the "password" in the form of jquery queries
	initSort: ($dndContainers, authTargetContainers) ->
		$dndContainers.sortable({
			connectWith: '.connectedSortable',
			placeholder: 'sortablePlaceHolder',
			zIndex: 1000
			#start: this.onDragStart,
			#stop: this.onDragStop
		})
		this.authContainers = authTargetContainers
	
	gatherAuths: () ->
		auths = []
		$.each this.authContainers, (index, container) ->
			auths[index] = []
			$(container).children().each (i, child) ->
				word = $(child).data('word')
				auths[index].push word
		return auths
	
	authorize: (btn) ->
		auths = this.gatherAuths()
		$.ajax({
			url: '/auth',
			type: 'POST',
			data: { pass: JSON.stringify(auths) }
		}).success (msg) ->
			if msg == 'FAILED'
				$('#authMsgs').html 'Incorrect combo.'
			else
				window.location = msg
}

