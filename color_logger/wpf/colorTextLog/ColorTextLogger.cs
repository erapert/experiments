﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace colorTextLog {
	class ColorTextLogger {
		public enum ColorMode { NoColor, SolidColor, EmbeddedColor }
		public ColorMode currentColorMode;
		public Color defaultColor;
		public Color currentSolidColor;

		private RichTextBox myrtb;
		private struct ColorChunk {
			public string text;
			public Color color;
		}
		private Dictionary<string, Color> colorTags;
		private Regex splitRegex; // used to split the string into an array of text and tags
		private Regex matchRegex; // used to decide if a string is a color tag
		// color tags are surrounded by these. i.e. "uncolored [r]RED[gg]GREY[g]GREEN[/] uncolored"
		// a backslash is hard-coded in front of both of these so that the regex can handle them
		// don't change them to something that doesn't need a backslash... i.e. don't use < and >
		private string openBracket = @"[";
		private string closeBracket = @"]";
		// how many indentation levels
		private int ind;
		// how many spaces per indentation level
		private int indSize;
		public int indentationSize {
			get { return indSize; }
			set { setIndentSize (value); }
		}
		// character to use when indenting
		private char indentChar = ' ';
		// spaces we add to the beginning when logging a string
		private string indStr;

		public ColorTextLogger (RichTextBox rtbHandle) {
			myrtb = rtbHandle;
			currentSolidColor = defaultColor = Colors.DarkGray;
			currentColorMode = ColorMode.NoColor;
			ind = 0;
			indSize = 4;
			indStr = new string (' ', ind * indSize);
			initColorTags ();
			initRegexes ();

			//
			// Turns off double spacing.
			// You can also add this to the xaml for the RichTextBox to do the same thing:
			// 
			// <RichTextBox.Resources>
			// 	<Style TargetType="{x:Type Paragraph}">
			// 		<Setter Property="Margin" Value="0"/>
			// 	</Style>
			// </RichTextBox.Resources>
			//
			var styling = new Style (typeof (Paragraph));
			styling.Setters.Add (new Setter (Paragraph.MarginProperty, new Thickness (0)));
			myrtb.Resources.Add (typeof (Paragraph), styling);
		}

		// this should be what you call when you want to log a line of text
		// it takes the current color mode into account, prepends indentation and appends a newline
		public ColorTextLogger logln (string text) {
			string msg = indStr + text + "\n";
			switch (currentColorMode) {
				case ColorMode.EmbeddedColor:
					return embeddedColorLog (msg);
				case ColorMode.SolidColor:
					return colorLog (msg, currentSolidColor);
				case ColorMode.NoColor:
					return colorLog (msg, defaultColor);
				default:
					throw new Exception (@"Invalid color mode: " + currentColorMode.ToString ());
			}
		}
		// call this when you don't want indentation and newline added
		public ColorTextLogger log (string msg) {
			switch (currentColorMode) {
				case ColorMode.EmbeddedColor:
					return embeddedColorLog (msg);
				case ColorMode.SolidColor:
					return colorLog (msg, currentSolidColor);
				case ColorMode.NoColor:
					return colorLog (msg, defaultColor);
				default:
					throw new Exception (@"Invalid color mode: " + currentColorMode.ToString ());
			}
		}

		// color the whole line with the same color
		public ColorTextLogger colorLog (string text, Color? color = null) {
			Color brushColor = (Color) ((color != null) ? color : currentSolidColor);
			// have to do it this way instead of just myrtb.AppendText() in order to get it styled...

			// get the end of the doc
			var range = new TextRange (myrtb.Document.ContentEnd, myrtb.Document.ContentEnd);
			// insert the text
			range.Text = text;
			// style it
			range.ApplyPropertyValue (TextElement.ForegroundProperty, new SolidColorBrush (brushColor));

			return this;
		}

		// color is embedded in the string
		public ColorTextLogger embeddedColorLog (string text) {
			var chunks = colorSplit (text);
			foreach (var chunk in chunks) {
				var range = new TextRange (myrtb.Document.ContentEnd, myrtb.Document.ContentEnd);
				range.Text = chunk.text;
				range.ApplyPropertyValue (TextElement.ForegroundProperty, new SolidColorBrush (chunk.color));
			}
			return this;
		}

		// use this to set the current color mode
		public ColorTextLogger mode (ColorMode mode, Color? newColor = null) {
			currentColorMode = mode;
			if (mode == ColorMode.SolidColor) {
				if (newColor != null) {
					currentSolidColor = (Color) newColor;
				} else {
					currentSolidColor = (Color) defaultColor;
				}
			}
			return this;
		}

		public ColorTextLogger indent (int i) {
			ind += i;
			if (ind < 0) { ind = 0; }
			rebuildIndentStr ();
			return this;
		}
		public ColorTextLogger resetIndent () {
			ind = 0;
			rebuildIndentStr ();
			return this;
		}
		public ColorTextLogger setIndent (int i) {
			ind = (i < 0) ? 0 : i;
			rebuildIndentStr ();
			return this;
		}
		// this method is chainable, the property setter isn't
		public ColorTextLogger setIndentSize (int i) {
			indSize = (i < 0) ? 0 : i;
			return this;
		}

		public ColorTextLogger logColorTags () {
			foreach (var tag in colorTags) {
				var str = openBracket + tag.Key + closeBracket + " : " + tag.Value.ToString ();
				this.colorLog (indStr + str + "\n", tag.Value);
			}
			return this;
		}

		
		/** internal stuff below **/

		private void rebuildIndentStr () {
			indStr = new string (indentChar, ind * indSize);
		}

		private List<ColorChunk> colorSplit (string text) {
			var chunks = new List<ColorChunk>();
			var splits = splitRegex.Split (text);
			for (var i = 0; i < splits.Length - 1; i++) {
				ColorChunk chunk;
				var m = matchRegex.Match (splits [i]);
				if (m.Success) {
					// if this is a color tag then it applies to the next chunk after it
					// so advance to the next section and give it my color
					chunk = new ColorChunk { text = splits[++i], color = lookUpColor (m.Value) };
				} else {
					// if this section isn't a color tag then it must be uncolored
					chunk = new ColorChunk { text = splits [i], color = defaultColor };
				}
				chunks.Add (chunk);
			}
			return chunks;
		}

		private Color lookUpColor (string colorTag) {
			var rtrn = colorTags [colorTag.Substring (1, colorTag.Length - 2)];
			if (rtrn == null) { rtrn = defaultColor; }
			return rtrn;
		}

		// the brackets are handled by the regex
		private void initColorTags () {
			this.colorTags = new Dictionary<string, Color> () {
				{"w", Colors.White},
				{"lgg", Colors.LightGray},
				{"gg", Colors.Gray},
				{"dgg", Colors.DarkGray},
				{"bb", Colors.Black},
				{"lr", Colors.Pink},
				{"r", Colors.Red},
				{"dr", Colors.DarkRed},
				{"lg", Colors.LightGreen},
				{"g", Colors.Green},
				{"dg", Colors.DarkGreen},
				{"lb", Colors.LightBlue},
				{"b", Colors.Blue},
				{"db", Colors.DarkBlue},
				{"c", Colors.Cyan},
				{"m", Colors.Magenta},
				{"y", Colors.Yellow},
				{"/", defaultColor}
			};
		}

		private void initRegexes () {
			var rstr = "";
			foreach (var tag in colorTags) {
				rstr += tag.Key + "|";
			}
			rstr = rstr.Substring (0, rstr.Length - 1); // chop off the last pipe
			var matchRstr = @"\" + openBracket + "(" + rstr + ")" + @"\" + closeBracket;
			this.matchRegex = new Regex (matchRstr);
			var splitRstr = rstr.Replace ("|", @"\" + closeBracket + @")|(\" + openBracket);
			splitRstr = @"(\" + openBracket + splitRstr + @"\" + closeBracket + ")";
			this.splitRegex = new Regex (splitRstr);
		}
	}
}
