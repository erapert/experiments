using System;

namespace VRL {
	class Vector3 {
		float x;
		float y;
		float z;
		
		public Vector3 () {
			set (0, 0, 0);
		}
		
		public Vector3 (float X, float Y, float Z) {
			set (X, Y, Z);
		}
		
		public void set (float X, float Y, float Z) {
			this.x = X;
			this.y = Y;
			this.z = Z;
		}
		
		public string stringify () {
			return string.Format ("({0}, {1}, {2})", x, y, z);
		}
		public bool equals (Vector3 other) {
			return ((x == other.x) && (y == other.y) && (z == other.z));
		}
		public Vector3 plusScalar (float factor) {
			return new Vector3 ((x + factor), (y + factor), (z + factor));
		}
		public Vector3 minusScalar (float factor) {
			return new Vector3 ((x - factor), (y - factor), (z - factor));
		}
		public Vector3 plus (Vector3 other) {
			return new Vector3 ((x + other.x), (y + other.y), (z + other.z));
		}
		public Vector3 minus (Vector3 other) {
			return new Vector3 ((x - other.x), (y - other.y), (z - other.z));
		}
		public Vector3 transform (float X, float Y, float Z) {
			return new Vector3 ((x + X), (y + Y), (z + Z));
		}
		public Vector3 scale (float factor) {
			return new Vector3 ((x * factor), (y * factor), (z * factor));
		}
		public float length () {
			return (float) Math.Sqrt ((double) dot (this));
		}
		public float dot (Vector3 other) {
			return ((x * other.x) + (y * other.y) + (z * other.z));
		}
		public Vector3 cross (Vector3 other) {
			return new Vector3 ((x * other.x), (y * other.y), (z * other.z));
		}
		public void print_self () {
			Console.WriteLine (stringify ());
		}
	}
}