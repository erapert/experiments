/*
	NOTES:
		- requires VRL to be an existing "namespace" object
		- to create a new instance do:
			var myCanvas = new VRL.CanvasWidget (document.getElementById ('some_canvas'));
*/
VRL.CanvasWidget = (function () {
	// privates
	
	/*
		CONSTRUCTOR
		args:
			canvasElement: element to init with
	*/
	function CanvasWidget (canvasElement) {
		this.canvas = canvasElement;
		this.context = this.canvas.getContext ('2d');
		this.context.lineCap = 'round'; // no jaggies if you please
		this.penIsDown = false;
		this.penColor = '#000000';
		this.penThickness = 1;
		this.penPos = {};
	}
	
	// publics
	CanvasWidget.prototype.drawLine = function (color, thickness, x1, y1, x2, y2) {
		var c = this.context;
		c.strokeStyle = color;
		c.lineWidth = thickness;
		c.beginPath ();
		c.moveTo (x1, y1);
		c.lineTo (x2, y2);
		c.stroke ();
	}
	CanvasWidget.prototype.clearCanvas = function () {
		// save any transforms because clearing will reset them
		this.context.save ();
		this.context.setTransform (1, 0, 0, 1, 0, 0);
		this.context.clearRect (0, 0, this.canvas.width, this.canvas.height);
		this.context.restore ();
	}
	// use this to set the pen to 'eraser' rather than setting penColor directly
	CanvasWidget.prototype.useEraser = function () {
		this.context.globalCompositeOperation = 'destination-out';
		this.context.strokeStyle = 'rgba (0, 0, 0, 1.0)';
	}
	CanvasWidget.prototype.setPenColor = function (color) {
		this.context.globalCompositeOperation = 'source-over';
		this.penColor = color;
	}
	CanvasWidget.prototype.setPenThickness = function (thickness) {
		this.penThickness = thickness;
	}
	
	return CanvasWidget;
})();