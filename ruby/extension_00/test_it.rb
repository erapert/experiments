#!/usr/bin/ruby

# import the .so file
require 'mytest'

# import the module itself so we can get at its methods
include MyTest

# print out the number-- should be 42

puts "you should now see the number forty-two: #{MyTest::get_num()}"

