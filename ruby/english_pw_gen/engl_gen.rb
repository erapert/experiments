require 'active_record'

class Words < ActiveRecord::Base
end

ActiveRecord::Base.establish_connection({
	:host => 'bhathens',
	:adapter => 'mysql2',
	:username => 'tosl',
	:password => 'tosl_password',
	:database => 'tosl_development'
})

rand_words = Words.order('RAND()').first(3)

#engl_pass = "#{rand_words[0].word}_#{rand_words[1].word}_#{rand_words[2].word}"
engl_pass = ''
rand_words.each do |w|
	engl_pass += w.word + '_'
end
engl_pass.chomp!('_')

puts "pass: \"#{engl_pass}\""
