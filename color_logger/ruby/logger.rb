class Logger
	attr_accessor :indentSize, :enableColor
	
	def initialize indentSize=1, enableColor=true
		@ind = 0
		@indentSize = indentSize
		@enableColor = enableColor
		@colors = {
			'[.]' => ["\033[90m", 'grey'],
			'[r]' => ["\033[91m", 'red'],
			'[g]' => ["\033[92m", 'green'],
			'[y]' => ["\033[93m", 'yellow'],
			'[b]' => ["\033[94m", 'blue'],
			'[m]' => ["\033[95m", 'magenta'],
			'[c]' => ["\033[96m", 'cyan'],
			'[w]' => ["\033[97m", 'white'],
			'[/]' => ["\033[0m", 'end color/reset']
		}
	end
	
	def log msg
		i = _getIndentStr
		if @enableColor
			m = _colorize msg
			puts "#{i}#{m}#{@colors['[/]']}"
		else
			puts "#{i}#{m}"
		end
		return self
	end
	
	def indent i=0
		@ind += i
		@ind = 0 if @ind < 0
		return self
	end
	
	def resetIndent
		@ind = 0
		return self
	end
	
	def setIndent i
		@ind = i
		@ind = 0 if @ind < 0
		return self
	end
	
	def _colorize msg
		rtrn = msg.dup
		@colors.each do |tag, color|
			rtrn.gsub! tag, color[0]
		end
		return rtrn
	end
	
	def _getIndentStr
		return ' ' * (@ind * @indentSize)
	end
	
	def _showColorCodes
		i = _getIndentStr
		@colors.each do |tag, color|
			puts "#{i}'#{tag}' => #{color}"
		end
	end
end


if __FILE__ == $0
	# You can set how many spaces are used for display and you can turn off color
	l = Logger.new indentSize=2, enableColor=false
	
	# don't worry, you can enable color later:
	l.enableColor = true
	
	l._showColorCodes
	#	'[.]' = '\033[90m' # grey
	#	'[r]' = '\033[91m' # red
	#	'[g]' = '\033[92m' # green
	#	'[y]' = '\033[93m' # yellow
	#	'[b]' = '\033[94m' # blue
	#	'[m]' = '\033[95m' # magenta
	#	'[c]' = '\033[96m' # cyan
	#	'[w]' = '\033[97m' # white
	#	'[/]' = '\033[0m'  # end color/reset
	
	# you can chain the public methods (except for the constructor which Python restrics)
	l.resetIndent.log 'Codes will be replaced with the proper ANSI escape sequences-- [c]they[y]do[r]not[g]turn[b]into[w]whitespace.[/]'
	
	# by the way, disabling color should run faster since it's no longer inserting the color codes
	l.enableColor = true
	l.log 'If you forget to use `[/]` to end a color'
	l.enableColor = true
	l.indent(1).log '[b]it will run \'till it finds a new color tag or to the end of the string.'
	l.indent(-1).log 'Because of this you don\'t have to terminate color before setting to a new color. Check this out:'
	l.indent(1).log '[r]RED [g]GREEN [b]BLUE[/] And this is uncolored.'
	
	l.resetIndent.log 'Setting indentation to less than zero simply resets to zero:'
	l.indent(5).log 'Indented to 5.'
	l.setIndent(-1000).log 'Indented to -1000.'
	
	# manually reseting the console colors in case testing screwed up
	puts "\033[0m"
end

