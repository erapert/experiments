#!/usr/bin/ruby

require 'optparse'

# ruby 1.8.7 doesn't support require_relative
# and newer versions of ruby don't want to accept a relative require path
if RUBY_VERSION == '1.8.7'
	def require_relative(x)
		print ''
	end
end
if RUBY_VERSION == '1.8.7'
	require 'bin/console_notifier.rb'
	require 'bin/fhelper.rb'
else
	require_relative 'bin/console_notifier.rb'
	require_relative 'bin/fhelper.rb'
end



# does the work
class Stitcher
	attr_accessor :output, :imgs, :exe, :sort, :size, :tile
	
	SORT_TYPES = {:none => 0, :numeric => 1, :alphabetic => 2}

	# constructors in ruby are named "initialize"
	def initialize()
		@output = @imgs = nil
		@sort = SORT_TYPES[:numeric]
		@exe = 'montage'
		@size = '256x256'
		@tile = '16x'
	end

	def process()
		case @sort
		when SORT_TYPES[:numeric]
			# sort numerically, not alphabetically
			# in order to preserve the intended order:
			#	1, 2, 3, 4 ... 11, 12, 13 ...
			# not
			#	1, 10, 11, 12, 13, ... 2, 21, 22 ...
			@imgs.sort! { |a,b|
				ai = File.basename(a).chomp(File.extname(a)).to_i
				bi = File.basename(b).chomp(File.extname(b)).to_i
				ai <=> bi;
			}
		when SORT_TYPES[:alphabetic]
			@imgs.sort!
		#else
			# don't sort
		end
		
		ConsoleNotifier.notify "stitching..."
	
		infiles = '"' + @imgs.join('" "') + '"'
		tile = @tile ? "-tile #{@tile}" : nil
		size = @size ? "-geometry #{@size}" : nil
		
		# -monitor tells montage to output progress
		`#{@exe} -monitor #{size} #{tile}  #{infiles} #{@output}`
		#ConsoleNotifier.important "#{@exe} -monitor #{size} #{tile} #{infiles} #{@output}"
	
		if File.exists? @output
			ConsoleNotifier.notify "Done. output: '#{@output}'"
		else
			ConsoleNotifier.error "Failed to write output file '#{@output}'-- check permissions?"
		end
	end
end


# main()
if __FILE__ == $0
	vs = Stitcher.new
	
	parser = OptionParser.new do |o|
		o.on('-o', '--output FILENAME', 'Name of output file.') {
			|out| vs.output = out
		}
		
		o.on('-n', '--numericsort', 'Turn on numeric sort (default): files are sorted numerically.') {
			|n| vs.sort = Stitcher::SORT_TYPES[:numeric]
		}
		
		o.on('-a', '--alphasort', 'Turn on alphabetic sorting: files are sorted alphabetically.') {
			|a| vs.sort = Stitcher::SORT_TYPES[:alphabetic]
		}
		
		o.on('-l', '--nosort', 'Turn off sorting, use the files in the order given.') {
			|l| vs.sort = Stitcher::SORT_TYPES[:none]
		}
		
		o.on('-d', '--dir DIRECTORY', 'All files in this directory will be stitched together.
					No trailing slash: "./some/directory" NOT "./some/directory/".') {
			|dir| vs.imgs = FHelper.collect_dir_filenames(dir)
		}
		
		o.on('-e', '--exe PATH_TO_MONTAGE_EXE', 'Full path to the "montage" executable from ImageMagick.
					use this if montage isn\'t on your PATH variable') {
			|e| vs.exe = e
		}
		
		o.on('-s', '--size SIZE', 'Size of each input image. (256x256 by default)') {
			|s| vs.size = s
		}
		
		o.on('-x', '--xdim XDIMENSION', 'How many tiles accross. (is set to result in 16x16 by default)') {
			|x| vs.tile = x + 'x'
		}
		
		o.on('-i', '--images CSV_IMG_NAMES', 'Pass image names on the command line. NO SPACES!
					Must look like:
					/full/path/to/image0.jpg,relative/path/image1.jpg,image2.jpg,some_dir/image3.jpg') {
			|imgs| vs.imgs = FHelper.collect_arg_filenames(imgs)
		}
	end
	
	if ARGV.length == 0
		puts parser.help()
		exit(1)
	else
		begin parser.parse!
		rescue OptionParser::InvalidOption => e
			ConsoleNotifier.error "Invalid option. #{e}"
			puts parser.help()
			exit(1)
		end
		
		if !vs.output
			ConsoleNotifier.error 'You must specify a filename to output to.'
			puts parser.help()
			exit(1)
		end
		
		if !vs.imgs
			ConsoleNotifier.error 'You must specify input files.'
			puts parser.help()
			exit(1)
		end
	
		vs.process()
	end
end
