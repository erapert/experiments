class ServerEntry < ActiveRecord::Base
	# int:		id
	# string:	name
	# string:	ip
	# string:	port
	# int:		flags
	# datetime:	start
	
	#
	#	args: nil
	#	returns: string: comma separated values--> "2,some_server,127.0.0.1,5478,19"
	#
	def stringify
		return "#{id},#{name},#{ip},#{port},#{flags},#{start}"
	end
end

