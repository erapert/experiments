require 'pp'
require 'optparse'

def filter_file args
	if !File.exists? args[:fname]
		raise "!ERROR! File does not exist? '#{args[:fname]}'" 
	end
	
	if args[:wordslist]
		badwords = args[:wordslist].split(',')
	else
		badwords = nil
	end
	
	if args[:nospaces]
		nospaces = true
	else
		nospaces = false
	end
	
	if args[:alphaonly]
		alphaonly = true
	else
		alphaonly = false
	end
	
	if args[:badchars]
		badchars = args[:badchars].split(',')
	else
		badchars = nil
	end
	
	if args[:chopafter]
		chopafter = args[:chopafter]
	else
		chopafter = nil
	end
	
	if args[:dropchars]
		dropchars = args[:dropchars].split //
	else
		dropchars = nil
	end
	
	if args[:caps]
		caps = args[:caps]
	else
		caps = false
	end
	
	infile = File.open args[:fname], 'r'
	outfile = File.open "FILTERED_#{args[:fname]}", 'w'
	
	while line = infile.gets
		word = line.strip
		
		# these filters will reject the whole line
		
		if nospaces and word.match /\s/
			puts "SPACES NOT ALLOWED: '#{word}'"
			next
		end
		
		if alphaonly and word =~ /[^a-zA-Z]/
			puts "NON-ALPHABETIC NOT ALLOWED: '#{word}'"
			next
		end
		
		if (badwords != nil and badwords.include?(word))
			puts "BAD WORD: '#{word}'"
			next
		end
		
		if badchars and has_bad_chars?(word, badchars)
			puts "BAD CHARS IN: '#{word}'"
			next
		end
		
		# after this point the filters will modify the line
		
		if chopafter
			ind = word.index(chopafter)
			if ind
				w = (word[0 .. ind - 1]).rstrip
				puts "CHOPPED: '#{word}' -> '#{w}'"
			else
				w = word
			end
		else
			w = word
		end
		
		if dropchars
			dropchars.each{ |dc| w = w.delete(dc) }
		end
		
		if caps
			if caps == 'caps'
				w.capitalize!
			elsif caps == 'down'
				w.downcase!
			end
		end
		
		outfile.puts w
	end
	
	infile.close
	outfile.close
end

def has_bad_chars? str, badchars
	badchars.each do |bc|
		if str.include? bc
			return true
		end
	end
	return false
end

if __FILE__ == $0
	args = {}
	parser = OptionParser.new do |o|
		o.on('-f', '--file FILE', 'Input file, each line to be filtered.') { |f| args[:fname] = f }
		o.on('-s', '--nospaces [NOSPACES]', 'Drop any line with whitespace in it.') { |s| args[:nospaces] = true }
		o.on('-w', '--wordlist [CSVSTRING]', 'Comma-separated list of words to be dropped.') { |w| args[:wordslist] = w }
		o.on('-c', '--badchars [CSVSTRING]', 'Comma-separated list of characters that are not allowed.') { |c| args[:badchars] = c }
		o.on('-a', '--alphaonly [ALPHAONLY]', 'If set then only lines with alphabetic characters are allowed.') { |a| args[:alphaonly] = true }
		o.on('-x', '--chopafter [STRING]', 'If string is found then everything after it is dropped and the rest of the line is kept.') { |x| args[:chopafter] = x }
		o.on('-d', '--dropchars [STRING]', 'Each character in the string, if found, will be removed from the output.') { |d| args[:dropchars] = d }
		o.on('-l', '--capitalize [caps|down]', 'Capitalize or lowercase every line. "caps" = capitalize, "down" = downcase.') { |caps| args[:caps] = caps }
	end
	
	if ARGV.length == 0
		puts parser.help
	else
		parser.parse!
		if !args[:fname]
			puts parser.help
		else
			filter_file args
			#pp args
		end
	end
end

