using System;
using System.Collections.Generic;
using NDesk.Options;				// great, Mono doesn't have a built-in arg parser so I had to go find one
using System.Diagnostics;			// Stopwatch is in here

using VRL; // namespace of Vector3

class Test {
	
	static private void unitTest () {
		Vector3 t0 = new Vector3 (1, 1, 1);
		Vector3 t1 = new Vector3 (1, 1, 1);
		Vector3 t2 = new Vector3 (2, 2, 2);
		
		Console.WriteLine ("t0 == t1: {0}", t0.equals (t1));
		Console.WriteLine ("t1 == t2: {0}", t1.equals (t2));
		
		t0.set (2, 2, 2);
		Console.WriteLine ("t0: {0}", t0.stringify ());
		t1.set (8, 8, 8);
		Console.WriteLine ("t1: {0}", t1.stringify ());
		t2.set (16, 16, 16);
		Console.WriteLine ("t2: {0}", t2.stringify ());
		
		Vector3 temp;
		temp = t0.plusScalar (2);
		Console.WriteLine ("t0 + 2 = {0}", temp.stringify ());
		
		Console.WriteLine ("t0.plus(t1): {0}", t0.plus(t1).stringify());
		Console.WriteLine ("t2.minus(t1): {0}", t2.minus(t1).stringify());
		
		Console.WriteLine ("t0.transform(0, 8, 0): {0}", t0.transform(0, 8, 0).stringify());
		
		Console.WriteLine ("t0.scale(0.5): {0}", t0.scale(0.5f).stringify());
		
		Console.WriteLine ("t1.length: {0}", t1.length());
		Console.WriteLine ("t2.length: {0}", t2.length());
		
		Console.WriteLine ("t0.dot(t1): {0}", t0.dot(t1));
		
		Console.WriteLine ("t1.cross(t2): {0}", t1.cross(t2).stringify());
		
		t1.set (1, 2, 3);
		t2.set (4, 5, 6);
		Console.WriteLine ("t1.set(1, 2, 3): {0}", t1.stringify());
		Console.WriteLine ("t2.set(4, 5, 6): {0}", t2.stringify());
		Console.WriteLine ("t1.cross(t2): {0}", t1.cross(t2).stringify());
	}
	
	static private void benchmark () {
		Stopwatch sw = new Stopwatch ();
		
		Vector3 t0 = new Vector3 (1, 2, 3);
		Vector3 t1 = new Vector3 (4, 5, 6);
		
		Console.WriteLine ("timing individual operations...");
		
		String tempStr;
		Vector3 tempV = new Vector3 ();
		float tempF;
		bool tempB;
		
		sw.Start ();
		t0.set (1, 2, 3);
		sw.Stop ();
		Console.WriteLine ("set: {0}", sw.Elapsed);
		
		sw.Start ();
		tempB = t0.equals (t1);
		sw.Stop ();
		Console.WriteLine ("equals: {0}", sw.Elapsed);
		
		sw.Start ();
		tempStr = t0.stringify ();
		sw.Stop ();
		Console.WriteLine ("stringify: {0}", sw.Elapsed);
		
		sw.Start ();
		tempV = t0.plusScalar (10.0f);
		sw.Stop ();
		Console.WriteLine ("plusScalar: {0}", sw.Elapsed);
		
		sw.Start ();
		tempV = t0.minusScalar (10.0f);
		sw.Stop ();
		Console.WriteLine ("minusScalar: {0}", sw.Elapsed);
		
		sw.Start ();
		tempV = t0.plus (t1);
		sw.Stop ();
		Console.WriteLine ("plus: {0}", sw.Elapsed);
		
		sw.Start ();
		tempV = t0.minus (t1);
		sw.Stop ();
		Console.WriteLine ("minus: {0}", sw.Elapsed);
		
		sw.Start ();
		tempV = t0.transform (0, 8, 0);
		sw.Stop ();
		Console.WriteLine ("transform: {0}", sw.Elapsed);
		
		sw.Start ();
		tempV = t0.scale (0.5f);
		sw.Stop ();
		Console.WriteLine ("scale: {0}", sw.Elapsed);
		
		sw.Start ();
		tempF = t0.length ();
		sw.Stop ();
		Console.WriteLine ("length: {0}", sw.Elapsed);
		
		sw.Start ();
		tempF = t0.dot (t1);
		sw.Stop ();
		Console.WriteLine ("dot: {0}", sw.Elapsed);
		
		sw.Start ();
		tempV = t0.cross (t1);
		sw.Stop ();
		Console.WriteLine ("cross: {0}", sw.Elapsed);
		
		
		Console.WriteLine ("Doing time trial (all ops in a row)");
		sw.Start ();
		for (int i = 0; i < 1000000; ++i) {
			t0.set (1, 2, 3);
			tempB = t0.equals (t1);
			tempStr = t0.stringify ();
			tempV = t0.plusScalar (10.0f);
			tempV = t0.minusScalar (10.0f);
			tempV = t0.plus (t1);
			tempV = t0.minus (t1);
			tempV = t0.transform (0, 8, 0);
			tempV = t0.scale (0.5f);
			tempF = t0.length ();
			tempF = t0.dot (t1);
			tempV = t0.cross (t1);
		}
		sw.Stop ();
		Console.WriteLine ("time trial: {0}", sw.Elapsed);
	}
	
	static public int Main (string [] args) {
		
		if (args.Length == 0) {
			Console.WriteLine ("no args");
			return 1;
		}
		
		var opts = new OptionSet () {
			{ "h|help", "produce help message", v => printHelp() },
			{ "u|unittest", "run and print unit test", v => unitTest() },
			{ "b|benchmark", "run the benchmark", v => benchmark() }
		};
		opts.Parse (args);
		
		return 0;
	}
	
	// for some reason NDesk.Options doesn't provide an automatic help display... an oversight surely
	static public void printHelp () {
		Console.Write (@"
			Usage:
				-h | --help			show this help message
				-u | --unittest		run and print the unit test
				-b | --benchmark	run the benchmark
		");
	}
}