#include <iostream>
#include <string>

using namespace std;

struct StringRun {
	char longestRunChar;
	int64_t lengthOfRun;
};

StringRun getLongestRun (const std::string & str) {
	auto prevChar = char { };
	auto maxCharSoFar = char { };
	auto maxLenSoFar = int64_t { 0 };
	auto count = int64_t { 1 };
	
	for (auto & currChar : str) {
		if (currChar == prevChar) {
			++count;
			if (count > maxLenSoFar) {
				maxCharSoFar = currChar;
				maxLenSoFar = count;
			}
		} else {
			// if we're looking at a char then we know we have at least one of 'em...
			// so we don't start at zero
			count = 1;
		}
		prevChar = currChar;
	}
	
	return StringRun { maxCharSoFar, maxLenSoFar };
}

int main (int argc, char ** argv) {
	// name of program is first arg...
	// so string to search is second arg
	if (argc != 2) {
		cout << "The only arg this program accepts is a string to search for runs" << endl;
		return 1;
	}
	
	auto testStr = std::string { argv[1] };
	auto longestRun = getLongestRun (testStr);
	
	cout << "Test string: '" << testStr << "'" << endl;
	cout << "Longest run: (" << longestRun.longestRunChar << ", " << longestRun.lengthOfRun << ")" << endl;
	
	return 0;
}
