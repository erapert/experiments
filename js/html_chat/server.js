var io = require ('socket.io').listen (8081);

io.sockets.on ('connection', function (sock) {
	sock.emit ('chat', { msg: 'user connected.' });
	
	sock.on ('chat', function (data) {
		// broadcast to the other clients
		sock.broadcast.emit ('chat', data);
		// send it to the original sender too so they can hear themselves
		sock.emit ('chat', data);
	});
	
	sock.on ('disconnect', function () {	
	});
});
