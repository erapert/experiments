import std.stdio;
import std.string; // used for indexOf() on strings

class Logger {
	struct ColorCode {
		string tag;
		string code;
		string name;
	}
	static ColorCode [] colors = [
		ColorCode ("[.]", "\033[90m", "grey"),
		ColorCode ("[r]", "\033[91m", "red"),
		ColorCode ("[g]", "\033[92m", "green"),
		ColorCode ("[y]", "\033[93m", "yellow"),
		ColorCode ("[b]", "\033[94m", "blue"),
		ColorCode ("[m]", "\033[95m", "magenta"),
		ColorCode ("[c]", "\033[96m", "cyan"),
		ColorCode ("[w]", "\033[97m", "white"),
		ColorCode ("[/]", "\033[0m", "end color/reset")
	];
	uint ind;
	uint indentSize;
	bool enableColor;
	
	this (uint indentSize=1, bool enableColor=true) {
		this.ind = 0;
		this.indentSize = indentSize;
		this.enableColor = enableColor;
	}
	
	auto log (Args...) (string fmt, Args args) {
		auto istr = getIndentStr ();
		auto m = format (fmt, args);
		if (enableColor) {
			// assuming that '[/]' stays at the end of the colors array...
			m = colorize (m);
			writefln ("%s%s%s", istr, m, colors[colors.length - 1].code);
		} else {
			writefln ("%s%s", istr, m);
		}
		return this;
	}
	
	auto indent (int i) {
		ind += i;
		if (ind < 0) { ind = 0; }
		return this;
	}
	
	auto resetIndent () {
		ind = 0;
		return this;
	}
	
	auto setIndent (uint i) {
		ind = i;
		return this;
	}
	
	string colorize (ref string msg) {
		string rtrn = msg;
		foreach (ref c; colors) {
			auto tagStart = rtrn.indexOf (c.tag);
			if (tagStart > -1) {
				rtrn = rtrn[0 .. tagStart] ~ c.code ~ rtrn[(tagStart + c.tag.length) .. rtrn.length];
			}
		}
		return rtrn;
	}
	
	// going with a very simple (slow) implementation here
	string getIndentStr () {
		string rtrn;
		auto len = (ind * indentSize);
		for (auto i = 0; i < len; ++i) {
			rtrn ~= ' ';
		}
		return rtrn;
	}
	
	void showColorCodes () {
		foreach (ref c; colors) {
			writefln ("%s", c);
		}
	}
}

