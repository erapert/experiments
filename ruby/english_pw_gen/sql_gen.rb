
#
#	Used to weed out good/bad words from './wlist_match10.txt'
#	The words are pulled from './wlist_match10.txt' at random if they're less than 7 letters long.
#	Output is an SQL statement that can be used to insert into a table of words.
#

if ARGV[0]
	def get_char
		tty_param = `stty -g`
		system 'stty raw'
		a = IO.read '/dev/stdin', 1
		system "stty #{tty_param}"
		return a
	end

	puts 'reading file...'
	dict = File.open('./wlist_match10.txt').read().split("\n")
	words = []

	puts 'looping...'

	cmd = 'g'
	while cmd != 'e'
		w = dict.sample
		if w.length <= 6
			puts "acceptable? (exit to exit, yes to accept, no to reject) > \"#{w}\""
			cmd = get_char
			if cmd == 'y'
				puts "accepted \"#{w}\""
				words << w
			end
		end
	end

	words.uniq!

	puts 'writing sql...'
	insert = "INSERT INTO `#{ARGV[0]}` (`word`) VALUES "
	words.each do |word|
		insert += " ('#{word}'),\n"
	end
	insert = insert.chomp ",\n"
	insert += ';'
	puts insert

	puts 'terminating...'
else
	puts 'need to pass the table name as arg'
end
