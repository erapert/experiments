/*
	requires (so include these in your HTML before me):
		jquery
		socket.io
		canvas.js -- VRL.CanvasWidget
	
	This object is responsible for:
		- hooking up the canvas controls (pen tool, pen thickness etc.)
		- hooking up the canvas itself (the actual VRL.CanvasWidget)
		- hooking up the socket.io stuff so that things get sent over a socket.io websocket
*/
VRL.MultiUserPaint = (function () {
	/**
		This constructor only inits the first wrapperElement match.
		
		args: an object/hash which contains these keys:
			mandatory:
				socket = socket.io object (i.e. socket = io.connect ('http://localhost'); )
				wrapperElementID = the element which contains the paint stuff and the canvas widget
			optional:
				canvas_width: integer, defaults to 512px
				canvas_height: integer, defaults to 512px
				canvas_bg: string, hex code for <canvas> background color, just like css
	*/
	function MultiUserPaint (args) {
		var _this = this;
		var config = $.extend ({
			canvas_width: 512,
			canvas_height: 512,
			canvas_bg: '#333333'
		}, args);
		if (!config.socket || !config.wrapperElementID) {
			throw 'Socket and wrapperElementID are both required in the config.';
		}
		
		this.$container = null;
		this.canvas = null;
		this.events = {
			onClearCanvas: 0,
			onDrawLine: 1,
			onSetPenColor: 2,
			onSetPenThickness: 3
		}
		
		// build the DOM object, insert it, hook up the listeners, etc.
		this.$container = $(config.wrapperElementID).first().append ('\
			<div id="canvas_controls" style="margin: 8px;">\
				Brush Size: <input type="number" id="brush_size_picker" value="1" min="1" max="20">\
				Brush Color: <input type="color" id="color_picker">\
				<input type="button" id="use_pen" value="Pen">\
				<input type="button" id="use_eraser" value="Eraser">\
				<input type="button" id="clear_canvas" value="Clear">\
			</div>\
			<canvas id="thecanvas" width="'+config.canvas_width+'" height="'+config.canvas_height+'"></canvas>\
		');
		
		// must init the canvas AFTER inserting the DOM stuff
		var $canvas = this.$container.find('#thecanvas');
		$canvas.css ('background-color', config.canvas_bg);
		
		// the callbacks here will be responsible for telling the other clients what I'm up to
		this.canvas = new VRL.CanvasWidget ($canvas[0]);
		
		// hook up the canvas mouse event listeners
		$canvas[0].onmousemove = function (evt) {
			var c = _this.canvas;
			if (c.penIsDown) {
				c.drawLine (
					c.penColor,
					c.penThickness,
					c.penPos.x, c.penPos.y,
					evt.offsetX, evt.offsetY
				);
				config.socket.emit ('canvas', {
					event: _this.events.onDrawLine,
					color: c.penColor,
					thickness: c.penThickness,
					x1: c.penPos.x, y1: c.penPos.y,
					x2: evt.offsetX, y2: evt.offsetY
				});
				c.penPos.x = evt.offsetX;
				c.penPos.y = evt.offsetY;
			}
			if (event.preventDefault) { event.preventDefault (); } else { return false; }
		}
		$canvas[0].onmousedown = function (evt) {
			_this.canvas.penIsDown = true;
			_this.canvas.penPos.x = evt.offsetX;
			_this.canvas.penPos.y = evt.offsetY;
			if (event.preventDefault) { event.preventDefault (); } else { return false; }
		}
		$canvas[0].onmouseup = function (evt) {
			_this.canvas.penIsDown = false;
			if (event.preventDefault) { event.preventDefault (); } else { return false; }
		}
		// the UI callbacks
		this.$container.find ('#brush_size_picker').on ('change', function (evt) {
			_this.canvas.setPenThickness (this.value);
			config.socket.emit ('canvas', {event: _this.events.onSetPenThickness, thickness: newThickness });
		});
		this.$container.find ('#color_picker').on ('change', function (evt) {
			_this.canvas.setPenColor (this.value);
			config.socket.emit ('canvas', { event: _this.events.onSetPenColor, color: this.value });
		});
		this.$container.find ('#use_pen').on ('click', function (evt) {
			_this.canvas.setPenColor (_this.$container.find('#color_picker').val());
		});
		this.$container.find ('#use_eraser').on ('click', function (evt) {
			_this.canvas.useEraser ();
		});
		this.$container.find ('#clear_canvas').on ('click', function (evt) {
			_this.canvas.clearCanvas ();
			config.socket.emit ('canvas', { event: _this.events.onClearCanvas });
		});
		// connect all the socket listeners
		config.socket.on ('connect', function () {
			console.log ('MultiUserPaint socket connected.');
		});
		config.socket.on ('canvas', function (data) {
			//console.log ('MultiUserPaint canvas message: ', data);
			switch (data.event) {
				case _this.events.onSetPenThickness:
					_this.canvas.setPenThickness (data.thickness);
					break;
				
				case _this.events.onSetPenColor:
					_this.canvas.setPenColor (data.color);
					break;
				
				case _this.events.onClearCanvas:
					_this.canvas.clearCanvas ();
					break;
				
				case _this.events.onDrawLine:
					_this.canvas.drawLine (data.color, data.thickness, data.x1, data.y1, data.x2, data.y2);
					break;
			}
		});
		config.socket.on ('disconnect', function () {
			console.log ('MultiUserPaint socket disconnected.');
		});
	}
	
	return MultiUserPaint;
})();
