var io = require ('socket.io').listen (8081, {
	//'log level': 1,
	'browser client minification': true
});

io.sockets.on ('connection', function (sock) {
	
	sock.on ('canvas', function (data) {
		sock.broadcast.emit ('canvas', data);
	});
	
});
