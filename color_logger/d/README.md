Logger
======
A terminal logger for [D](http://dlang.org/) that supports color.

Requirements
------------
* [GDC 4.6+](https://github.com/D-Programming-GDC/GDC)
* ANSI color support for your terminal

Compiling the test
------------------
1. Make sure you have gdc installed: `sudo apt-get install gdc`
2. Run `make` from the same dir as the Makefile. `make clean` should delete the compilation output for you.

Usage
-----
Just plop `logger.d` somewhere that your compiler or project can find it then:
####Quick and simple:
```d
	import logger;
	
	void main () {
		auto l = new Logger ();
		l.log ("[r]RED [g]GREEN [b]%s[/]", "BLUE");
    }
```
You can run the module itself to see some testing output: `./logger.py`

####More involved:
```d
	import logger;
	
	void main () {
		// you can set the width for indentation in spaces
		// and you can turn off color
		auto l = new Logger (4, false);
	
		// you can turn color support on later though
		l.enableColor = true;
		// and you can set indentSize too
		l.indentSize = 2;
	
		// anything that std.string.format() supports
		l.resetIndent()
		.log ("[c]log()[/] supports string formatting:").indent (1)
		.log ("Roses are [r]RED")
		.log ("Violets are [b]%s[/]", "BLUE")
		.log ("and this is the number [m]%d[/]", 42)
		.resetIndent();
	
		// notice you can chain most functions together
		l.log ("Colors [y]run to the end[/] mark").indent (2)
			.log ("[r]But lines are auto-terminated.")
			.log ("So this is not colorized.")
		.resetIndent ()
		.log ("you can reset")
		.setIndent (2)
			.log ("and set (%d) the indentation level", 2)
		.resetIndent ();

		l.log ("Prints out all the codes for you:");
		l.showColorCodes ();
	}
```
