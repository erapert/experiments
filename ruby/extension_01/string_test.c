#include "ruby.h"

VALUE StringTest = Qnil;

void Init_stringtest ();
VALUE method_say_hi ();
VALUE method_printf_rbstr ();

void Init_stringtest ()
	{
	StringTest = rb_define_module ("StringTest");
	
	rb_define_method (StringTest, "say_hi", method_say_hi, 0);
	rb_define_method (StringTest, "print_str", method_printf_rbstr, 1);
	}

VALUE method_say_hi (VALUE self)
	{
	return rb_str_new2 ("Hello ruby. Why so slow?");
	}

VALUE method_printf_rbstr (VALUE self, VALUE rb_str)
	{
	Check_Type (rb_str, T_STRING);
	
	printf ("printing from c: \"%s\"\n", RSTRING(rb_str)->ptr);
	
	return Qnil; // All ruby methods must return a VALUE. Even it it's just nil.
	}

