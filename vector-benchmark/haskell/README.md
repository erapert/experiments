Vector benchmark in Haskell
===========================

This is a [Haskell](https://www.haskell.org/) implementation of the Vector3 benchmark.

It aims to be close in layout to the other implementations, keeping similar semantics, while using some "native" Haskell facilities.

Requirements
------------
* ghc, of course.
* [Criterion](http://www.serpentine.com/criterion/tutorial.html#benchmarking-pure-functions) cabal package (holy smokes! criterion is so cool!).

Building
--------
	cabal update
	cabal install -j --disable-test criterion
	ghc -O --make test

Running
-------
	test.exe --output results.html