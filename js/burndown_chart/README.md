BurndownChart
=============

A jQuery plugin that draws a burndown chart to a `<canvas>` element.

Requires
--------

* [jQuery](jquery.com)
* Browser that [supports HTML5](http://caniuse.com/canvas) `<canvas>`

Usage
-----

Note that the horizontal axis should be generated from the start and end dates of a sprint.

```javascript
// remainingManHours + completedManHours + assignedManHours need to equal whatever the total man hours are for this sprint
var data = {
    	horizontalAxisLabels: [ 1, 2, 3, 4, 5, 6, 7 ],
    	verticalAxisLabels: [ 0, 50, 100 ],
    	remainingManHours: [ 90, 80, 70, 60 ],
    	completedManHours: [ 0, 10, 20, 30 ],
    	assignedManHours: [ 10, 10, 10, 10 ]
    },
    // options passed in will override the defaults
    opts = {
        marginX: 32,
        marginY: 32,
        barWidth: 32,
        labelColor: '#FFFFFF'
    };

$('#thecanvas').BurndownChart (data, opts);
```

Example for how to generate the horizontal labels from sprint start/end dates:
```javascript
function dateDayDelta (firstDate, secondDate) {
	var timeDiff = Math.abs (secondDate.getTime() - firstDate.getTime());
	var deltaDays = Math.ceil (timeDiff / (1000 * 3600 * 24));
	return deltaDays;
}
var numDays = dateDayDelta (new Date ('01/01/2014'), new Date ('14/01/2014'));
for (var i = 0; i <= numDays; i++) {
    data.horizontalAxisLabels.push (i);
}
```

Options
-------

Any/all of these defaults can be overridden:

```javascript
{
	labelColor: '#000000',
	verticalLabelContinuationColor: '#BBBBBB',
	idealColor: '#0099CC',
	actualColor: '#FF0000',
	estimatedColor: '#CCCCCC',
	projectedColor: '#60FF60',
	remainingColor: '#FF6060',
	completedColor: '#33AA33',
	assignedColor: '#CC9900',
	marginX: 16,
	marginY: 16,
	columnSpacing: 16,
	barWidth: 8,
	lineWidth: 1
}
```