#!/usr/bin/ruby

require 'socket' # tcp server stuff, of course
require 'thread' # needed for Queue functionality

sock = TCPServer.new 'localhost', 20000

msgs = Queue.new
clients = []

puts 'starting echo thread...'
Thread.start do
	while msg = msgs.pop
		clients.each do |client|
			client.puts msg
		end
	end
end

puts 'accepting clients...'
loop do
	Thread.start sock.accept do |client|
		begin
			puts 'client connected.'
			clients << client
			
			while line = client.gets
				m = line.chomp!
				msgs << m
				($stdout << "> #{m}\n").flush
			end
		rescue
			($stderr << "!ERROR! #{$!.inspect}\n#{$!.backtrace}\n").flush
		ensure
			puts 'client closed.'
			clients.delete client
			client.close
		end
	end
end
