/*
	NOTES:
		- requires VRL to be an existing "namespace" object
		- to create a new instance do:
			var myCanvas = new VRL.CanvasWidget (document.getElementById ('some_canvas'));
*/
VRL.CanvasWidget = (function () {
	// privates
	
	// check to see if `f` is a function
	// doing it manually to reduce requirements on things like underscore or jquery
	function isFunc (f) {
		return !!(f && f.constructor && f.call && f.apply);
	}
	
	/*
		CONSTRUCTOR
		
		notes:
			- the callbacks specified in args{} are called at the END of the methods
			- callbacks are all bound to _this so that `this` inside the functions should refer to this instance
				rather than to the callback function itself.
		args {
			// MANDATORY
				canvasElement: element to init with
			// OPTIONAL
				onMouseUp: function(event)
				onMouseDown: function(event)
				onMouseMove: function(event)
				onDrawLine: function(color, thickness, x1, y1, x2, y2) called after a line is stroked (generally called per-mousemove)
				onClearCanvas: function()
				onSetPenColor: function(color) (doesn't work when setting the attr directly)
				onSetPenThickness: function(thickness) (doesn't work when setting the attr directly)
		}
	*/
	function CanvasWidget (args) {
		var _this = this;
		this.config = args;
		if (!this.config.canvasElement) { throw 'canvasElement is required config.'; }
		
		this.canvas = this.config.canvasElement;
		this.context = this.canvas.getContext ('2d');
		this.context.lineCap = 'round'; // no jaggies if you please
		this.penIsDown = false;
		this.penColor = '#000000';
		this.penThickness = 1;
		this.penPos = {};
		
		this.canvas.onmouseup = function (evt) {
			_this.penIsDown = false;
			if (isFunc (_this.config.onMouseUp)) { _this.config.onMouseUp.call (_this, evt); }
		};
		this.canvas.onmousedown = function (evt) {
			_this.penIsDown = true;
			_this.penPos.x = evt.offsetX;
			_this.penPos.y = evt.offsetY;
			
			if (isFunc (_this.config.onMouseDown)) { _this.config.onMouseDown.call (_this, evt); }
			
			// don't select text, don't change to a text cursor, etc.
			if (event.preventDefault) {
				event.preventDefault ();
			} else {
				return false; // curse you, IE.
			}
		};
		this.canvas.onmousemove = function (evt) {
			if (_this.penIsDown) {
				_this.drawLine (_this.penColor, _this.penThickness, _this.penPos.x, _this.penPos.y, evt.offsetX, evt.offsetY);
				_this.penPos.x = evt.offsetX;
				_this.penPos.y = evt.offsetY;
			}
			
			if (isFunc (_this.config.onMouseMove)) { _this.config.onMouseMove.call (_this, evt); }
			
			if (event.preventDefault) {
				event.preventDefault ();
			} else {
				return false; // curse you, IE.
			}
		};
	}
	
	// publics
	CanvasWidget.prototype.drawLine = function (color, thickness, x1, y1, x2, y2) {
		var c = this.context;
		c.strokeStyle = color;
		c.lineWidth = thickness;
		c.beginPath ();
		c.moveTo (x1, y1);
		c.lineTo (x2, y2);
		c.stroke ();
		if (isFunc (this.config.onDrawLine)) { this.config.onDrawLine.call (this, color, thickness, x1, y1, x2, y2); }
	}
	CanvasWidget.prototype.clearCanvas = function () {
		// save any transforms because clearing will reset them
		this.context.save ();
		this.context.setTransform (1, 0, 0, 1, 0, 0);
		this.context.clearRect (0, 0, this.canvas.width, this.canvas.height);
		this.context.restore ();
		if (isFunc (this.config.onClearCanvas)) { this.config.onClearCanvas.call (this); }
	}
	// use this to set the pen to 'eraser' rather than setting penColor directly
	CanvasWidget.prototype.useEraser = function () {
		this.context.globalCompositeOperation = 'destination-out';
		this.context.strokeStyle = 'rgba (0, 0, 0, 1.0)';
	}
	CanvasWidget.prototype.setPenColor = function (color) {
		this.context.globalCompositeOperation = 'source-over';
		this.penColor = color;
		if (isFunc (this.config.onSetPenColor)) { this.config.onSetPenColor.call (this, color); }
	}
	CanvasWidget.prototype.setPenThickness = function (thickness) {
		this.penThickness = thickness;
		if (isFunc (this.config.onSetPenThickness)) { this.config.onSetPenThickness.call (this, thickness); }
	}
	
	return CanvasWidget;
})();