#!/bin/bash


function printTestHeader () {
	echo "==============================================";
	echo "$1";
	echo "==============================================";
}


printTestHeader "ruby"
cd ./ruby
#rvm use 1.8.7
ruby ./test.rb -b

#rvm use 1.9.3
#ruby ./test.rb

#rvm use 2.0.0
#ruby ./test.rb



printTestHeader "php"
cd ../php
php ./test.php -b


printTestHeader "python"
cd ../python
python ./test.py -b 'true'


printTestHeader "C++"
cd ../cpp
./test.x86 --b


printTestHeader "C#"
cd ../cs
./test.x86 -b

printTestHeader "js"
cd ../js
./test.js -b

printTestHeader "lua"
cd ../lua
lua ./test.lua -b
