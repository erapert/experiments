#ifndef OGL_MESH_H
#define OGL_MESH_H 1

#include <vector>
#include <GL/glew.h>
#include <glm/glm.hpp>

class OGLMesh {
	public:
						OGLMesh					();
						~OGLMesh				();

	private:
						OGLMesh					(const OGLMesh & other);
		OGLMesh *		copy					(const OGLMesh & other);
		OGLMesh *		operator =				(const OGLMesh & other);

	protected:
		std::vector <glm::vec3>		verts;
		std::vector <GLuint>		elements;
};

#endif
