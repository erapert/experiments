#include <iostream>
using namespace std;

// requires that you pass "-lboost_program_options" to g++ (you'll need boost installed of course)
#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include "vector3.h"
#include "timer.h"		// c++ doesn't seem to have a standard millisecond resolution timer... :(

void unit_test () {
	Vector3 t0 (1, 1, 1);
	Vector3 t1 (1, 1, 1);
	Vector3 t2 (2, 2, 2);
	
	cout << "t0 == t1: " << t0.equals (t1) << endl;
	cout << "t1 == t2: " << t1.equals (t2) << endl;
	
	t0.set (2, 2, 2);
	cout << "t0: " << t0.stringify () << endl;
	t1.set (8, 8, 8);
	cout << "t1: " << t1.stringify () << endl;
	t2.set (16, 16, 16);
	cout << "t2: " << t2.stringify () << endl;
	
	Vector3 temp = t0.plusScalar (2);
	cout << "t0 + 2 = " << temp.stringify () << endl;
	
	cout << "t0.plus(t1): " << t0.plus(t1).stringify() << endl;
	cout << "t2.minus(t1): " << t2.minus(t1).stringify() << endl;
	
	cout << "t0.transform(0, 8, 0): " << t0.transform(0, 8, 0).stringify() << endl;
	
	cout << "t0.scale(0.5): " << t0.scale(0.5).stringify() << endl;
	
	cout << "t1.length: " << t1.length() << endl;
	cout << "t2.length: " << t2.length() << endl;
	
	cout << "t0.dot(t1): " << t0.dot(t1) << endl;
	
	cout << "t1.cross(t2): " << t1.cross(t2).stringify() << endl;
	
	t1.set (1, 2, 3);
	t2.set (4, 5, 6);
	cout << "t1.set(1, 2, 3): " << t1.stringify() << endl;
	cout << "t2.set(4, 5, 6): " << t2.stringify() << endl;
	cout << "t1.cross(t2): " << t1.cross(t2).stringify() << endl;
}

void benchmark () {
	Timer t;
	
	Vector3 t0 (1, 2, 3);
	Vector3 t1 (4, 5, 6);
	
	cout << "timing individual operations..." << endl;
	
	// php, python, and ruby benchmarks all assign to temp vars,
	// so we'll do that here too. Consider it part of the benchmark.
	string tempStr;
	Vector3 tempV;
	float tempF;
	bool tempB;
	
	t.start ();
	t0.set (1, 2, 3);
	t.stop ();
	cout << "set: " << t.durationStr() << endl;
	
	t.start ();
	tempB = t0.equals (t1);
	t.stop ();
	cout << "equals: " << t.durationStr() << endl;
	
	t.start ();
	tempStr = t0.stringify ();
	t.stop ();
	cout << "stringify: " << t.durationStr() << endl;
	
	t.start ();
	tempV = t0.plusScalar (10.0);
	t.stop ();
	cout << "plusScalar: " << t.durationStr() << endl;
	
	t.start ();
	tempV = t0.minusScalar (10.0);
	t.stop ();
	cout << "minusScalar: " << t.durationStr() << endl;
	
	t.start ();
	tempV = t0.plus (t1);
	t.stop ();
	cout << "plus: " << t.durationStr() << endl;
	
	t.start ();
	tempV = t0.minus (t1);
	t.stop ();
	cout << "minus: " << t.durationStr() << endl;
	
	t.start ();
	tempV = t0.transform (0, 8, 0);
	t.stop ();
	cout << "transform: " << t.durationStr() << endl;
	
	t.start ();
	tempV = t0.scale (0.5);
	t.stop ();
	cout << "scale: " << t.durationStr() << endl;
	
	t.start ();
	tempF = t0.length ();
	t.stop ();
	cout << "length: " << t.durationStr() << endl;
	
	t.start ();
	tempF = t0.dot (t1);
	t.stop ();
	cout << "dot: " << t.durationStr() << endl;
	
	t.start ();
	tempV = t0.cross (t1);
	t.stop ();
	cout << "cross: " << t.durationStr() << endl;
	
	cout << "Doing time trial (all ops in a row)" << endl;
	t.start ();
	for (int i = 0; i < 1000000; ++i) {
		t0.set (1, 2, 3);
		tempB = t0.equals (t1);
		tempStr = t0.stringify ();
		tempV = t0.plusScalar (10.0);
		tempV = t0.minusScalar (10.0);
		tempV = t0.plus (t1);
		tempV = t0.minus (t1);
		tempV = t0.transform (0, 8, 0);
		tempV = t0.scale (0.5);
		tempF = t0.length ();
		tempF = t0.dot (t1);
		tempV = t0.cross (t1);
	}
	t.stop ();
	cout << "time trial: " << t.durationStr() << endl;
}

int main (int argc, char ** argv) {
	
	po::options_description desc ("Allowed options");
	
	/*
		DUDE! check out this gnarly syntax-- method chaining C++ style!!
		add_options() returns a options_description_easy_init object which
		has operator() overloaded to return a reference to itself which is
		then invoked... tah-dah, method chaining!
	*/
	desc.add_options ()
		("help", "produce help message")
		("u", "run and print unit test")
		("b", "run the benchmark")
	;
	
	po::variables_map args;
	try {
		po::store (po::parse_command_line (argc, argv, desc), args);
		po::notify (args);
	} catch (...) {
		cout << desc << endl;
		return 1;
	}
	
	if ((argc == 1) || (args.count ("help"))) {
		cout << desc << endl;
		return 1;
	}
	
	if (args.count ("u")) {
		unit_test ();
	}
	
	if (args.count ("b")) {
		benchmark ();
	}
	
	return 0;
}
