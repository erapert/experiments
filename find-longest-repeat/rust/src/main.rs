use std::env;
use std::fmt;

struct StringRun {
	longest_run_char: char,
	length_of_run: i64
}

impl fmt::Display for StringRun {
	fn fmt (&self, f: &mut fmt::Formatter) -> fmt::Result {
		write! (f, "({}, {})", self.longest_run_char, self.length_of_run)
	}
}


fn get_longest_run (search_str: &String) -> StringRun {
	let mut prev_char : char = '\0';
	let mut max_char_so_far : char = '\0';
	let mut max_len_so_far : i64 = 0;
	let mut count : i64 = 1;
	
	for curr_char in search_str.chars() {
		if curr_char == prev_char {
			count += 1;
			if count > max_len_so_far {
				max_char_so_far = curr_char;
				max_len_so_far = count;
			}
		} else {
			count = 1;
		}
		prev_char = curr_char;
	}
	
	StringRun { longest_run_char: max_char_so_far, length_of_run: max_len_so_far }
}


fn run () -> i32 {
	if env::args().len() != 2 {
		println! ("The only arg this program accepts is a string to search for runs");
		return 1;
	}
	
	let test_str = env::args().nth(1).unwrap();
	let longest_run = get_longest_run (&test_str);
	
	println! ("Test string: {}", test_str);
	println! ("Longest run: {}", longest_run);
	
	return 0;
}

fn main () {
	std::process::exit (run());
}
