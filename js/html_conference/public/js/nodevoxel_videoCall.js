/*
	a single video + audio communication box/window (this is where you appear)
	requires: nodevoxel_utils.js
*/
var namespace = nodevoxel_ns || {};
namespace.VideoCall = function (vidElem, opts) {
	var cfg = Object.assign ({
		width: 320, height: 240
	}, (opts || {}));
	
	var _this = this,
		myVidElem = vidElem,
		is_running = false;
	
	_this.mediaStream = null;
	
	if (!navigator.getUserMedia) {
		navigator.getUserMedia = navigator.mozGetUserMedia || navigator.webkitGetUserMedia;
	}
	if (!navigator.getUserMedia) {
		throw 'This browser doesn\'t support getUserMedia; can\'t start video or audio.';
	}
	
	this.start = function () {
		if (this.is_running) { return; }
		
		// start the camera and audio
		navigator.getUserMedia (
			{ video: true, audio: true},
			function (stream) {
				_this.mediaStream = stream;
				myVidElem.setAttribute ('width', cfg.width);
				myVidElem.setAttribute ('height', cfg.height);
				myVidElem.src = window.URL.createObjectURL (stream);
				myVidElem.play ();
				myVidElem.style.display = '';
				is_running = true;
			},
			function (err) {
				is_running = false;
				throw err;
			}
		);
	}
	
	this.stop = function () {
		if (!this.isRunning) { return; }
		
		// stop the camera and audio
		_this.mediaStream.stop ();
		//myVidElem.src = null;
		myVidElem.style.display = 'none'; // we don't want to leave a frozen image on screen
		
		is_running = false;
	}
	
	this.isRunning = function () {
		return is_running;
	}
};
