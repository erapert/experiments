$(function () {
	var localStream, localPeerConnection, remotePeerConnection;
	
	$('#start').on ('click', function () {
		$(this).attr ('disabled', true);
		// see definition in wrtc_adapter.js
		getUserMedia (
			{ video: true, audio: true },
			function (stream) {
				$('#localVid').attr ('src', URL.createObjectURL (stream));
				localStream = stream;
				$('#call').attr ('disabled', false);
			},
			function (err) {
				throw err;
			}
		);
	});
	
	$('#call').on ('click', function () {
		$('#call').attr ('disabled', true);
		$('#hang-up').attr ('disabled', false);
		
		if ((localStream.getVideoTracks().length > 0) && (localStream.getAudioTracks().length > 0)) {
			console.log ('using (', localStream.getVideoTracks()[0].label, ') and (', localStream.getAudioTracks()[0].label, ')');
		}
		
		var servers = null;
		localPeerConnection = new RTCPeerConnection (servers);
		localPeerConnection.onicecandidate = function (evt) {
			if (!evt.candidate) {
				throw 'local ice candidate was null?!';
			}
			remotePeerConnection.addIceCandidate (new RTCIceCandidate (evt.candidate));
		};
		
		remotePeerConnection = new RTCPeerConnection (servers);
		remotePeerConnection.onicecandidate = function (evt) {
			if (!evt.candidate) {
				throw 'remote ice candidate was null?!';
			}
			localPeerConnection.addIceCandidate (new RTCIceCandidate (evt.candidate));
		};
		remotePeerConnection.onaddstream = function (evt) {
			$('#remoteVid').attr ('src', URL.createObjectURL (evt.stream));
		};
		
		localPeerConnection.addStream (localStream);
		localPeerConnection.createOffer (function (descrip) {
			localPeerConnection.setLocalDescription (descrip);
			remotePeerConnection.setRemoteDescription (descrip);
			
			remotePeerConnection.createAnswer (function (descrip) {
				remotePeerConnection.setLocalDescription (descrip);
				localPeerConnection.setRemoteDescription (descrip);
			}, function (err) {
				throw err;
			});
			
		}, function (err) {
			throw err;
		});
	});
	
	$('#hang-up').on ('click', function () {
		localPeerConnection.close ();
		remotePeerConnection.close ();
		localPeerConnection = null;
		remotePeerConnection = null;
		$(this).attr ('disabled', true);
		$('#call').attr ('disabled', false);
	});
});
