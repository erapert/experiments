/*
	convert 12 hour time string to 24 hour time string
	args:
		timeStr = a 12 hour string for time: "8:00 pm", "2:00am" etc.
	returns:
		a 24 hour time string: '12:00am' => '00:00', '1:00pm' => '13:00'
*/
module.exports.convert12to24 = function (timeStr) {
	var tstr = timeStr.replace (/ /g, '').toLowerCase(); // strip all the spaces and downcase
	var meridian = tstr.substr (tstr.length - 2, 2);
	var hours = tstr.substr (0, tstr.indexOf (':'));
	var minutes = tstr.substr (tstr.indexOf (':') + 1, 2);
	
	if (meridian == 'pm') {
		hours = (hours == '12') ? hours : (parseInt (hours, 10) + 12);
	} else {
		// make sure it's always two digits
		if (hours.length < 2) { hours = '0' + hours; }
		// midnigth is 00:00, noon is 12:00
		if (hours == '12') { hours = '00'; }
	}
	
	return hours + ':' + minutes;
}

/*
	convert American date format (mm/dd/yyyy) to mysql format (yyyy-mm-dd)
	args:
		dateStr = a string in american format (mm/dd/yyyy)
	returns:
		mysql formatted date (yyyy-mm-dd)
*/
module.exports.americanDate_to_mysqlDate = function (dateStr) {
	var tmp = dateStr.split ('/');
	var t = tmp[2];
	tmp[2] = tmp[1];
	tmp[1] = tmp[0];
	tmp[0] = t;
	return tmp.join ('-');
}
