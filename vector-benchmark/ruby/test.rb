#!/usr/bin/ruby

require 'benchmark'
require 'optparse'

if RUBY_VERSION == '1.8.7'
	require 'vector3'
else
	require_relative 'vector3'
end



def unit_test
	t0 = Vector3.new 1, 1, 1
	t1 = Vector3.new 1, 1, 1
	t2 = Vector3.new 2, 2, 2
	
	puts "t0 == t1: #{t0.equals? t1}"
	puts "t1 == t2: #{t1.equals? t2}"
	
	t0.set 2, 2, 2
	puts "t0: #{t0.stringify}"
	t1.set 8, 8, 8
	puts "t1: #{t1.stringify}"	
	t2.set 16, 16, 16
	puts "t2: #{t2.stringify}"
	
	
	temp = t0.plusScalar 2
	puts "t0 + 2 = #{temp.stringify}"
	
	puts "t0.plus(t1): #{t0.plus(t1).stringify}"
	puts "t2.minus(t1): #{t2.minus(t1).stringify}"
	
	puts "t0.transform(0, 8, 0): #{t0.transform(0, 8, 0).stringify}"
	
	puts "t0.scale 0.5: #{t0.scale(0.5).stringify}"
	
	puts "t1.length: #{t1.length}"
	puts "t2.length: #{t2.length}"
	
	puts "t0.dot(t1): #{t0.dot(t1)}"
	
	puts "t1.cross(t2): #{t1.cross(t2).stringify}"
	
	t1.set 1, 2, 3
	t2.set 4, 5, 6
	puts "t1.set(1, 2, 3): #{t1.stringify}"
	puts "t2.set(4, 5, 6): #{t2.stringify}"
	puts "t1.cross(t2): #{t1.cross(t2).stringify}"
end
	
def bench_mark
	t0 = Vector3.new 1, 2, 3
	t1 = Vector3.new 4, 5, 6
	
	puts 'timing individual operations...'
	Benchmark.bm(16) do |b|
		b.report('stringify: ') do
			temp = t0.stringify
		end
		
		b.report('equals?: ') do
			temp = t0.equals? t1
		end
		
		b.report('plusScalar: ') do
			temp = t0.plusScalar 10.0
		end
		
		b.report('minusScalar: ') do
			temp = t0.minusScalar 10.0
		end
		
		b.report('plus: ') do
			temp = t0.plus t1
		end
		
		b.report('minus: ') do
			temp = t0.minus t1
		end
		
		b.report('transform: ') do
			temp = t0.transform 0, 8, 0
		end
		
		b.report('scale: ') do
			temp = t0.scale 0.5
		end
		
		b.report('length: ') do
			temp = t0.length
		end
		
		b.report('dot: ') do
			temp = t0.dot t1
		end
		
		b.report('cross: ') do
			temp = t0.cross t1
		end
	end
	
	puts 'Doing time trial (all ops in a row)'
	
	# gives "user", "system", and "total" CPU times (clock cycles?),
	# and gives "real" time in seconds
	t = Benchmark.measure{
		(0..1000000).each do
			t0.set 1, 2, 3
			temp = t0.stringify
			temp = t0.plus t1
			temp = t0.transform(0, 8, 0)
			temp = t0.scale 0.5
			temp = t0.length
			temp = t0.dot t1
			temp = t0.cross t1
		end
	}
	puts "time trial (secs for user, system, total, (real)):\n #{t}"
end

# if run from the command line then check argv and see what to do
if __FILE__ == $0
	parser = OptionParser.new do |o|
		o.on('-u', 'run and print the unit test') { unit_test() }
		o.on('-b', 'run the benchmark') { bench_mark() }
	end
	if ARGV.length == 0
		puts parser.help()
	else
		parser.parse!
	end
end
