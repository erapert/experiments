<html>
	<head>
		<title>Carousel Test</title>
		<link rel="stylesheet" href="style.css">
		<script src="js/jquery-1.8.0.min.js"></script>
		<script src="js/jquery.easing.1.3.js"></script>
		<script src="js/jquery.mousewheel.js"></script>
		<!--<script src="js/jquery-ui-1.8.23.custom.min.js"></script>-->
		<?php
			// hard-coded vars that'll be pulled from the DB on the production version
			$users = array ();
			$names = array ('erik', 'carl', 'scott', 'andrew', 'seth', 'kenny', 'kat', 'jones', 'smith', 'seldon');
			$num_users = count ($names);
			
			for ($i = 0; $i < $num_users; $i++) {
				$users [] = array ('name' => $names [$i], 'uid' => $i);
			}
			$initial_pos = 0;
			$max_displayed_users = 3;
		?>
		<!--<script src="vrl_carousel.js"></script>-->
		<script src="vrlcarousel.js"></script>
	</head>
	<body>
		<div class="topbar">
			<!--
			<div id="carousel_ArrowLeft" class="carouselArrow" onclick="carouselScroll('left')"> &lt;&lt; </div>
			<div id="carousel_ArrowRight" class="carouselArrow" onclick="carouselScroll('right')"> &gt;&gt; </div>
			-->
		</div>
		<div id="vrlCarouselContainer" class="taskListWrapper">
			<div id="vrlCarousel_navL" class="carouselArrow"> &lt;&lt; </div>
			<div id="vrlCarousel_navR" class="carouselArrow"> &gt;&gt; </div>
			<div id="vrlCarouselContent" class="carouselWrapper">
				<?php
					// build the carousel
					foreach ($users as $user) {
						$id = $user ['uid'] + 1;
						$n = $user ['name'];
						echo "<section id=\"$id\" class=\"teamMemberList teamMemberList-$id\">";
							echo "<h4>$n</h4>";
						echo "</section>";
					}
				?>
			</div>
		</div>
	</body>
	<script>
		//var carousel = null;
		
		$(document).ready (function () {
			var carousel = $('#vrlCarouselContainer').vrlCarousel ();
			console.log ('doc ready');
		});
		
	</script>
</html>
