Chat Widget
===========

Just a trivially simple chat widget using [socket.io](http://socket.io/) and [node.js](http://nodejs.org/).

Usage
-----

```html
<html>
	<head>
		<title>HTML5 Chat Experiment</title>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
		<script src="./node_modules/socket.io/node_modules/socket.io-client/dist/socket.io.js"></script>
		<script>var VRL = VRL || {};</script>
		<script src="chat.js"></script>
		<style>
			body {
				background-color: #101010;
				font-family: 'Ubuntu' ubuntu;
				color: white;
			}
		</style>
	</head>
	<body>
		<h1>Chat Demo</h1>
		<div id="chat_container">
			<!-- chat stuff will be inserted here -->
		</div>
		<script>
			$(document).ready (function () {
				var socket = io.connect ('http://localhost:8081');
				
				/*
				    containerID and socket are required.
				    width and height are in pixels.
				*/
				var chat = new VRL.ChatWidget ({
					containerID: '#chat_container',
					socket: socket,
					width: 512, height: 128
				});
				
				
			});
		</script>
	</body>
</html>
```