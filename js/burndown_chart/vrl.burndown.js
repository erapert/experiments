/*
	A jQuery plugin for rendering a burndown chart to a <canvas>
	Requires: jQuery
*/
(function ($) {
	
	var helpers = {
		drawLine: function (ctx, style, x1, y1, x2, y2) {
			ctx.strokeStyle = style;
			ctx.beginPath ();
				ctx.moveTo (x1, y1);
				ctx.lineTo (x2, y2);
			ctx.closePath ();
			ctx.stroke ();
		},
		drawCircle: function (ctx, color, posx, posy, radius) {
			var threesixty = (Math.PI / 180) * 360; // canvas takes radians, so we convert degrees to radians
			ctx.fillStyle = color;
			ctx.arc (posx, posy, radius, 0, threesixty, true);
			ctx.fill ();
		},
		biggestInArray: function (array) {
			return Math.max.apply (Math, array);
		},
		averageRate: function (array) {
			var sumdelta = 0;
			for (var i = 0; i < array.length; i++) {
				if (array [i + 1]) {
					sumdelta += array [i] - array [i + 1];
				}
			}
			return sumdelta / array.length;
		},
		pairwiseSum: function (a1, a2) {
			if (a1.length != a2.length) { throw 'Arrays must be equal in length.'; }
			var rtrn = [];
			for (var i = 0; ((i < a1.length) && (i < a2.length)); i++) {
				rtrn.push (a1 [i] + a2 [i]);
			}
			return rtrn;
		}
	};
	
	
	// the plugin
	$.fn.BurndownChart = function (data, opts) {
		// sets up the config by overwriting defaults with opts
		var config = $.extend ({}, {
			labelColor: '#000000',
			verticalLabelContinuationColor: '#BBBBBB',
			idealColor: '#0099CC',
			actualColor: '#FF0000',
			estimatedColor: '#CCCCCC',
			projectedColor: '#60FF60',
			remainingColor: '#FF6060',
			completedColor: '#33AA33',
			assignedColor: '#CC9900',
			marginX: 16,
			marginY: 16,
			columnSpacing: 16,
			barWidth: 8,
			lineWidth: 1
		}, opts);
		
		return this.each (function () {
			var ctx = this.getContext ('2d');
			var canvasHeight = this.height,
				canvasWidth = this.width,
				xAxisLength = (canvasWidth - (config.marginX * 2)),		// drawable length of the axis
				yAxisLength = (canvasHeight - (config.marginY * 2)),
				VAxisPosX = config.marginX,								// X position of the vertical axis line
				HAxisPosY = canvasHeight - config.marginY,				// Y position of the horizontal axis line
				biggestXLabel = helpers.biggestInArray (data.horizontalAxisLabels),
				biggestYLabel = helpers.biggestInArray (data.verticalAxisLabels),
				yfactor = (yAxisLength / biggestYLabel),
				xfactor = (xAxisLength / biggestXLabel);
			
			
			// returns a list of Y values that line up with the labels
			function drawVerticalAxis (data) {
				// draw the labels
				var axisLabels = data.verticalAxisLabels.sort (function (a, b) { return a > b; }),
					stepSize = (yAxisLength / (axisLabels.length - 1)),
					y = HAxisPosY,
					rtrn = [];
				
				ctx.fillStyle = config.labelColor;
				ctx.lineWidth = 1;
				for (var i = 0; i < axisLabels.length; i++) {
					// draw the label itself
					ctx.fillText (axisLabels[i], 0, y - 2);
					// indicates the exact position the label represents
					helpers.drawLine (ctx, config.labelColor, config.marginX, y, 0, y);
					// a light line across the graph
					helpers.drawLine (ctx, config.verticalLabelContinuationColor, config.marginX, y, canvasWidth - config.marginX, y);
					rtrn.push (y);
					// remember, HTML5 canvas (0,0) is in the top left corner
					// so we're going bottom to top
					y -= stepSize;
				}
				// draw vertical line last so that it stamps over the stubs of the labels
				helpers.drawLine (
					ctx, config.labelColor,
					config.marginX, config.marginY,
					config.marginX, HAxisPosY
				);
				return rtrn;
			}
			
			// returns a list of X values that line up with the labels
			function drawHorizontalAxis (data) {
				var labels = data.horizontalAxisLabels,
					stepSize = (xAxisLength / labels.length),
					x = config.marginX,
					rtrn = [];
				
				// draw the lables
				ctx.fillStyle = config.labelColor;
				for (var i = 0; i < labels.length; i++) {
					ctx.fillText (labels[i], (x + ((config.barWidth / 2) - 4)), canvasHeight);
					//helpers.drawLine (ctx, config.labelColor, x, HAxisPosY, x, canvasHeight);
					rtrn.push (x);
					x += stepSize;
				}
				
				// axis line
				helpers.drawLine (
					ctx, config.labelColor,
					config.marginX, HAxisPosY,
					canvasWidth - config.marginX, HAxisPosY
				);
				
				return rtrn;
			}
			
			// draws the remaining, completed, assigned bars for a day
			// returns the Y position of the total uncompleted tasks for the day
			function drawDailyBars (data, index, posX) {
				var r = data.remainingManHours[index],
					c = data.completedManHours[index],
					a = data.assignedManHours[index],
					rheight = HAxisPosY - (r * yfactor),
					aheight = rheight - (a * yfactor),
					cheight = aheight - (c * yfactor);
				
				// +1 to dodge the vertical axis, -1 to dodge the horizontal axis
				ctx.fillStyle = config.remainingColor;
				ctx.fillRect (posX + 1, rheight, config.barWidth, HAxisPosY - rheight - 1);
				
				ctx.fillStyle = config.assignedColor;
				ctx.fillRect (posX + 1, aheight, config.barWidth, a * yfactor);
				
				ctx.fillStyle = config.completedColor;
				ctx.fillRect (posX + 1, cheight, config.barWidth, c * yfactor);
				
				// return the Y position of the total uncompleted tasks for the day
				return aheight;
			}
			
			// draws the lines for estimated completion time etc.
			function drawLines (data, xlabels, yposUndones) {
				// all the graph lines should be this width
				ctx.lineWidth = config.lineWidth;
				var barOffset = config.barWidth / 2,	// the bar itself has a thickness, so we want to draw in the middle of a bar
					lastSegmentIndex = 0;				// index of the last line segment drawn
				
				// draw the ideal line
					helpers.drawLine (
						ctx, config.idealColor,
						VAxisPosX + barOffset, config.marginY,
						canvasWidth - config.marginX, HAxisPosY
					);
				
				// draw the line for actual completed tasks
					if (xlabels.length != yposUndones.length) {
						throw 'number of xlabels must match number of undone y positions';
					}
					
					for (var x = 0; ((x < xlabels.length) && (x < yposUndones.length)); x++) {
						if ((xlabels [x + 1]) && (yposUndones [x + 1])) {
							var xpos1 = xlabels [x] + barOffset,
								xpos2 = xlabels [x + 1] + barOffset,
								ypos1 = yposUndones [x],
								ypos2 = yposUndones [x + 1];
							
							helpers.drawLine (ctx, config.actualColor, xpos1, ypos1, xpos2, ypos2);
							helpers.drawCircle (ctx, config.actualColor, xpos1, ypos1, 6);
							lastSegmentIndex = x + 1;
						}
					}
					// draw a dot at the last point
					helpers.drawCircle (ctx, config.actualColor, xlabels[lastSegmentIndex] + barOffset, yposUndones[lastSegmentIndex], 6);
				
				// trajectory completion: draw a line according to the slope running through the last dot
					var xpos1 = xlabels [0],
						xpos2 = xlabels [lastSegmentIndex],
						ypos1 = config.marginY,
						ypos2 = yposUndones [lastSegmentIndex],
						xdifference = xpos2 - xpos1,
						ydifference = ypos2 - ypos1;
					
					// note that we have to make the Y negative because the canvas origin is top left...
					var trajectorySlope = (-ydifference) / xdifference;
					
					helpers.drawLine (
						ctx, config.projectedColor,
						xpos1 + barOffset, ypos1,
						-(HAxisPosY / trajectorySlope), HAxisPosY
					);
				
				// estimated completion: line according to the average rate of completion
					var manHoursSum = helpers.pairwiseSum (data.remainingManHours, data.assignedManHours),
						avgRate = helpers.averageRate (manHoursSum);
					helpers.drawLine (
						ctx, config.estimatedColor,
						xpos1 + barOffset, ypos1,
						(avgRate * xfactor) + barOffset, HAxisPosY
					);
			}
			
			// man-hours labels
			ylabels = drawVerticalAxis (data);
			// dates labels
			xlabels = drawHorizontalAxis (data);
			
			var undones = [],
				dailyUndone = null;
			for (var i = 0; i < xlabels.length; i++) {
				dailyUndone = drawDailyBars (data, i, xlabels [i]);
				undones.push (dailyUndone);
			}
			
			drawLines (data, xlabels, undones);
		});
	};
})(jQuery);